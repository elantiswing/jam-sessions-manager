<?

use Cake\Core\Configure;

return [
  'Users.SimpleRbac.permissions' => [
    [
      'role' => '*',
      'plugin' => 'CakeDC/Users',
      'controller' => 'Users',
      'action' => ['login', 'logout'],
    ],
    [
      'role' => '*',
      'plugin' => false,
      'controller' => 'MyUsers',
      'action' => ['edit', 'getUserSelections', 'changePassword1'],
    ],
    [
      'role' => '*',
      'controller' => 'Pages',
      'action' => ['index', 'schedule'],
    ],
    [
      'role' => '*',
      'controller' => 'Tunes',
      'action' => ['proposesList']
    ],
    [
      'role' => '*',
      'controller' => 'Tunes',
      'action' => ['viewProposes', 'add'],
      'allowed' => function (array $user, $role, \Cake\Network\Request $request) {
        return Configure::read('config')['add_suggestions'];
      }
    ],
    [
      'role' => '*',
      'controller' => 'Tunes',
      'action' => ['saveSelection'],
      'allowed' => function (array $user, $role, \Cake\Network\Request $request) {
        return Configure::read('config')['tune_selection'];
      }
    ],
    [
      'role' => '*',
      'controller' => 'Tunes',
      'action' => ['select'],
      'allowed' => function (array $user, $role, \Cake\Network\Request $request) {
        return Configure::read('config')['users_view_select_tunes_list'];
      }
    ],
    [
      'role' => '*',
      'plugin' => 'CakeDC/Forum',
      'controller' => 'Likes',
      'action' => ['add'],
    ],
    [
      'role' => '*',
      'plugin' => 'CakeDC/Forum',
      'controller' => 'Replies',
      'action' => ['add', 'edit', 'delete'],
    ],
    [
      'role' => '*',
      'plugin' => 'CakeDC/Forum',
      'controller' => 'Threads',
      'action' => ['my', 'add', 'edit', 'delete'],
    ],
  ]
];
