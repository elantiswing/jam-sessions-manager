-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 28-10-2019 a las 21:19:56
-- Versión del servidor: 5.5.64-MariaDB
-- Versión de PHP: 7.1.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `jam`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `available_schedules`
--

CREATE TABLE `available_schedules` (
  `id` bigint(20) NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  `type` enum('morning','evening') NOT NULL DEFAULT 'morning',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `available_schedules`
--

INSERT INTO `available_schedules` (`id`, `start`, `end`, `type`, `created`, `updated`) VALUES
(1, '2019-11-02 10:30:00', '2019-11-02 14:30:00', 'morning', '2017-10-17 00:00:00', '2017-10-17 00:00:00'),
(2, '2019-11-02 16:00:00', '2019-11-02 22:30:00', 'evening', '2017-10-17 00:00:00', '2017-10-17 00:00:00'),
(3, '2019-11-03 10:30:00', '2019-11-03 14:30:00', 'morning', '2017-10-17 00:00:00', '2017-10-17 00:00:00'),
(4, '2019-11-03 16:00:00', '2019-11-03 22:30:00', 'evening', '2017-10-17 00:00:00', '2017-10-17 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cake_d_c_forum_phinxlog`
--

CREATE TABLE `cake_d_c_forum_phinxlog` (
  `version` bigint(20) NOT NULL,
  `migration_name` varchar(100) DEFAULT NULL,
  `start_time` timestamp NULL DEFAULT NULL,
  `end_time` timestamp NULL DEFAULT NULL,
  `breakpoint` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cake_d_c_forum_phinxlog`
--

INSERT INTO `cake_d_c_forum_phinxlog` (`version`, `migration_name`, `start_time`, `end_time`, `breakpoint`) VALUES
(20170711111306, 'Initial', '2019-10-09 21:57:04', '2019-10-09 21:57:05', 0),
(20170712131022, 'AddPostsLastReplyCreatedColumn', '2019-10-09 21:57:05', '2019-10-09 21:57:05', 0),
(20170712202134, 'AddPostsIsVisibleColumn', '2019-10-09 21:57:05', '2019-10-09 21:57:06', 0),
(20170718130618, 'CreateModeratorsTable', '2019-10-09 21:57:06', '2019-10-09 21:57:06', 0),
(20170720115402, 'AddReportsCount', '2019-10-09 21:57:06', '2019-10-09 21:57:06', 0),
(20170720122740, 'CreateLikesTable', '2019-10-09 21:57:06', '2019-10-09 21:57:06', 0),
(20171011135829, 'AddCategoriesAndPostsLastReplyId', '2019-10-09 21:57:06', '2019-10-09 21:57:07', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cake_d_c_users_phinxlog`
--

CREATE TABLE `cake_d_c_users_phinxlog` (
  `version` bigint(20) NOT NULL,
  `migration_name` varchar(100) DEFAULT NULL,
  `start_time` timestamp NULL DEFAULT NULL,
  `end_time` timestamp NULL DEFAULT NULL,
  `breakpoint` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cake_d_c_users_phinxlog`
--

INSERT INTO `cake_d_c_users_phinxlog` (`version`, `migration_name`, `start_time`, `end_time`, `breakpoint`) VALUES
(20150513201111, 'Initial', '2019-10-09 11:27:35', '2019-10-09 11:27:35', 0),
(20161031101316, 'AddSecretToUsers', '2019-10-09 11:27:35', '2019-10-09 11:27:35', 0),
(20190208174112, 'AddAdditionalDataToUsers', '2019-10-09 11:27:35', '2019-10-09 11:27:35', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `config`
--

CREATE TABLE `config` (
  `id` int(11) NOT NULL,
  `config` varchar(100) NOT NULL,
  `value` varchar(100) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `config`
--

INSERT INTO `config` (`id`, `config`, `value`) VALUES
(1, 'add_suggestions', '0'),
(2, 'users_view_tunes_list', '0'),
(3, 'tune_selection', '0'),
(4, 'users_view_select_tunes_list', '1'),
(5, 'view_rehearsals', '0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `editions`
--

CREATE TABLE `editions` (
  `id` int(20) NOT NULL,
  `edition` int(4) NOT NULL,
  `name` varchar(100) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `editions`
--

INSERT INTO `editions` (`id`, `edition`, `name`) VALUES
(1, 2016, ''),
(2, 2017, ''),
(3, 2018, 'Delas Jam'),
(4, 2019, 'Erecciones generales');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `editions_tunes`
--

CREATE TABLE `editions_tunes` (
  `tune_id` int(20) NOT NULL,
  `edition_id` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `editions_tunes`
--

INSERT INTO `editions_tunes` (`tune_id`, `edition_id`) VALUES
(9, 1),
(11, 1),
(13, 1),
(14, 1),
(15, 2),
(18, 1),
(19, 1),
(23, 1),
(24, 2),
(30, 1),
(31, 1),
(35, 1),
(38, 1),
(39, 1),
(43, 1),
(44, 1),
(47, 1),
(49, 1),
(50, 1),
(52, 1),
(53, 1),
(57, 1),
(58, 1),
(59, 1),
(60, 1),
(63, 1),
(67, 1),
(71, 2),
(74, 1),
(76, 1),
(78, 1),
(79, 1),
(80, 2),
(81, 1),
(86, 2),
(89, 2),
(91, 2),
(93, 2),
(96, 2),
(98, 2),
(103, 2),
(106, 2),
(110, 2),
(113, 2),
(114, 2),
(115, 2),
(117, 2),
(119, 2),
(124, 2),
(126, 2),
(128, 2),
(132, 2),
(133, 2),
(134, 2),
(138, 2),
(140, 2),
(141, 2),
(142, 2),
(144, 3),
(145, 3),
(147, 3),
(148, 3),
(149, 3),
(150, 3),
(151, 3),
(152, 3),
(154, 3),
(156, 3),
(157, 3),
(158, 3),
(159, 3),
(160, 3),
(162, 3),
(163, 3),
(164, 3),
(165, 3),
(166, 3),
(167, 3),
(168, 3),
(170, 3),
(171, 3),
(172, 3),
(174, 3),
(175, 3),
(176, 3),
(177, 3),
(179, 3),
(183, 4),
(184, 4),
(187, 4),
(188, 4),
(189, 4),
(190, 4),
(191, 4),
(192, 4),
(193, 4),
(194, 4),
(195, 4),
(196, 4),
(197, 4),
(198, 4),
(199, 4),
(200, 4),
(202, 4),
(203, 4),
(205, 4),
(206, 4),
(209, 4),
(211, 4),
(217, 4),
(219, 4),
(223, 4),
(226, 4),
(227, 4),
(228, 4),
(229, 4),
(230, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `editions_users`
--

CREATE TABLE `editions_users` (
  `user_id` char(36) NOT NULL,
  `edition_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `editions_users`
--

INSERT INTO `editions_users` (`user_id`, `edition_id`) VALUES
('03198792-5c0d-4473-b218-447a2ec7f1fd', 4),
('078e48a4-5b2a-410f-8bef-33babe2378c3', 4),
('1310f7d5-0bf8-4406-b57b-12a588cf0e18', 4),
('15685769-e355-421a-ab82-c19f0efcce87', 4),
('18f7f0b3-e58c-49e2-ab95-fefe897de617', 4),
('1d7ab30b-16ec-470a-aead-7f51346dcebb', 4),
('1e93668c-8b5d-4988-a3d1-bffcd8a7417e', 4),
('30dbfe50-099a-46df-b8fb-caf4e6983c0d', 4),
('33d81274-7b8a-48ad-82cf-3087de06dac0', 4),
('3c4c8b18-c991-43f6-a484-23be4b42565e', 4),
('3e3d7f52-db85-473d-9c83-3d0d8f7ef9f0', 4),
('4303263a-0b6c-40ff-95f6-c3563de7a013', 4),
('53f2bfa4-e4aa-429d-b177-2dea148fdeb3', 4),
('56588347-a0d7-4424-ad26-fc4fcba747eb', 4),
('5a3d04ca-acf8-4110-95e8-6f8f79232060', 4),
('5c2f81fd-780f-4fd0-99af-23a813ea79b3', 4),
('5cbbd8ab-32a8-41b4-8633-b2302a73f60d', 4),
('6192448a-c6cf-4ffa-92c7-d0b11b74f629', 4),
('63d24792-9c5a-45e7-af41-19184caa3fff', 4),
('682cf312-9a70-4b40-a7e3-6dd6d4afb25e', 4),
('697fc2e6-dfcf-4c23-901d-ef12d760491b', 4),
('6ae6a5b1-88b1-48fc-923d-f4dbe6b4c929', 4),
('707a332a-7931-4527-9a1f-cfc3f114e1d7', 4),
('7858508c-b4fe-4004-982e-0e88ea4ad4a2', 4),
('8c052cad-30aa-4049-91d0-5b2429d7c581', 4),
('b2348b12-f6b7-4758-accd-fc0291e1cec8', 4),
('b2efde4c-e55e-446d-9146-8cfbaeca72c3', 4),
('cfb9a2db-45ec-472f-a749-e8c6f48e266d', 4),
('d12c031e-3628-4585-9c11-0557f086b835', 4),
('d882f52c-7e98-485c-b483-2a320a6e7981', 4),
('e1a1051a-24d8-4960-a9ee-84be21b6cfa2', 4),
('e1fc5fc5-c42c-4bb5-97c0-d88a75e61f9a', 4),
('eed947e5-8779-4e98-9c0a-b283465963d1', 4),
('efcf62f6-62e5-44ed-becd-af853db69a5f', 4),
('f26b98db-4590-4335-a3ad-e2340b8b2de3', 4),
('f8265210-f8dc-4832-90fd-7625a1a76461', 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `forum_categories`
--

CREATE TABLE `forum_categories` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `last_post_id` int(11) DEFAULT NULL,
  `lft` int(11) NOT NULL,
  `rght` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `threads_count` int(11) NOT NULL,
  `replies_count` int(11) NOT NULL,
  `is_visible` tinyint(1) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `forum_categories`
--

INSERT INTO `forum_categories` (`id`, `parent_id`, `last_post_id`, `lft`, `rght`, `title`, `slug`, `description`, `threads_count`, `replies_count`, `is_visible`, `created`, `modified`) VALUES
(1, NULL, NULL, 1, 2, 'Eres tú (Mocedades)', 'eres-tu-mocedades', '', 0, 0, 1, '2019-10-27 15:49:48', '2019-10-27 15:49:48'),
(2, NULL, NULL, 3, 4, 'Ingrata (Café Tacuba)', 'ingrata-cafe-tacuba', '', 0, 0, 1, '2019-10-27 15:49:48', '2019-10-27 15:49:48'),
(3, NULL, NULL, 5, 6, 'Soy una punk (Aerolineas Federales)', 'soy-una-punk-aerolineas-federales', '', 0, 0, 1, '2019-10-27 15:49:48', '2019-10-27 15:49:48'),
(4, NULL, NULL, 7, 8, 'El Rey y yo (Los Ángeles Negros)', 'el-rey-y-yo-los-angeles-negros', '', 0, 0, 1, '2019-10-27 15:49:48', '2019-10-27 15:49:48'),
(5, NULL, NULL, 9, 10, 'Obsesión (Aventura)', 'obsesion-aventura', '', 0, 0, 1, '2019-10-27 15:49:48', '2019-10-27 15:49:48'),
(6, NULL, NULL, 11, 12, 'Promesas que no valen nada (Los Piratas)', 'promesas-que-no-valen-nada-los-piratas', '', 0, 0, 1, '2019-10-27 15:49:48', '2019-10-27 15:49:48'),
(7, NULL, NULL, 13, 14, 'Huesos (Los Burros)', 'huesos-los-burros', '', 0, 0, 1, '2019-10-27 15:49:48', '2019-10-27 15:49:48'),
(8, NULL, NULL, 15, 16, 'Se me ha muerto el canario (No me pises que llevo chanclas)', 'se-me-ha-muerto-el-canario-no-me-pises-que-llevo-chanclas', '', 0, 0, 1, '2019-10-27 15:49:48', '2019-10-27 15:49:48'),
(9, NULL, NULL, 17, 18, 'Bad Romance (Lady Gaga)', 'bad-romance-lady-gaga', '', 0, 0, 1, '2019-10-27 15:49:48', '2019-10-27 15:49:48'),
(10, NULL, NULL, 19, 20, 'There is a light that never goes out (The Smiths)', 'there-is-a-light-that-never-goes-out-the-smiths', '', 0, 0, 1, '2019-10-27 15:49:48', '2019-10-27 15:49:48'),
(11, NULL, 3, 21, 22, 'Starman (David Bowie)', 'starman-david-bowie', '', 1, 2, 1, '2019-10-27 15:49:48', '2019-10-27 20:40:29'),
(12, NULL, NULL, 23, 24, '99 red ballons (Nena)', '99-red-ballons-nena', '', 0, 0, 1, '2019-10-27 15:49:48', '2019-10-27 15:49:48'),
(13, NULL, NULL, 25, 26, 'Go your Own Way (Fleetwood Mac)', 'go-your-own-way-fleetwood-mac', '', 0, 0, 1, '2019-10-27 15:49:48', '2019-10-27 15:49:48'),
(14, NULL, NULL, 27, 28, 'God Save the Queen  (Sex Pistols)', 'god-save-the-queen-sex-pistols', '', 0, 0, 1, '2019-10-27 15:49:48', '2019-10-27 15:49:48'),
(15, NULL, NULL, 29, 30, 'Girls just wanna have fun (Cindy Lauper)', 'girls-just-wanna-have-fun-cindy-lauper', '', 0, 0, 1, '2019-10-27 15:49:48', '2019-10-27 15:49:48'),
(16, NULL, NULL, 31, 32, 'Enjoy the silence (Depeche Mode)', 'enjoy-the-silence-depeche-mode', '', 0, 0, 1, '2019-10-27 15:49:48', '2019-10-27 15:49:48'),
(17, NULL, NULL, 33, 34, 'A Little Less Conversation (Elvis Presley)', 'a-little-less-conversation-elvis-presley', '', 0, 0, 1, '2019-10-27 15:49:48', '2019-10-27 15:49:48'),
(18, NULL, NULL, 35, 36, 'Macho Man (Village People)', 'macho-man-village-people', '', 0, 0, 1, '2019-10-27 15:49:48', '2019-10-27 15:49:48'),
(19, NULL, NULL, 37, 38, 'Strutter (Kiss)', 'strutter-kiss', '', 0, 0, 1, '2019-10-27 15:49:48', '2019-10-27 15:49:48'),
(20, NULL, NULL, 39, 40, 'Little Green Bag (B.S.O Reservoir Dogs)', 'little-green-bag-b-s-o-reservoir-dogs', '', 0, 0, 1, '2019-10-27 15:49:48', '2019-10-27 15:49:48'),
(21, NULL, NULL, 41, 42, 'Time of my life (B.S.O Dirty Dancing)', 'time-of-my-life-b-s-o-dirty-dancing', '', 0, 0, 1, '2019-10-27 15:49:48', '2019-10-27 15:49:48'),
(22, NULL, NULL, 43, 44, 'Heart (Barracuda)', 'heart-barracuda', '', 0, 0, 1, '2019-10-27 15:49:48', '2019-10-27 15:49:48'),
(23, NULL, NULL, 45, 46, 'Tanto la quería (Andy y Lucas)', 'tanto-la-queria-andy-y-lucas', '', 0, 0, 1, '2019-10-27 15:49:48', '2019-10-27 15:49:48'),
(24, NULL, NULL, 47, 48, 'Amores de barra (Ella baila sola)', 'amores-de-barra-ella-baila-sola', '', 0, 0, 1, '2019-10-27 15:49:48', '2019-10-27 15:49:48'),
(25, NULL, NULL, 49, 50, 'Voy a mil (Olé Olé)', 'voy-a-mil-ole-ole', '', 0, 0, 1, '2019-10-27 15:49:48', '2019-10-27 15:49:48'),
(26, NULL, NULL, 51, 52, 'Embrujada (Tino Casal)', 'embrujada-tino-casal', '', 0, 0, 1, '2019-10-27 15:49:48', '2019-10-27 15:49:48'),
(27, NULL, NULL, 53, 54, 'Gimme (Abba)', 'gimme-abba', '', 0, 0, 1, '2019-10-27 15:49:48', '2019-10-27 15:49:48'),
(28, NULL, NULL, 55, 56, 'Kick Out The Jams (MCS)', 'kick-out-the-jams-mcs', '', 0, 0, 1, '2019-10-27 15:49:48', '2019-10-27 15:49:48'),
(29, NULL, NULL, 57, 58, 'hit the road Jack (Ray Charles)', 'hit-the-road-jack-ray-charles', '', 0, 0, 1, '2019-10-27 15:49:48', '2019-10-27 15:49:48'),
(30, NULL, NULL, 59, 60, 'Time to pretend (MGMT)', 'time-to-pretend-mgmt', '', 0, 0, 1, '2019-10-27 15:49:48', '2019-10-27 15:49:48');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `forum_likes`
--

CREATE TABLE `forum_likes` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `user_id` char(36) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `forum_likes`
--

INSERT INTO `forum_likes` (`id`, `post_id`, `user_id`, `created`, `modified`) VALUES
(1, 1, 'd12c031e-3628-4585-9c11-0557f086b835', '2019-10-28 08:47:46', '2019-10-28 08:47:46');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `forum_moderators`
--

CREATE TABLE `forum_moderators` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `user_id` char(36) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `forum_posts`
--

CREATE TABLE `forum_posts` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `category_id` int(11) NOT NULL,
  `last_reply_id` int(11) DEFAULT NULL,
  `user_id` char(36) NOT NULL,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `message` longtext NOT NULL,
  `replies_count` int(11) NOT NULL,
  `reports_count` int(11) NOT NULL,
  `likes_count` int(11) NOT NULL,
  `is_sticky` tinyint(1) NOT NULL,
  `is_locked` tinyint(1) NOT NULL,
  `is_visible` tinyint(1) NOT NULL DEFAULT '1',
  `last_reply_created` datetime DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `forum_posts`
--

INSERT INTO `forum_posts` (`id`, `parent_id`, `category_id`, `last_reply_id`, `user_id`, `title`, `slug`, `message`, `replies_count`, `reports_count`, `likes_count`, `is_sticky`, `is_locked`, `is_visible`, `last_reply_created`, `created`, `modified`) VALUES
(1, NULL, 11, 3, '18f7f0b3-e58c-49e2-ab95-fefe897de617', 'Cambio de versión', 'cambio-de-verison', 'Hola chic@s d Starman, me ncanta sta versión dl propio Bowie, q os parece si la hacemos así, no se si pa los músicos cambia algo pero a mi hasta m parece un poco + rápida. Decidme algo. \r\nEs el\"(1972) official video\"\r\nhttps://youtu.be/sI66hcu9fIs', 2, 0, 1, 0, 0, 1, '2019-10-28 08:54:59', '2019-10-27 20:40:29', '2019-10-27 20:49:44'),
(2, 1, 11, NULL, 'd12c031e-3628-4585-9c11-0557f086b835', '', '', '<p>Vale!&nbsp;</p>', 0, 0, 0, 0, 0, 1, NULL, '2019-10-28 06:51:18', '2019-10-28 06:51:18'),
(3, 1, 11, NULL, '33d81274-7b8a-48ad-82cf-3087de06dac0', '', '', '<p>Sen faio</p>', 0, 0, 0, 0, 0, 1, NULL, '2019-10-28 08:54:59', '2019-10-28 08:54:59');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `forum_reports`
--

CREATE TABLE `forum_reports` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `user_id` char(36) NOT NULL,
  `message` longtext NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `instruments`
--

CREATE TABLE `instruments` (
  `id` bigint(20) NOT NULL,
  `instrument` varchar(50) NOT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `instruments`
--

INSERT INTO `instruments` (`id`, `instrument`, `created`, `updated`) VALUES
(1, 'Voz', NULL, NULL),
(2, 'Coros', NULL, NULL),
(3, 'Guitarra solista', NULL, NULL),
(4, 'Guitarra', NULL, NULL),
(5, 'Baixo', NULL, '2016-10-19 00:48:51'),
(6, 'Batería', NULL, '2016-10-20 11:16:20'),
(7, 'Teclado', NULL, NULL),
(8, 'Acordeón', '2017-10-26 14:58:59', '2017-10-26 14:58:59'),
(9, 'Violín', '2019-10-20 00:00:00', '2019-10-20 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `instruments_tunes`
--

CREATE TABLE `instruments_tunes` (
  `tune_id` bigint(20) NOT NULL,
  `instrument_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `instruments_tunes`
--

INSERT INTO `instruments_tunes` (`tune_id`, `instrument_id`) VALUES
(2, 1),
(2, 3),
(2, 5),
(2, 6),
(6, 1),
(6, 3),
(6, 5),
(6, 6),
(6, 7),
(7, 1),
(7, 2),
(7, 3),
(7, 4),
(7, 5),
(7, 6),
(8, 1),
(8, 3),
(8, 5),
(8, 6),
(8, 7),
(9, 1),
(9, 2),
(9, 3),
(9, 5),
(9, 6),
(9, 7),
(10, 1),
(10, 3),
(10, 4),
(10, 5),
(10, 6),
(11, 1),
(11, 2),
(11, 3),
(11, 5),
(11, 6),
(11, 7),
(12, 1),
(12, 3),
(12, 4),
(12, 5),
(12, 6),
(13, 1),
(13, 3),
(13, 5),
(13, 6),
(14, 1),
(14, 3),
(14, 5),
(14, 6),
(15, 1),
(15, 2),
(15, 3),
(15, 5),
(15, 6),
(15, 7),
(16, 1),
(16, 3),
(16, 5),
(16, 6),
(18, 1),
(18, 3),
(18, 5),
(18, 6),
(18, 7),
(19, 1),
(19, 3),
(19, 4),
(19, 5),
(19, 6),
(20, 1),
(20, 2),
(20, 3),
(20, 5),
(20, 6),
(21, 1),
(21, 2),
(21, 3),
(21, 5),
(21, 6),
(21, 7),
(22, 1),
(22, 3),
(22, 4),
(22, 5),
(22, 6),
(23, 1),
(23, 2),
(23, 3),
(23, 5),
(23, 6),
(24, 1),
(24, 2),
(24, 3),
(24, 5),
(24, 6),
(25, 1),
(25, 2),
(25, 3),
(25, 5),
(25, 6),
(25, 7),
(26, 1),
(26, 2),
(26, 3),
(26, 5),
(26, 6),
(27, 1),
(27, 3),
(27, 5),
(27, 6),
(27, 7),
(28, 1),
(28, 3),
(28, 5),
(28, 6),
(29, 1),
(29, 2),
(29, 3),
(29, 5),
(29, 6),
(30, 1),
(30, 2),
(30, 3),
(30, 5),
(30, 6),
(30, 7),
(31, 1),
(31, 3),
(31, 4),
(31, 5),
(31, 6),
(32, 1),
(32, 3),
(32, 5),
(32, 6),
(33, 1),
(33, 2),
(33, 3),
(33, 5),
(33, 6),
(34, 1),
(34, 3),
(34, 5),
(34, 6),
(34, 7),
(35, 1),
(35, 2),
(35, 3),
(35, 5),
(35, 6),
(35, 7),
(36, 1),
(36, 3),
(36, 5),
(36, 6),
(37, 1),
(37, 2),
(37, 3),
(37, 5),
(37, 6),
(37, 7),
(38, 1),
(38, 2),
(38, 3),
(38, 5),
(38, 6),
(39, 1),
(39, 2),
(39, 3),
(39, 5),
(39, 6),
(39, 7),
(40, 1),
(40, 3),
(40, 5),
(40, 6),
(40, 7),
(41, 1),
(41, 3),
(41, 5),
(41, 6),
(42, 1),
(42, 3),
(42, 5),
(42, 6),
(42, 7),
(43, 1),
(43, 2),
(43, 3),
(43, 5),
(43, 6),
(43, 7),
(44, 1),
(44, 2),
(44, 3),
(44, 5),
(44, 6),
(44, 7),
(45, 1),
(45, 2),
(45, 3),
(45, 5),
(45, 6),
(45, 7),
(46, 1),
(46, 2),
(46, 3),
(46, 4),
(46, 5),
(46, 6),
(47, 1),
(47, 2),
(47, 3),
(47, 4),
(47, 5),
(47, 6),
(48, 1),
(48, 2),
(48, 3),
(48, 5),
(48, 6),
(48, 7),
(49, 1),
(49, 2),
(49, 3),
(49, 5),
(49, 6),
(49, 7),
(50, 1),
(50, 2),
(50, 3),
(50, 5),
(50, 6),
(51, 1),
(51, 3),
(51, 4),
(51, 5),
(51, 6),
(52, 1),
(52, 3),
(52, 4),
(52, 5),
(52, 6),
(53, 1),
(53, 2),
(53, 3),
(53, 4),
(53, 5),
(53, 6),
(55, 1),
(55, 2),
(55, 3),
(55, 5),
(55, 6),
(55, 7),
(56, 1),
(56, 3),
(56, 4),
(56, 5),
(56, 6),
(57, 1),
(57, 2),
(57, 3),
(57, 5),
(57, 6),
(58, 1),
(58, 2),
(58, 3),
(58, 5),
(58, 6),
(59, 1),
(59, 2),
(59, 3),
(59, 5),
(59, 6),
(59, 7),
(60, 1),
(60, 2),
(60, 3),
(60, 5),
(60, 6),
(61, 1),
(61, 2),
(61, 3),
(61, 5),
(61, 6),
(61, 7),
(62, 1),
(62, 2),
(62, 3),
(62, 4),
(62, 5),
(62, 6),
(62, 7),
(63, 1),
(63, 3),
(63, 4),
(63, 5),
(63, 6),
(64, 1),
(64, 3),
(64, 5),
(64, 6),
(64, 7),
(65, 1),
(65, 2),
(65, 3),
(65, 5),
(65, 6),
(65, 7),
(66, 1),
(66, 2),
(66, 3),
(66, 5),
(66, 6),
(67, 1),
(67, 2),
(67, 3),
(67, 5),
(67, 6),
(68, 1),
(68, 2),
(68, 3),
(68, 5),
(68, 6),
(68, 7),
(69, 1),
(69, 2),
(69, 3),
(69, 5),
(69, 6),
(69, 7),
(70, 1),
(70, 3),
(70, 5),
(70, 6),
(71, 1),
(71, 2),
(71, 3),
(71, 5),
(71, 6),
(71, 7),
(72, 1),
(72, 2),
(72, 3),
(72, 5),
(72, 6),
(72, 7),
(73, 1),
(73, 2),
(73, 3),
(73, 5),
(73, 6),
(73, 7),
(74, 1),
(74, 3),
(74, 4),
(74, 5),
(74, 6),
(75, 1),
(75, 3),
(75, 4),
(75, 5),
(75, 6),
(76, 1),
(76, 2),
(76, 3),
(76, 5),
(76, 6),
(77, 1),
(77, 3),
(77, 5),
(77, 6),
(78, 1),
(78, 2),
(78, 3),
(78, 5),
(78, 6),
(78, 7),
(79, 1),
(79, 2),
(79, 3),
(79, 5),
(79, 6),
(79, 7),
(80, 1),
(80, 2),
(80, 3),
(80, 5),
(80, 6),
(80, 7),
(81, 1),
(81, 3),
(81, 5),
(81, 6),
(86, 1),
(86, 2),
(86, 3),
(86, 5),
(86, 6),
(86, 7),
(89, 1),
(89, 3),
(89, 4),
(89, 5),
(89, 6),
(91, 1),
(91, 4),
(91, 5),
(91, 6),
(91, 7),
(93, 1),
(93, 3),
(93, 5),
(93, 6),
(96, 1),
(96, 2),
(96, 3),
(96, 5),
(96, 6),
(96, 7),
(98, 1),
(98, 3),
(98, 4),
(98, 5),
(98, 6),
(98, 7),
(103, 1),
(103, 3),
(103, 5),
(103, 6),
(103, 7),
(106, 1),
(106, 2),
(106, 3),
(106, 5),
(106, 6),
(106, 7),
(107, 1),
(107, 2),
(107, 3),
(107, 4),
(107, 5),
(107, 6),
(110, 1),
(110, 2),
(110, 3),
(110, 5),
(110, 6),
(110, 7),
(112, 1),
(112, 3),
(112, 5),
(112, 6),
(112, 7),
(113, 1),
(113, 2),
(113, 3),
(113, 5),
(113, 6),
(113, 7),
(114, 1),
(114, 2),
(114, 3),
(114, 5),
(114, 6),
(114, 7),
(115, 1),
(115, 2),
(115, 4),
(115, 5),
(115, 6),
(115, 7),
(117, 1),
(117, 3),
(117, 4),
(117, 5),
(117, 6),
(117, 7),
(119, 1),
(119, 2),
(119, 4),
(119, 5),
(119, 6),
(119, 8),
(124, 1),
(124, 2),
(124, 4),
(124, 5),
(124, 6),
(124, 8),
(126, 1),
(126, 4),
(126, 5),
(126, 6),
(128, 1),
(128, 2),
(128, 4),
(128, 5),
(128, 6),
(128, 7),
(131, 1),
(131, 2),
(131, 3),
(131, 5),
(131, 6),
(131, 7),
(132, 1),
(132, 3),
(132, 4),
(132, 5),
(132, 6),
(133, 1),
(133, 2),
(133, 3),
(133, 4),
(133, 5),
(133, 6),
(134, 1),
(134, 3),
(134, 5),
(134, 6),
(134, 7),
(137, 1),
(137, 3),
(137, 5),
(137, 6),
(138, 1),
(138, 2),
(138, 3),
(138, 5),
(138, 6),
(139, 1),
(139, 3),
(139, 5),
(139, 6),
(140, 1),
(140, 2),
(140, 3),
(140, 5),
(140, 6),
(140, 7),
(141, 1),
(141, 2),
(141, 3),
(141, 4),
(141, 5),
(141, 6),
(142, 1),
(142, 3),
(142, 4),
(142, 5),
(142, 6),
(143, 1),
(143, 2),
(143, 3),
(143, 5),
(143, 6),
(143, 7),
(144, 1),
(144, 2),
(144, 3),
(144, 5),
(144, 6),
(145, 1),
(145, 2),
(145, 3),
(145, 5),
(145, 6),
(145, 7),
(147, 1),
(147, 3),
(147, 5),
(147, 6),
(147, 7),
(148, 1),
(148, 3),
(148, 5),
(148, 6),
(148, 7),
(149, 1),
(149, 3),
(149, 5),
(149, 6),
(149, 7),
(150, 1),
(150, 2),
(150, 3),
(150, 5),
(150, 6),
(150, 7),
(151, 1),
(151, 2),
(151, 3),
(151, 5),
(151, 6),
(151, 7),
(152, 1),
(152, 2),
(152, 3),
(152, 5),
(152, 6),
(152, 7),
(154, 1),
(154, 2),
(154, 3),
(154, 5),
(154, 6),
(155, 1),
(155, 2),
(155, 3),
(155, 5),
(155, 6),
(155, 7),
(156, 1),
(156, 3),
(156, 4),
(156, 5),
(156, 6),
(157, 1),
(157, 2),
(157, 3),
(157, 5),
(157, 6),
(158, 1),
(158, 2),
(158, 3),
(158, 4),
(158, 5),
(158, 6),
(159, 1),
(159, 2),
(159, 3),
(159, 5),
(159, 6),
(159, 7),
(160, 1),
(160, 2),
(160, 3),
(160, 4),
(160, 5),
(160, 6),
(160, 7),
(162, 1),
(162, 2),
(162, 3),
(162, 5),
(162, 6),
(163, 1),
(163, 3),
(163, 4),
(163, 5),
(163, 6),
(164, 1),
(164, 2),
(164, 3),
(164, 4),
(164, 5),
(164, 6),
(165, 1),
(165, 2),
(165, 3),
(165, 4),
(165, 5),
(165, 6),
(166, 1),
(166, 3),
(166, 4),
(166, 5),
(166, 6),
(167, 1),
(167, 2),
(167, 3),
(167, 5),
(167, 6),
(167, 7),
(168, 1),
(168, 2),
(168, 3),
(168, 5),
(168, 6),
(168, 7),
(170, 1),
(170, 2),
(170, 3),
(170, 5),
(170, 6),
(171, 1),
(171, 3),
(171, 4),
(171, 5),
(171, 6),
(172, 1),
(172, 2),
(172, 3),
(172, 5),
(172, 6),
(172, 7),
(174, 1),
(174, 2),
(174, 3),
(174, 5),
(174, 6),
(174, 7),
(175, 1),
(175, 2),
(175, 3),
(175, 4),
(175, 5),
(175, 6),
(175, 7),
(176, 1),
(176, 2),
(176, 3),
(176, 5),
(176, 6),
(176, 7),
(177, 1),
(177, 2),
(177, 3),
(177, 5),
(177, 6),
(177, 7),
(179, 1),
(179, 2),
(179, 3),
(179, 5),
(179, 6),
(179, 8),
(183, 1),
(183, 2),
(183, 3),
(183, 4),
(183, 5),
(183, 6),
(183, 7),
(184, 1),
(184, 2),
(184, 3),
(184, 5),
(184, 6),
(184, 7),
(185, 1),
(185, 2),
(185, 3),
(185, 4),
(185, 5),
(185, 6),
(185, 7),
(186, 1),
(186, 2),
(186, 3),
(186, 4),
(186, 5),
(186, 6),
(186, 7),
(187, 1),
(187, 2),
(187, 3),
(187, 4),
(187, 5),
(187, 6),
(187, 7),
(187, 9),
(188, 1),
(188, 3),
(188, 5),
(188, 6),
(188, 7),
(189, 1),
(189, 2),
(189, 3),
(189, 5),
(189, 6),
(190, 1),
(190, 2),
(190, 3),
(190, 4),
(190, 5),
(190, 6),
(191, 1),
(191, 2),
(191, 3),
(191, 4),
(191, 5),
(191, 6),
(192, 1),
(192, 2),
(192, 3),
(192, 5),
(192, 6),
(192, 7),
(193, 1),
(193, 2),
(193, 3),
(193, 5),
(193, 6),
(193, 7),
(194, 1),
(194, 3),
(194, 5),
(194, 6),
(195, 1),
(195, 2),
(195, 3),
(195, 5),
(195, 6),
(195, 7),
(196, 1),
(196, 3),
(196, 5),
(196, 6),
(196, 7),
(197, 1),
(197, 2),
(197, 3),
(197, 4),
(197, 5),
(197, 6),
(198, 1),
(198, 3),
(198, 5),
(198, 6),
(199, 1),
(199, 2),
(199, 3),
(199, 5),
(199, 6),
(199, 7),
(200, 1),
(200, 2),
(200, 3),
(200, 5),
(200, 6),
(200, 7),
(201, 1),
(201, 2),
(201, 3),
(201, 4),
(201, 5),
(201, 6),
(202, 1),
(202, 2),
(202, 3),
(202, 5),
(202, 6),
(203, 1),
(203, 2),
(203, 3),
(203, 5),
(203, 6),
(203, 7),
(204, 1),
(204, 2),
(204, 3),
(204, 5),
(204, 6),
(204, 7),
(205, 1),
(205, 3),
(205, 4),
(205, 5),
(205, 6),
(206, 1),
(206, 3),
(206, 4),
(206, 5),
(206, 6),
(207, 1),
(207, 2),
(207, 3),
(207, 4),
(207, 5),
(207, 6),
(207, 7),
(208, 1),
(208, 3),
(208, 4),
(208, 5),
(208, 6),
(209, 1),
(209, 2),
(209, 3),
(209, 5),
(209, 6),
(209, 7),
(210, 1),
(210, 2),
(210, 3),
(210, 4),
(210, 5),
(210, 6),
(210, 7),
(211, 1),
(211, 2),
(211, 3),
(211, 4),
(211, 5),
(211, 6),
(212, 1),
(212, 2),
(212, 3),
(212, 4),
(212, 5),
(212, 6),
(213, 1),
(213, 2),
(213, 3),
(213, 5),
(213, 6),
(213, 7),
(214, 1),
(214, 3),
(214, 4),
(214, 5),
(214, 6),
(215, 1),
(215, 3),
(215, 4),
(215, 5),
(215, 6),
(215, 7),
(216, 1),
(216, 3),
(216, 4),
(216, 5),
(216, 6),
(216, 7),
(217, 1),
(217, 2),
(217, 3),
(217, 4),
(217, 5),
(217, 6),
(217, 7),
(218, 1),
(218, 2),
(218, 3),
(218, 4),
(218, 5),
(218, 6),
(219, 1),
(219, 2),
(219, 3),
(219, 4),
(219, 5),
(219, 6),
(220, 1),
(220, 2),
(220, 3),
(220, 4),
(220, 5),
(220, 6),
(221, 1),
(221, 2),
(221, 3),
(221, 4),
(221, 5),
(221, 6),
(223, 1),
(223, 2),
(223, 3),
(223, 5),
(223, 6),
(224, 1),
(224, 2),
(224, 3),
(224, 5),
(224, 6),
(224, 7),
(225, 1),
(225, 2),
(225, 3),
(225, 5),
(225, 6),
(225, 7),
(226, 1),
(226, 3),
(226, 5),
(226, 6),
(226, 7),
(227, 1),
(227, 2),
(227, 3),
(227, 5),
(227, 6),
(227, 7),
(228, 1),
(228, 3),
(228, 4),
(228, 5),
(228, 6),
(229, 1),
(229, 2),
(229, 4),
(229, 5),
(229, 6),
(229, 7),
(230, 1),
(230, 2),
(230, 3),
(230, 4),
(230, 5),
(230, 6),
(230, 7);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `instruments_users`
--

CREATE TABLE `instruments_users` (
  `user_id` char(36) NOT NULL,
  `instrument_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `instruments_users`
--

INSERT INTO `instruments_users` (`user_id`, `instrument_id`) VALUES
('03198792-5c0d-4473-b218-447a2ec7f1fd', 1),
('03198792-5c0d-4473-b218-447a2ec7f1fd', 2),
('03198792-5c0d-4473-b218-447a2ec7f1fd', 3),
('03198792-5c0d-4473-b218-447a2ec7f1fd', 4),
('03198792-5c0d-4473-b218-447a2ec7f1fd', 5),
('05388740-d3f7-4308-9d9d-16039387cab7', 2),
('05388740-d3f7-4308-9d9d-16039387cab7', 4),
('05388740-d3f7-4308-9d9d-16039387cab7', 6),
('078e48a4-5b2a-410f-8bef-33babe2378c3', 1),
('078e48a4-5b2a-410f-8bef-33babe2378c3', 2),
('078e48a4-5b2a-410f-8bef-33babe2378c3', 4),
('0bf111e7-6ff1-4261-96e7-3f33cfa8d4aa', 6),
('1310f7d5-0bf8-4406-b57b-12a588cf0e18', 2),
('1310f7d5-0bf8-4406-b57b-12a588cf0e18', 6),
('15685769-e355-421a-ab82-c19f0efcce87', 1),
('15685769-e355-421a-ab82-c19f0efcce87', 2),
('159775bb-fd04-4560-b12d-ded09b36ebd8', 5),
('18f7f0b3-e58c-49e2-ab95-fefe897de617', 1),
('18f7f0b3-e58c-49e2-ab95-fefe897de617', 2),
('1d7ab30b-16ec-470a-aead-7f51346dcebb', 6),
('1e93668c-8b5d-4988-a3d1-bffcd8a7417e', 2),
('1e93668c-8b5d-4988-a3d1-bffcd8a7417e', 6),
('33d81274-7b8a-48ad-82cf-3087de06dac0', 2),
('33d81274-7b8a-48ad-82cf-3087de06dac0', 5),
('3c4c8b18-c991-43f6-a484-23be4b42565e', 1),
('3c4c8b18-c991-43f6-a484-23be4b42565e', 2),
('3c4c8b18-c991-43f6-a484-23be4b42565e', 4),
('3e3d7f52-db85-473d-9c83-3d0d8f7ef9f0', 2),
('3e3d7f52-db85-473d-9c83-3d0d8f7ef9f0', 6),
('4303263a-0b6c-40ff-95f6-c3563de7a013', 2),
('4303263a-0b6c-40ff-95f6-c3563de7a013', 3),
('4303263a-0b6c-40ff-95f6-c3563de7a013', 4),
('4de03e76-2628-4061-889e-19911f7cc01e', 3),
('4de03e76-2628-4061-889e-19911f7cc01e', 4),
('53f2bfa4-e4aa-429d-b177-2dea148fdeb3', 2),
('53f2bfa4-e4aa-429d-b177-2dea148fdeb3', 3),
('53f2bfa4-e4aa-429d-b177-2dea148fdeb3', 4),
('56588347-a0d7-4424-ad26-fc4fcba747eb', 1),
('56588347-a0d7-4424-ad26-fc4fcba747eb', 2),
('56588347-a0d7-4424-ad26-fc4fcba747eb', 5),
('56c7c858-df60-49a9-9bd9-f5040a1f952a', 3),
('56c7c858-df60-49a9-9bd9-f5040a1f952a', 4),
('58f41b46-8897-4ac0-8e33-a4c79172c9fb', 7),
('594260bf-cd34-4c8a-b9fd-58b29bcb2842', 1),
('594260bf-cd34-4c8a-b9fd-58b29bcb2842', 2),
('594260bf-cd34-4c8a-b9fd-58b29bcb2842', 4),
('594260bf-cd34-4c8a-b9fd-58b29bcb2842', 5),
('594260bf-cd34-4c8a-b9fd-58b29bcb2842', 7),
('5a3d04ca-acf8-4110-95e8-6f8f79232060', 2),
('5a3d04ca-acf8-4110-95e8-6f8f79232060', 3),
('5a3d04ca-acf8-4110-95e8-6f8f79232060', 4),
('5c16538a-0d0a-4183-b2ea-a4cb2cfcfe2a', 7),
('5c2f81fd-780f-4fd0-99af-23a813ea79b3', 2),
('5c2f81fd-780f-4fd0-99af-23a813ea79b3', 3),
('5c2f81fd-780f-4fd0-99af-23a813ea79b3', 4),
('5cbbd8ab-32a8-41b4-8633-b2302a73f60d', 2),
('5cbbd8ab-32a8-41b4-8633-b2302a73f60d', 3),
('5cbbd8ab-32a8-41b4-8633-b2302a73f60d', 4),
('5cbbd8ab-32a8-41b4-8633-b2302a73f60d', 7),
('6192448a-c6cf-4ffa-92c7-d0b11b74f629', 1),
('6192448a-c6cf-4ffa-92c7-d0b11b74f629', 2),
('63d24792-9c5a-45e7-af41-19184caa3fff', 2),
('63d24792-9c5a-45e7-af41-19184caa3fff', 5),
('682cf312-9a70-4b40-a7e3-6dd6d4afb25e', 2),
('682cf312-9a70-4b40-a7e3-6dd6d4afb25e', 7),
('682cf312-9a70-4b40-a7e3-6dd6d4afb25e', 9),
('697fc2e6-dfcf-4c23-901d-ef12d760491b', 1),
('697fc2e6-dfcf-4c23-901d-ef12d760491b', 2),
('6ae6a5b1-88b1-48fc-923d-f4dbe6b4c929', 2),
('6ae6a5b1-88b1-48fc-923d-f4dbe6b4c929', 5),
('707a332a-7931-4527-9a1f-cfc3f114e1d7', 1),
('707a332a-7931-4527-9a1f-cfc3f114e1d7', 2),
('707a332a-7931-4527-9a1f-cfc3f114e1d7', 4),
('7858508c-b4fe-4004-982e-0e88ea4ad4a2', 2),
('7858508c-b4fe-4004-982e-0e88ea4ad4a2', 6),
('8c052cad-30aa-4049-91d0-5b2429d7c581', 2),
('8c052cad-30aa-4049-91d0-5b2429d7c581', 3),
('8c052cad-30aa-4049-91d0-5b2429d7c581', 4),
('8c052cad-30aa-4049-91d0-5b2429d7c581', 7),
('8c052cad-30aa-4049-91d0-5b2429d7c581', 8),
('9030222b-8d63-414e-ba94-ca7efd14ac3e', 3),
('9030222b-8d63-414e-ba94-ca7efd14ac3e', 4),
('9610c544-4b60-4f77-8100-bfb1b2b77018', 7),
('9a0ae66b-7f0a-4644-ab3b-bbc338387185', 8),
('afa7577f-59bb-4df0-b750-fd9dea6ed46a', 2),
('afa7577f-59bb-4df0-b750-fd9dea6ed46a', 7),
('b2348b12-f6b7-4758-accd-fc0291e1cec8', 1),
('b2348b12-f6b7-4758-accd-fc0291e1cec8', 2),
('b2efde4c-e55e-446d-9146-8cfbaeca72c3', 1),
('b2efde4c-e55e-446d-9146-8cfbaeca72c3', 2),
('b2efde4c-e55e-446d-9146-8cfbaeca72c3', 5),
('b57e7c2f-9f6f-49cc-a99d-fad0da0de1c0', 3),
('b57e7c2f-9f6f-49cc-a99d-fad0da0de1c0', 4),
('ce797503-e948-4bac-b53f-198fca6fbbd4', 6),
('cfb9a2db-45ec-472f-a749-e8c6f48e266d', 2),
('cfb9a2db-45ec-472f-a749-e8c6f48e266d', 5),
('d12c031e-3628-4585-9c11-0557f086b835', 2),
('d12c031e-3628-4585-9c11-0557f086b835', 3),
('d12c031e-3628-4585-9c11-0557f086b835', 4),
('d2b4a380-de23-4823-b6b3-7117eb950b56', 6),
('d882f52c-7e98-485c-b483-2a320a6e7981', 1),
('d882f52c-7e98-485c-b483-2a320a6e7981', 2),
('e1a1051a-24d8-4960-a9ee-84be21b6cfa2', 2),
('e1a1051a-24d8-4960-a9ee-84be21b6cfa2', 3),
('e1a1051a-24d8-4960-a9ee-84be21b6cfa2', 4),
('e1a1051a-24d8-4960-a9ee-84be21b6cfa2', 5),
('e1a1051a-24d8-4960-a9ee-84be21b6cfa2', 7),
('e1fc5fc5-c42c-4bb5-97c0-d88a75e61f9a', 1),
('e1fc5fc5-c42c-4bb5-97c0-d88a75e61f9a', 2),
('e1fc5fc5-c42c-4bb5-97c0-d88a75e61f9a', 3),
('e1fc5fc5-c42c-4bb5-97c0-d88a75e61f9a', 4),
('e1fc5fc5-c42c-4bb5-97c0-d88a75e61f9a', 5),
('e1fc5fc5-c42c-4bb5-97c0-d88a75e61f9a', 6),
('e1fc5fc5-c42c-4bb5-97c0-d88a75e61f9a', 7),
('eed947e5-8779-4e98-9c0a-b283465963d1', 1),
('eed947e5-8779-4e98-9c0a-b283465963d1', 2),
('eed947e5-8779-4e98-9c0a-b283465963d1', 6),
('eed947e5-8779-4e98-9c0a-b283465963d1', 7),
('efcf62f6-62e5-44ed-becd-af853db69a5f', 2),
('efcf62f6-62e5-44ed-becd-af853db69a5f', 5),
('f26b98db-4590-4335-a3ad-e2340b8b2de3', 2),
('f26b98db-4590-4335-a3ad-e2340b8b2de3', 6),
('f5616091-2207-4e7f-baab-1ffabff8301f', 1),
('f5616091-2207-4e7f-baab-1ffabff8301f', 2),
('f5616091-2207-4e7f-baab-1ffabff8301f', 3),
('f5616091-2207-4e7f-baab-1ffabff8301f', 4),
('f8265210-f8dc-4832-90fd-7625a1a76461', 1),
('f8265210-f8dc-4832-90fd-7625a1a76461', 2),
('f8265210-f8dc-4832-90fd-7625a1a76461', 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `schedules`
--

CREATE TABLE `schedules` (
  `id` bigint(20) NOT NULL,
  `user_id` char(36) NOT NULL,
  `edition_id` int(10) NOT NULL,
  `available_schedules_id` bigint(20) NOT NULL,
  `start` datetime DEFAULT NULL,
  `end` datetime DEFAULT NULL,
  `unavailable` tinyint(1) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `schedules`
--

INSERT INTO `schedules` (`id`, `user_id`, `edition_id`, `available_schedules_id`, `start`, `end`, `unavailable`, `created`, `updated`) VALUES
(1, '3e3d7f52-db85-473d-9c83-3d0d8f7ef9f0', 4, 1, '2019-11-02 10:30:00', '2019-11-02 14:30:00', 0, '2019-10-18 13:03:33', NULL),
(2, '3e3d7f52-db85-473d-9c83-3d0d8f7ef9f0', 4, 2, '2019-11-02 16:00:00', '2019-11-02 18:00:00', 0, '2019-10-18 13:03:33', NULL),
(3, '3e3d7f52-db85-473d-9c83-3d0d8f7ef9f0', 4, 3, '2019-11-03 11:00:00', '2019-11-03 14:30:00', 0, '2019-10-18 13:03:33', NULL),
(4, '3e3d7f52-db85-473d-9c83-3d0d8f7ef9f0', 4, 4, '2019-11-03 16:00:00', '2019-11-03 22:30:00', 0, '2019-10-18 13:03:33', NULL),
(5, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 4, 1, NULL, NULL, 1, '2019-10-18 13:56:15', NULL),
(6, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 4, 2, NULL, NULL, 1, '2019-10-18 13:56:15', NULL),
(7, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 4, 3, NULL, NULL, 1, '2019-10-18 13:56:15', NULL),
(8, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 4, 4, NULL, NULL, 1, '2019-10-18 13:56:15', NULL),
(9, '30dbfe50-099a-46df-b8fb-caf4e6983c0d', 4, 1, NULL, NULL, 1, '2019-10-18 18:59:47', NULL),
(10, '30dbfe50-099a-46df-b8fb-caf4e6983c0d', 4, 2, NULL, NULL, 1, '2019-10-18 18:59:47', NULL),
(11, '30dbfe50-099a-46df-b8fb-caf4e6983c0d', 4, 3, NULL, NULL, 1, '2019-10-18 18:59:47', NULL),
(12, '30dbfe50-099a-46df-b8fb-caf4e6983c0d', 4, 4, NULL, NULL, 1, '2019-10-18 18:59:47', NULL),
(13, '6192448a-c6cf-4ffa-92c7-d0b11b74f629', 4, 1, '2019-11-02 11:00:00', '2019-11-02 14:30:00', 0, '2019-10-18 21:20:29', NULL),
(14, '6192448a-c6cf-4ffa-92c7-d0b11b74f629', 4, 2, '2019-11-02 16:00:00', '2019-11-02 22:30:00', 0, '2019-10-18 21:20:30', NULL),
(15, '6192448a-c6cf-4ffa-92c7-d0b11b74f629', 4, 3, '2019-11-03 11:00:00', '2019-11-03 14:30:00', 0, '2019-10-18 21:20:30', NULL),
(16, '6192448a-c6cf-4ffa-92c7-d0b11b74f629', 4, 4, '2019-11-03 16:00:00', '2019-11-03 22:30:00', 0, '2019-10-18 21:20:30', NULL),
(17, 'b2348b12-f6b7-4758-accd-fc0291e1cec8', 4, 1, NULL, NULL, 1, '2019-10-19 07:35:32', NULL),
(18, 'b2348b12-f6b7-4758-accd-fc0291e1cec8', 4, 2, '2019-11-02 16:00:00', '2019-11-02 22:30:00', 0, '2019-10-19 07:35:32', NULL),
(19, 'b2348b12-f6b7-4758-accd-fc0291e1cec8', 4, 3, '2019-11-03 10:30:00', '2019-11-03 14:30:00', 0, '2019-10-19 07:35:32', NULL),
(20, 'b2348b12-f6b7-4758-accd-fc0291e1cec8', 4, 4, '2019-11-03 16:00:00', '2019-11-03 22:30:00', 0, '2019-10-19 07:35:32', NULL),
(21, '697fc2e6-dfcf-4c23-901d-ef12d760491b', 4, 1, NULL, NULL, 1, '2019-10-19 07:38:42', NULL),
(22, '697fc2e6-dfcf-4c23-901d-ef12d760491b', 4, 2, NULL, NULL, 1, '2019-10-19 07:38:42', NULL),
(23, '697fc2e6-dfcf-4c23-901d-ef12d760491b', 4, 3, '2019-11-03 10:30:00', '2019-11-03 14:30:00', 0, '2019-10-19 07:38:42', NULL),
(24, '697fc2e6-dfcf-4c23-901d-ef12d760491b', 4, 4, '2019-11-03 16:00:00', '2019-11-03 22:30:00', 0, '2019-10-19 07:38:42', NULL),
(25, 'd882f52c-7e98-485c-b483-2a320a6e7981', 4, 1, '2019-11-02 11:30:00', '2019-11-02 14:00:00', 0, '2019-10-19 07:41:00', NULL),
(26, 'd882f52c-7e98-485c-b483-2a320a6e7981', 4, 2, '2019-11-02 16:30:00', '2019-11-02 21:00:00', 0, '2019-10-19 07:41:00', NULL),
(27, 'd882f52c-7e98-485c-b483-2a320a6e7981', 4, 3, '2019-11-03 12:00:00', '2019-11-03 14:00:00', 0, '2019-10-19 07:41:00', NULL),
(28, 'd882f52c-7e98-485c-b483-2a320a6e7981', 4, 4, NULL, NULL, 1, '2019-10-19 07:41:00', NULL),
(29, '1e93668c-8b5d-4988-a3d1-bffcd8a7417e', 4, 1, '2019-11-02 10:30:00', '2019-11-02 14:30:00', 0, '2019-10-19 07:41:11', NULL),
(30, '1e93668c-8b5d-4988-a3d1-bffcd8a7417e', 4, 2, '2019-11-02 16:00:00', '2019-11-02 22:30:00', 0, '2019-10-19 07:41:11', NULL),
(31, '1e93668c-8b5d-4988-a3d1-bffcd8a7417e', 4, 3, '2019-11-03 10:30:00', '2019-11-03 14:30:00', 0, '2019-10-19 07:41:11', NULL),
(32, '1e93668c-8b5d-4988-a3d1-bffcd8a7417e', 4, 4, '2019-11-03 16:00:00', '2019-11-03 22:30:00', 0, '2019-10-19 07:41:11', NULL),
(33, '3c4c8b18-c991-43f6-a484-23be4b42565e', 4, 1, '2019-11-02 10:30:00', '2019-11-02 13:45:00', 0, '2019-10-19 07:42:56', NULL),
(34, '3c4c8b18-c991-43f6-a484-23be4b42565e', 4, 2, '2019-11-02 16:00:00', '2019-11-02 21:00:00', 0, '2019-10-19 07:42:56', NULL),
(35, '3c4c8b18-c991-43f6-a484-23be4b42565e', 4, 3, '2019-11-03 10:30:00', '2019-11-03 13:45:00', 0, '2019-10-19 07:42:56', NULL),
(36, '3c4c8b18-c991-43f6-a484-23be4b42565e', 4, 4, '2019-11-03 16:00:00', '2019-11-03 21:00:00', 0, '2019-10-19 07:42:56', NULL),
(37, '078e48a4-5b2a-410f-8bef-33babe2378c3', 4, 1, '2019-11-02 10:30:00', '2019-11-02 14:30:00', 0, '2019-10-19 07:43:36', NULL),
(38, '078e48a4-5b2a-410f-8bef-33babe2378c3', 4, 2, '2019-11-02 16:00:00', '2019-11-02 22:30:00', 0, '2019-10-19 07:43:36', NULL),
(39, '078e48a4-5b2a-410f-8bef-33babe2378c3', 4, 3, '2019-11-03 10:30:00', '2019-11-03 14:30:00', 0, '2019-10-19 07:43:36', NULL),
(40, '078e48a4-5b2a-410f-8bef-33babe2378c3', 4, 4, '2019-11-03 16:00:00', '2019-11-03 22:30:00', 0, '2019-10-19 07:43:36', NULL),
(41, '6ae6a5b1-88b1-48fc-923d-f4dbe6b4c929', 4, 1, '2019-11-02 12:30:00', '2019-11-02 14:30:00', 0, '2019-10-19 07:50:11', NULL),
(42, '6ae6a5b1-88b1-48fc-923d-f4dbe6b4c929', 4, 2, '2019-11-02 16:00:00', '2019-11-02 22:30:00', 0, '2019-10-19 07:50:11', NULL),
(43, '6ae6a5b1-88b1-48fc-923d-f4dbe6b4c929', 4, 3, '2019-11-03 11:30:00', '2019-11-03 14:30:00', 0, '2019-10-19 07:50:11', NULL),
(44, '6ae6a5b1-88b1-48fc-923d-f4dbe6b4c929', 4, 4, '2019-11-03 16:00:00', '2019-11-03 22:30:00', 0, '2019-10-19 07:50:11', NULL),
(45, '15685769-e355-421a-ab82-c19f0efcce87', 4, 1, '2019-11-02 10:30:00', '2019-11-02 14:30:00', 0, '2019-10-19 07:55:14', NULL),
(46, '15685769-e355-421a-ab82-c19f0efcce87', 4, 2, '2019-11-02 16:00:00', '2019-11-02 19:30:00', 0, '2019-10-19 07:55:14', NULL),
(47, '15685769-e355-421a-ab82-c19f0efcce87', 4, 3, '2019-11-03 10:30:00', '2019-11-03 14:30:00', 0, '2019-10-19 07:55:14', NULL),
(48, '15685769-e355-421a-ab82-c19f0efcce87', 4, 4, '2019-11-03 16:00:00', '2019-11-03 22:30:00', 0, '2019-10-19 07:55:14', NULL),
(49, 'd12c031e-3628-4585-9c11-0557f086b835', 4, 1, '2019-11-02 10:30:00', '2019-11-02 12:30:00', 0, '2019-10-19 08:00:31', NULL),
(50, 'd12c031e-3628-4585-9c11-0557f086b835', 4, 2, NULL, NULL, 1, '2019-10-19 08:00:31', NULL),
(51, 'd12c031e-3628-4585-9c11-0557f086b835', 4, 3, '2019-11-03 10:30:00', '2019-11-03 14:00:00', 0, '2019-10-19 08:00:31', NULL),
(52, 'd12c031e-3628-4585-9c11-0557f086b835', 4, 4, '2019-11-03 17:00:00', '2019-11-03 22:30:00', 0, '2019-10-19 08:00:31', NULL),
(53, 'f26b98db-4590-4335-a3ad-e2340b8b2de3', 4, 1, '2019-11-02 10:30:00', '2019-11-02 14:30:00', 0, '2019-10-19 08:01:37', NULL),
(54, 'f26b98db-4590-4335-a3ad-e2340b8b2de3', 4, 2, '2019-11-02 16:00:00', '2019-11-02 22:30:00', 0, '2019-10-19 08:01:37', NULL),
(55, 'f26b98db-4590-4335-a3ad-e2340b8b2de3', 4, 3, '2019-11-03 10:30:00', '2019-11-03 14:30:00', 0, '2019-10-19 08:01:37', NULL),
(56, 'f26b98db-4590-4335-a3ad-e2340b8b2de3', 4, 4, '2019-11-03 16:00:00', '2019-11-03 22:30:00', 0, '2019-10-19 08:01:37', NULL),
(57, '56588347-a0d7-4424-ad26-fc4fcba747eb', 4, 1, NULL, NULL, 1, '2019-10-19 08:04:29', NULL),
(58, '56588347-a0d7-4424-ad26-fc4fcba747eb', 4, 2, NULL, NULL, 1, '2019-10-19 08:04:29', NULL),
(59, '56588347-a0d7-4424-ad26-fc4fcba747eb', 4, 3, '2019-11-03 10:30:00', '2019-11-03 14:30:00', 0, '2019-10-19 08:04:29', NULL),
(60, '56588347-a0d7-4424-ad26-fc4fcba747eb', 4, 4, '2019-11-03 16:00:00', '2019-11-03 22:30:00', 0, '2019-10-19 08:04:29', NULL),
(61, '7858508c-b4fe-4004-982e-0e88ea4ad4a2', 4, 1, '2019-11-02 13:00:00', '2019-11-02 14:30:00', 0, '2019-10-19 08:07:59', NULL),
(62, '7858508c-b4fe-4004-982e-0e88ea4ad4a2', 4, 2, '2019-11-02 16:00:00', '2019-11-02 22:30:00', 0, '2019-10-19 08:07:59', NULL),
(63, '7858508c-b4fe-4004-982e-0e88ea4ad4a2', 4, 3, '2019-11-03 10:30:00', '2019-11-03 14:30:00', 0, '2019-10-19 08:07:59', NULL),
(64, '7858508c-b4fe-4004-982e-0e88ea4ad4a2', 4, 4, '2019-11-03 16:00:00', '2019-11-03 22:30:00', 0, '2019-10-19 08:07:59', NULL),
(65, '5c2f81fd-780f-4fd0-99af-23a813ea79b3', 4, 1, '2019-11-02 10:30:00', '2019-11-02 13:30:00', 0, '2019-10-19 08:52:51', NULL),
(66, '5c2f81fd-780f-4fd0-99af-23a813ea79b3', 4, 2, '2019-11-02 16:00:00', '2019-11-02 18:00:00', 0, '2019-10-19 08:52:51', NULL),
(67, '5c2f81fd-780f-4fd0-99af-23a813ea79b3', 4, 3, '2019-11-03 10:30:00', '2019-11-03 14:30:00', 0, '2019-10-19 08:52:51', NULL),
(68, '5c2f81fd-780f-4fd0-99af-23a813ea79b3', 4, 4, NULL, NULL, 1, '2019-10-19 08:52:51', NULL),
(69, '4303263a-0b6c-40ff-95f6-c3563de7a013', 4, 1, '2019-11-02 10:30:00', '2019-11-02 14:30:00', 0, '2019-10-19 09:25:18', NULL),
(70, '4303263a-0b6c-40ff-95f6-c3563de7a013', 4, 2, '2019-11-02 16:00:00', '2019-11-02 22:30:00', 0, '2019-10-19 09:25:18', NULL),
(71, '4303263a-0b6c-40ff-95f6-c3563de7a013', 4, 3, '2019-11-03 10:30:00', '2019-11-03 14:30:00', 0, '2019-10-19 09:25:18', NULL),
(72, '4303263a-0b6c-40ff-95f6-c3563de7a013', 4, 4, '2019-11-03 16:00:00', '2019-11-03 22:30:00', 0, '2019-10-19 09:25:18', NULL),
(73, 'e1a1051a-24d8-4960-a9ee-84be21b6cfa2', 4, 1, '2019-11-02 10:30:00', '2019-11-02 14:30:00', 0, '2019-10-19 10:11:53', NULL),
(74, 'e1a1051a-24d8-4960-a9ee-84be21b6cfa2', 4, 2, '2019-11-02 16:00:00', '2019-11-02 22:30:00', 0, '2019-10-19 10:11:53', NULL),
(75, 'e1a1051a-24d8-4960-a9ee-84be21b6cfa2', 4, 3, '2019-11-03 10:30:00', '2019-11-03 14:30:00', 0, '2019-10-19 10:11:53', NULL),
(76, 'e1a1051a-24d8-4960-a9ee-84be21b6cfa2', 4, 4, '2019-11-03 16:00:00', '2019-11-03 22:30:00', 0, '2019-10-19 10:11:53', NULL),
(77, '18f7f0b3-e58c-49e2-ab95-fefe897de617', 4, 1, '2019-11-02 10:30:00', '2019-11-02 14:00:00', 0, '2019-10-19 10:40:56', NULL),
(78, '18f7f0b3-e58c-49e2-ab95-fefe897de617', 4, 2, '2019-11-02 16:00:00', '2019-11-02 22:30:00', 0, '2019-10-19 10:40:56', NULL),
(79, '18f7f0b3-e58c-49e2-ab95-fefe897de617', 4, 3, '2019-11-03 10:30:00', '2019-11-03 14:00:00', 0, '2019-10-19 10:40:56', NULL),
(80, '18f7f0b3-e58c-49e2-ab95-fefe897de617', 4, 4, '2019-11-03 16:00:00', '2019-11-03 22:30:00', 0, '2019-10-19 10:40:56', NULL),
(81, '05388740-d3f7-4308-9d9d-16039387cab7', 4, 1, '2019-11-02 10:30:00', '2019-11-02 14:30:00', 0, '2019-10-19 10:53:28', NULL),
(82, '05388740-d3f7-4308-9d9d-16039387cab7', 4, 2, '2019-11-02 16:00:00', '2019-11-02 22:30:00', 0, '2019-10-19 10:53:28', NULL),
(83, '05388740-d3f7-4308-9d9d-16039387cab7', 4, 3, '2019-11-03 10:30:00', '2019-11-03 14:30:00', 0, '2019-10-19 10:53:28', NULL),
(84, '05388740-d3f7-4308-9d9d-16039387cab7', 4, 4, '2019-11-03 16:00:00', '2019-11-03 22:30:00', 0, '2019-10-19 10:53:28', NULL),
(85, '33d81274-7b8a-48ad-82cf-3087de06dac0', 4, 1, '2019-11-02 10:30:00', '2019-11-02 14:30:00', 0, '2019-10-19 11:55:52', NULL),
(86, '33d81274-7b8a-48ad-82cf-3087de06dac0', 4, 2, '2019-11-02 16:00:00', '2019-11-02 22:30:00', 0, '2019-10-19 11:55:52', NULL),
(87, '33d81274-7b8a-48ad-82cf-3087de06dac0', 4, 3, '2019-11-03 10:30:00', '2019-11-03 14:30:00', 0, '2019-10-19 11:55:52', NULL),
(88, '33d81274-7b8a-48ad-82cf-3087de06dac0', 4, 4, '2019-11-03 16:00:00', '2019-11-03 22:30:00', 0, '2019-10-19 11:55:52', NULL),
(89, '63d24792-9c5a-45e7-af41-19184caa3fff', 4, 1, '2019-11-02 10:30:00', '2019-11-02 14:30:00', 0, '2019-10-19 12:33:10', NULL),
(90, '63d24792-9c5a-45e7-af41-19184caa3fff', 4, 2, NULL, NULL, 1, '2019-10-19 12:33:10', NULL),
(91, '63d24792-9c5a-45e7-af41-19184caa3fff', 4, 3, '2019-11-03 10:30:00', '2019-11-03 14:30:00', 0, '2019-10-19 12:33:10', NULL),
(92, '63d24792-9c5a-45e7-af41-19184caa3fff', 4, 4, '2019-11-03 16:00:00', '2019-11-03 22:30:00', 0, '2019-10-19 12:33:10', NULL),
(93, 'cfb9a2db-45ec-472f-a749-e8c6f48e266d', 4, 1, '2019-11-02 10:30:00', '2019-11-02 14:30:00', 0, '2019-10-19 12:38:27', NULL),
(94, 'cfb9a2db-45ec-472f-a749-e8c6f48e266d', 4, 2, '2019-11-02 16:00:00', '2019-11-02 22:30:00', 0, '2019-10-19 12:38:27', NULL),
(95, 'cfb9a2db-45ec-472f-a749-e8c6f48e266d', 4, 3, '2019-11-03 10:30:00', '2019-11-03 14:30:00', 0, '2019-10-19 12:38:27', NULL),
(96, 'cfb9a2db-45ec-472f-a749-e8c6f48e266d', 4, 4, '2019-11-03 16:00:00', '2019-11-03 18:00:00', 0, '2019-10-19 12:38:27', NULL),
(97, '594260bf-cd34-4c8a-b9fd-58b29bcb2842', 4, 1, '2019-11-02 10:30:00', '2019-11-02 14:30:00', 0, '2019-10-19 12:50:57', NULL),
(98, '594260bf-cd34-4c8a-b9fd-58b29bcb2842', 4, 2, '2019-11-02 16:00:00', '2019-11-02 22:30:00', 0, '2019-10-19 12:50:57', NULL),
(99, '594260bf-cd34-4c8a-b9fd-58b29bcb2842', 4, 3, '2019-11-03 10:30:00', '2019-11-03 14:30:00', 0, '2019-10-19 12:50:57', NULL),
(100, '594260bf-cd34-4c8a-b9fd-58b29bcb2842', 4, 4, '2019-11-03 16:00:00', '2019-11-03 22:30:00', 0, '2019-10-19 12:50:57', NULL),
(101, 'efcf62f6-62e5-44ed-becd-af853db69a5f', 4, 1, '2019-11-02 10:30:00', '2019-11-02 12:30:00', 0, '2019-10-19 13:03:18', NULL),
(102, 'efcf62f6-62e5-44ed-becd-af853db69a5f', 4, 2, NULL, NULL, 1, '2019-10-19 13:03:18', NULL),
(103, 'efcf62f6-62e5-44ed-becd-af853db69a5f', 4, 3, '2019-11-03 10:30:00', '2019-11-03 14:30:00', 0, '2019-10-19 13:03:18', NULL),
(104, 'efcf62f6-62e5-44ed-becd-af853db69a5f', 4, 4, '2019-11-03 16:00:00', '2019-11-03 22:30:00', 0, '2019-10-19 13:03:18', NULL),
(105, '5cbbd8ab-32a8-41b4-8633-b2302a73f60d', 4, 1, '2019-11-02 13:30:00', '2019-11-02 14:30:00', 0, '2019-10-19 13:26:58', NULL),
(106, '5cbbd8ab-32a8-41b4-8633-b2302a73f60d', 4, 2, '2019-11-02 16:00:00', '2019-11-02 22:30:00', 0, '2019-10-19 13:26:58', NULL),
(107, '5cbbd8ab-32a8-41b4-8633-b2302a73f60d', 4, 3, '2019-11-03 13:30:00', '2019-11-03 14:30:00', 0, '2019-10-19 13:26:58', NULL),
(108, '5cbbd8ab-32a8-41b4-8633-b2302a73f60d', 4, 4, '2019-11-03 16:00:00', '2019-11-03 22:30:00', 0, '2019-10-19 13:26:58', NULL),
(109, 'b2efde4c-e55e-446d-9146-8cfbaeca72c3', 4, 1, NULL, NULL, 1, '2019-10-19 19:36:33', NULL),
(110, 'b2efde4c-e55e-446d-9146-8cfbaeca72c3', 4, 2, NULL, NULL, 1, '2019-10-19 19:36:33', NULL),
(111, 'b2efde4c-e55e-446d-9146-8cfbaeca72c3', 4, 3, NULL, NULL, 1, '2019-10-19 19:36:33', NULL),
(112, 'b2efde4c-e55e-446d-9146-8cfbaeca72c3', 4, 4, NULL, NULL, 1, '2019-10-19 19:36:33', NULL),
(113, '682cf312-9a70-4b40-a7e3-6dd6d4afb25e', 4, 1, '2019-11-02 10:30:00', '2019-11-02 14:30:00', 0, '2019-10-20 09:58:59', NULL),
(114, '682cf312-9a70-4b40-a7e3-6dd6d4afb25e', 4, 2, '2019-11-02 16:00:00', '2019-11-02 22:30:00', 0, '2019-10-20 09:58:59', NULL),
(115, '682cf312-9a70-4b40-a7e3-6dd6d4afb25e', 4, 3, '2019-11-03 10:30:00', '2019-11-03 14:30:00', 0, '2019-10-20 09:58:59', NULL),
(116, '682cf312-9a70-4b40-a7e3-6dd6d4afb25e', 4, 4, '2019-11-03 16:00:00', '2019-11-03 22:30:00', 0, '2019-10-20 09:58:59', NULL),
(117, '5a3d04ca-acf8-4110-95e8-6f8f79232060', 4, 1, '2019-11-02 10:30:00', '2019-11-02 13:30:00', 0, '2019-10-20 10:08:29', NULL),
(118, '5a3d04ca-acf8-4110-95e8-6f8f79232060', 4, 2, '2019-11-02 19:00:00', '2019-11-02 22:30:00', 0, '2019-10-20 10:08:29', NULL),
(119, '5a3d04ca-acf8-4110-95e8-6f8f79232060', 4, 3, '2019-11-03 10:30:00', '2019-11-03 13:30:00', 0, '2019-10-20 10:08:29', NULL),
(120, '5a3d04ca-acf8-4110-95e8-6f8f79232060', 4, 4, '2019-11-03 18:00:00', '2019-11-03 22:30:00', 0, '2019-10-20 10:08:29', NULL),
(121, 'f8265210-f8dc-4832-90fd-7625a1a76461', 4, 1, '2019-11-02 10:30:00', '2019-11-02 14:30:00', 0, '2019-10-20 16:56:22', NULL),
(122, 'f8265210-f8dc-4832-90fd-7625a1a76461', 4, 2, '2019-11-02 16:00:00', '2019-11-02 22:30:00', 0, '2019-10-20 16:56:22', NULL),
(123, 'f8265210-f8dc-4832-90fd-7625a1a76461', 4, 3, '2019-11-03 10:30:00', '2019-11-03 14:30:00', 0, '2019-10-20 16:56:22', NULL),
(124, 'f8265210-f8dc-4832-90fd-7625a1a76461', 4, 4, '2019-11-03 16:00:00', '2019-11-03 22:30:00', 0, '2019-10-20 16:56:22', NULL),
(125, '1310f7d5-0bf8-4406-b57b-12a588cf0e18', 4, 1, '2019-11-02 10:30:00', '2019-11-02 14:30:00', 0, '2019-10-20 17:03:45', NULL),
(126, '1310f7d5-0bf8-4406-b57b-12a588cf0e18', 4, 2, '2019-11-02 16:00:00', '2019-11-02 22:30:00', 0, '2019-10-20 17:03:45', NULL),
(127, '1310f7d5-0bf8-4406-b57b-12a588cf0e18', 4, 3, '2019-11-03 10:30:00', '2019-11-03 14:30:00', 0, '2019-10-20 17:03:45', NULL),
(128, '1310f7d5-0bf8-4406-b57b-12a588cf0e18', 4, 4, '2019-11-03 16:00:00', '2019-11-03 22:30:00', 0, '2019-10-20 17:03:45', NULL),
(129, 'e1fc5fc5-c42c-4bb5-97c0-d88a75e61f9a', 4, 1, '2019-11-02 10:30:00', '2019-11-02 14:30:00', 0, '2019-10-20 18:25:59', NULL),
(130, 'e1fc5fc5-c42c-4bb5-97c0-d88a75e61f9a', 4, 2, '2019-11-02 16:00:00', '2019-11-02 22:30:00', 0, '2019-10-20 18:25:59', NULL),
(131, 'e1fc5fc5-c42c-4bb5-97c0-d88a75e61f9a', 4, 3, '2019-11-03 10:30:00', '2019-11-03 14:30:00', 0, '2019-10-20 18:25:59', NULL),
(132, 'e1fc5fc5-c42c-4bb5-97c0-d88a75e61f9a', 4, 4, '2019-11-03 16:00:00', '2019-11-03 22:30:00', 0, '2019-10-20 18:25:59', NULL),
(133, '707a332a-7931-4527-9a1f-cfc3f114e1d7', 4, 1, '2019-11-02 10:30:00', '2019-11-02 14:30:00', 0, '2019-10-21 07:22:24', NULL),
(134, '707a332a-7931-4527-9a1f-cfc3f114e1d7', 4, 2, '2019-11-02 16:00:00', '2019-11-02 18:00:00', 0, '2019-10-21 07:22:24', NULL),
(135, '707a332a-7931-4527-9a1f-cfc3f114e1d7', 4, 3, '2019-11-03 10:30:00', '2019-11-03 14:30:00', 0, '2019-10-21 07:22:24', NULL),
(136, '707a332a-7931-4527-9a1f-cfc3f114e1d7', 4, 4, '2019-11-03 16:00:00', '2019-11-03 22:30:00', 0, '2019-10-21 07:22:24', NULL),
(137, '8c052cad-30aa-4049-91d0-5b2429d7c581', 4, 1, NULL, NULL, 1, '2019-10-21 07:22:49', NULL),
(138, '8c052cad-30aa-4049-91d0-5b2429d7c581', 4, 2, '2019-11-02 16:00:00', '2019-11-02 22:30:00', 0, '2019-10-21 07:22:49', NULL),
(139, '8c052cad-30aa-4049-91d0-5b2429d7c581', 4, 3, '2019-11-03 10:30:00', '2019-11-03 14:30:00', 0, '2019-10-21 07:22:49', NULL),
(140, '8c052cad-30aa-4049-91d0-5b2429d7c581', 4, 4, '2019-11-03 16:00:00', '2019-11-03 22:30:00', 0, '2019-10-21 07:22:49', NULL),
(141, '03198792-5c0d-4473-b218-447a2ec7f1fd', 4, 1, NULL, NULL, 1, '2019-10-21 13:22:53', NULL),
(142, '03198792-5c0d-4473-b218-447a2ec7f1fd', 4, 2, '2019-11-02 16:00:00', '2019-11-02 22:30:00', 0, '2019-10-21 13:22:53', NULL),
(143, '03198792-5c0d-4473-b218-447a2ec7f1fd', 4, 3, NULL, NULL, 1, '2019-10-21 13:22:53', NULL),
(144, '03198792-5c0d-4473-b218-447a2ec7f1fd', 4, 4, NULL, NULL, 1, '2019-10-21 13:22:53', NULL),
(145, '1d7ab30b-16ec-470a-aead-7f51346dcebb', 4, 1, '2019-11-02 10:30:00', '2019-11-02 14:30:00', 0, '2019-10-23 13:29:48', NULL),
(146, '1d7ab30b-16ec-470a-aead-7f51346dcebb', 4, 2, '2019-11-02 16:00:00', '2019-11-02 22:30:00', 0, '2019-10-23 13:29:48', NULL),
(147, '1d7ab30b-16ec-470a-aead-7f51346dcebb', 4, 3, NULL, NULL, 1, '2019-10-23 13:29:48', NULL),
(148, '1d7ab30b-16ec-470a-aead-7f51346dcebb', 4, 4, NULL, NULL, 1, '2019-10-23 13:29:48', NULL),
(149, '53f2bfa4-e4aa-429d-b177-2dea148fdeb3', 4, 1, NULL, NULL, 1, '2019-10-25 19:58:51', NULL),
(150, '53f2bfa4-e4aa-429d-b177-2dea148fdeb3', 4, 2, '2019-11-02 16:00:00', '2019-11-02 22:30:00', 0, '2019-10-25 19:58:51', NULL),
(151, '53f2bfa4-e4aa-429d-b177-2dea148fdeb3', 4, 3, NULL, NULL, 1, '2019-10-25 19:58:51', NULL),
(152, '53f2bfa4-e4aa-429d-b177-2dea148fdeb3', 4, 4, '2019-11-03 16:00:00', '2019-11-03 22:30:00', 0, '2019-10-25 19:58:51', NULL),
(153, 'eed947e5-8779-4e98-9c0a-b283465963d1', 4, 1, '2019-11-02 10:30:00', '2019-11-02 14:30:00', 0, '2019-10-26 09:28:27', NULL),
(154, 'eed947e5-8779-4e98-9c0a-b283465963d1', 4, 2, '2019-11-02 16:00:00', '2019-11-02 22:30:00', 0, '2019-10-26 09:28:27', NULL),
(155, 'eed947e5-8779-4e98-9c0a-b283465963d1', 4, 3, NULL, NULL, 1, '2019-10-26 09:28:27', NULL),
(156, 'eed947e5-8779-4e98-9c0a-b283465963d1', 4, 4, NULL, NULL, 1, '2019-10-26 09:28:27', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `schedules_exceptions`
--

CREATE TABLE `schedules_exceptions` (
  `tune_id` int(10) NOT NULL DEFAULT '0',
  `user_id` char(36) NOT NULL DEFAULT '',
  `edition_id` int(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `schedules_exceptions`
--

INSERT INTO `schedules_exceptions` (`tune_id`, `user_id`, `edition_id`) VALUES
(183, '6192448a-c6cf-4ffa-92c7-d0b11b74f629', 4),
(183, '697fc2e6-dfcf-4c23-901d-ef12d760491b', 4),
(183, '707a332a-7931-4527-9a1f-cfc3f114e1d7', 4),
(183, '8c052cad-30aa-4049-91d0-5b2429d7c581', 4),
(183, 'd12c031e-3628-4585-9c11-0557f086b835', 4),
(183, 'd882f52c-7e98-485c-b483-2a320a6e7981', 4),
(183, 'eed947e5-8779-4e98-9c0a-b283465963d1', 4),
(183, 'efcf62f6-62e5-44ed-becd-af853db69a5f', 4),
(183, 'f26b98db-4590-4335-a3ad-e2340b8b2de3', 4),
(184, '1310f7d5-0bf8-4406-b57b-12a588cf0e18', 4),
(184, '3e3d7f52-db85-473d-9c83-3d0d8f7ef9f0', 4),
(184, '697fc2e6-dfcf-4c23-901d-ef12d760491b', 4),
(184, '8c052cad-30aa-4049-91d0-5b2429d7c581', 4),
(184, 'd12c031e-3628-4585-9c11-0557f086b835', 4),
(184, 'f8265210-f8dc-4832-90fd-7625a1a76461', 4),
(189, '15685769-e355-421a-ab82-c19f0efcce87', 4),
(189, '56588347-a0d7-4424-ad26-fc4fcba747eb', 4),
(189, '5c2f81fd-780f-4fd0-99af-23a813ea79b3', 4),
(191, '697fc2e6-dfcf-4c23-901d-ef12d760491b', 4),
(192, '697fc2e6-dfcf-4c23-901d-ef12d760491b', 4),
(192, '8c052cad-30aa-4049-91d0-5b2429d7c581', 4),
(194, '697fc2e6-dfcf-4c23-901d-ef12d760491b', 4),
(195, '697fc2e6-dfcf-4c23-901d-ef12d760491b', 4),
(199, 'b2efde4c-e55e-446d-9146-8cfbaeca72c3', 4),
(202, 'd12c031e-3628-4585-9c11-0557f086b835', 4),
(203, '697fc2e6-dfcf-4c23-901d-ef12d760491b', 4),
(206, '707a332a-7931-4527-9a1f-cfc3f114e1d7', 4),
(219, '15685769-e355-421a-ab82-c19f0efcce87', 4),
(227, 'd12c031e-3628-4585-9c11-0557f086b835', 4),
(229, '56588347-a0d7-4424-ad26-fc4fcba747eb', 4),
(229, 'd12c031e-3628-4585-9c11-0557f086b835', 4),
(230, '56588347-a0d7-4424-ad26-fc4fcba747eb', 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `social_accounts`
--

CREATE TABLE `social_accounts` (
  `id` char(36) NOT NULL,
  `user_id` char(36) NOT NULL,
  `provider` varchar(255) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `reference` varchar(255) NOT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `description` text,
  `link` varchar(255) NOT NULL,
  `token` varchar(500) NOT NULL,
  `token_secret` varchar(500) DEFAULT NULL,
  `token_expires` datetime DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `data` text NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tunes`
--

CREATE TABLE `tunes` (
  `id` bigint(20) NOT NULL,
  `user_id` char(36) NOT NULL,
  `name` varchar(100) NOT NULL,
  `artist` varchar(100) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `selected` int(4) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tunes`
--

INSERT INTO `tunes` (`id`, `user_id`, `name`, `artist`, `link`, `selected`, `created`, `updated`) VALUES
(2, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'Problema sexual', 'Ilegales', 'https://www.youtube.com/watch?v=8t5C2FlrRRI', NULL, '2016-10-19 02:32:55', '2019-10-03 13:36:12'),
(6, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'Embrujada', 'Tino Casal', 'https://www.youtube.com/watch?v=hkTdZH0KVjU', NULL, '2016-10-21 18:03:35', '2016-10-21 18:03:35'),
(7, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'Dinero', 'Obús', 'https://www.youtube.com/watch?v=WPRyBXVoKPc', NULL, '2016-10-21 18:04:52', '2016-10-21 18:04:52'),
(8, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'Quiero ser santa', 'Parálisis Permanente', 'https://www.youtube.com/watch?v=vHWKOxbRyac', NULL, '2016-10-21 18:06:05', '2016-10-21 18:06:05'),
(9, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'Ni tú ni nadie', 'Alaska', 'https://www.youtube.com/watch?v=bSmhZ3umh0Q', 2016, '2016-10-21 18:07:26', '2016-10-21 18:07:26'),
(10, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'Still loving you', 'Scorpions', 'https://www.youtube.com/watch?v=7pOr3dBFAeY', NULL, '2016-10-21 18:09:24', '2016-10-21 18:09:24'),
(11, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'Hush', 'Deep Purple', 'https://www.youtube.com/watch?v=W1PNvopXjbg', 2016, '2016-10-21 18:10:51', '2016-10-21 18:10:51'),
(12, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'Everlong', 'Foo fighters', 'https://www.youtube.com/watch?v=crOZk88eCcg', NULL, '2016-10-21 18:13:59', '2016-10-21 18:13:59'),
(13, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'She', 'Green Day', 'https://www.youtube.com/watch?v=OnF0pkWD2Tc', 2016, '2016-10-21 18:14:13', '2016-10-21 18:14:13'),
(14, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'crash', 'the primitives', 'https://www.youtube.com/watch?v=5JVmV-m4wXg', 2016, '2016-10-21 18:16:31', '2016-10-21 18:16:42'),
(15, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'I\'M So Excited', 'The Pointer Sisters', 'https://www.youtube.com/watch?v=8iwBM_YB1sE', 2017, '2016-10-21 18:17:38', '2017-10-26 14:49:29'),
(16, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'Antichrist', 'Sex Pistols', 'https://www.youtube.com/watch?v=qbmWs6Jf5dc', NULL, '2016-10-21 18:19:17', '2016-10-21 18:19:17'),
(18, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'Jhonny B. Goode', 'Chuck Berry ', 'https://www.youtube.com/watch?v=ZFo8-JqzSCM', 2016, '2016-10-21 18:21:38', '2016-10-21 18:24:39'),
(19, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'Eye of the tiger', 'Survivor ', 'https://www.youtube.com/watch?v=ERT_7u5L0dc', 2016, '2016-10-21 18:22:28', '2016-10-21 18:22:28'),
(20, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'Sabotage', 'Beastie Boys', 'https://www.youtube.com/watch?v=z5rRZdiu1UE', NULL, '2016-10-21 18:24:44', '2016-10-21 18:24:44'),
(21, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'Carrie', 'Europe', 'https://www.youtube.com/watch?v=KmWE9UBFwtY', NULL, '2016-10-21 18:25:35', '2016-10-21 18:26:18'),
(22, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'Heroes', 'David Bowie', 'https://www.youtube.com/watch?v=Tgcc5V9Hu3g', NULL, '2016-10-21 18:27:39', '2016-10-21 18:27:39'),
(23, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'Rebel rebel', 'David Bowie', 'https://www.youtube.com/watch?v=U16Xg_rQZkA', 2016, '2016-10-21 18:28:35', '2016-10-21 18:28:35'),
(24, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'Mejor', 'Los brincos', 'https://www.youtube.com/watch?v=uMH13A6NBaU', 2017, '2016-10-21 18:30:29', '2017-10-26 15:01:14'),
(25, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'Hablame de ti', 'Los Pecos', 'https://www.youtube.com/watch?v=QMcK32w_P0w', NULL, '2016-10-21 18:31:44', '2016-10-21 18:31:44'),
(26, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', ' London calling', 'The Clash', 'https://www.youtube.com/watch?v=EfK-WX2pa8c', NULL, '2016-10-21 18:33:52', '2017-10-26 14:51:47'),
(28, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'I fought the law', 'The clash', 'https://www.youtube.com/watch?v=KsS0cvTxU-8', NULL, '2016-10-21 18:35:19', '2016-10-21 18:35:19'),
(29, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'Start Me Up', 'The Rolling Stones', 'https://www.youtube.com/watch?v=SGyOaCXr8Lw', NULL, '2016-10-21 18:35:38', '2016-10-21 18:35:38'),
(30, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'Hace calor', 'Los rodriguez', 'https://www.youtube.com/watch?v=vDC8D_lgwHU', 2016, '2016-10-21 18:37:39', '2016-10-21 18:38:14'),
(31, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'Even flow', 'Pearl Jam', 'https://www.youtube.com/watch?v=tkbgtVFlyCQ', 2016, '2016-10-21 18:39:00', '2016-10-21 18:39:00'),
(32, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'Necesito más', 'Obús', 'https://www.youtube.com/watch?v=SoRHjg0n31Y', NULL, '2016-10-21 18:40:06', '2016-10-21 18:40:06'),
(33, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'Que güeno que estoy', 'Mojinos Escocios', 'https://www.youtube.com/watch?v=YtGM-9Z2bKQ', NULL, '2016-10-21 18:40:59', '2016-10-21 18:40:59'),
(34, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'Long Tall Sally', 'Little Richard', 'https://www.youtube.com/watch?v=eFFgbc5Vcbw', NULL, '2016-10-21 18:41:53', '2016-10-21 18:41:53'),
(35, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'Hound dog', 'Elvis presley', 'https://www.youtube.com/watch?v=4HWhrPaDTj4', 2016, '2016-10-21 18:42:36', '2016-10-24 15:45:49'),
(36, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'I can dream', 'Skunk Anansie', 'https://www.youtube.com/watch?v=MJRIY9U4zhQ', NULL, '2016-10-21 18:44:39', '2016-10-21 18:44:39'),
(37, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'Gorila dance', 'Melodi', 'https://www.youtube.com/watch?v=9JA0mdcfXUQ', NULL, '2016-10-21 18:44:44', '2016-10-21 18:44:44'),
(38, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'Asereghé', 'Las Ketcchup', 'https://www.youtube.com/watch?v=V0PisGe66mY', 2016, '2016-10-21 18:46:20', '2019-10-03 09:30:02'),
(39, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'Macarena', 'Los del río', 'https://www.youtube.com/watch?v=XiBYM6g8Tck', 2016, '2016-10-21 18:47:19', '2016-10-21 18:47:19'),
(40, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'Azzurro', 'Adriano Celentano', 'https://www.youtube.com/watch?v=Q1VGoKBKR3I', NULL, '2016-10-21 18:48:38', '2016-10-21 18:48:38'),
(41, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'Pode ser', 'Victor coyote (xabarin)', 'https://www.youtube.com/watch?v=wGOjQFtvfZ8', NULL, '2016-10-21 18:49:39', '2016-10-21 18:49:39'),
(42, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'I feel good', 'James Brawn', 'https://www.youtube.com/watch?v=U5TqIdff_DQ', NULL, '2016-10-21 18:50:19', '2016-10-21 18:50:19'),
(43, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'Aquí no hay playa', 'The refrescos', 'https://www.youtube.com/watch?v=E8XRN9fpm-0', 2016, '2016-10-21 18:51:58', '2016-10-21 18:51:58'),
(44, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'Ellos las prefieren gordas', 'Orquesta Mondragon', 'https://www.youtube.com/watch?v=vNFU-yJEXM4', 2016, '2016-10-21 18:52:57', '2016-10-21 18:52:57'),
(45, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'Yo tambien necesito amar', 'Ana y Jonny', 'https://www.youtube.com/watch?v=8NMWZET3BWA', NULL, '2016-10-21 18:53:29', '2016-10-21 18:53:29'),
(46, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'Obcequeitor', 'Mamaladilla', 'https://www.youtube.com/watch?v=4Ji_daZjuag', NULL, '2016-10-21 18:54:15', '2016-10-21 18:54:15'),
(47, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'Rock the night', 'Europe', 'https://www.youtube.com/watch?v=QORp2Gy54mI', 2016, '2016-10-21 18:55:49', '2016-10-24 15:38:32'),
(48, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'flashdance', 'flashdance', 'https://www.youtube.com/watch?v=atrqRDgEhsY', NULL, '2016-10-21 18:57:24', '2016-10-21 18:57:24'),
(49, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'You\'re the one that I want', 'Grease', 'https://www.youtube.com/watch?v=7oKPYe53h78', 2016, '2016-10-21 19:00:29', '2016-10-21 19:00:29'),
(50, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'Purple Rain', 'Prince', 'https://www.youtube.com/watch?v=t5b8U_A25A8', 2016, '2016-10-21 19:01:30', '2016-11-04 15:54:42'),
(51, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'Años 80', 'Los piratas', 'https://www.youtube.com/watch?v=gwPtEwcuffU', NULL, '2016-10-21 19:02:51', '2016-10-21 19:02:51'),
(52, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'Ace Of Spades', 'Motorhead', 'https://www.youtube.com/watch?v=vcf7DnHi54g', 2016, '2016-10-21 19:03:46', '2016-10-21 19:03:46'),
(53, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'Paradise city', 'Guns n roses', 'https://www.youtube.com/watch?v=GHEUsGhUtgg', 2016, '2016-10-21 19:04:31', '2016-10-21 19:04:31'),
(55, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'Cerdo', 'Molotov', 'https://www.youtube.com/watch?v=OlCKJM5OYWM', NULL, '2016-10-21 19:06:40', '2016-10-21 19:06:40'),
(56, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'La cuenta atrás', 'Los enemigos', 'https://www.youtube.com/watch?v=6s_O0MRT0zA', NULL, '2016-10-21 19:07:46', '2016-10-21 19:07:46'),
(57, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'Mas vale cholo', 'Molotov', 'https://www.youtube.com/watch?v=3NY5q7D1D0Y', 2016, '2016-10-21 19:08:54', '2016-10-21 19:08:54'),
(58, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'Joselito', 'Kiko Veneno', 'https://www.youtube.com/watch?v=LCNBo9Ck1Ss', 2016, '2016-10-21 19:10:35', '2016-10-21 19:10:35'),
(59, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'Dejaré mi tierra por ti', 'Nino Bravo', 'https://www.youtube.com/watch?v=zse7ihM76xg', 2016, '2016-10-21 19:12:00', '2016-10-21 19:12:00'),
(60, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'Testoy amando locamente', 'Las grecas', 'https://www.youtube.com/watch?v=U4il05Pt5Nk', 2016, '2016-10-21 19:15:35', '2016-10-21 19:15:35'),
(62, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'Me duele la cara de ser tan guapo', 'Los inhumanos', 'https://www.youtube.com/watch?v=aZVV6JhLKHc', NULL, '2016-10-21 19:17:50', '2016-10-21 19:18:11'),
(63, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'Por las noches', 'Los Ronaldos', 'https://www.youtube.com/watch?v=ZKJGW4l5osI', 2016, '2016-10-21 19:18:37', '2016-10-21 19:18:37'),
(64, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'Seguriño', 'Seguriño', 'https://www.youtube.com/watch?v=XDTil3ZkiSg', NULL, '2016-10-21 19:20:03', '2016-10-21 19:20:03'),
(65, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'La barbacoa, chiringuito. ....', 'Jordi Dann', 'https://www.youtube.com/watch?v=2muE6yXwN9g', NULL, '2016-10-21 19:22:31', '2016-10-21 19:22:31'),
(66, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'Cuéntame', 'Fórmula V', 'https://www.youtube.com/watch?v=MzxAfLZXWyY', NULL, '2016-10-21 19:22:56', '2016-10-24 15:43:00'),
(67, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'Quero xamon', 'Miguel Costas, Aerolineas', 'https://www.youtube.com/watch?v=6JGKK-dcENw', 2016, '2016-10-21 19:24:07', '2016-10-21 19:24:07'),
(68, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'Yo tomo', 'Bersuit Vergarabat', 'https://www.youtube.com/watch?v=-ap7MdG88nI', NULL, '2016-10-21 19:25:04', '2016-10-21 19:25:04'),
(69, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'La vida es un carnaval', 'Celia Cruz', 'https://www.youtube.com/watch?v=0nBFWzpWXuM', NULL, '2016-10-21 19:26:23', '2016-10-21 19:26:23'),
(70, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'A Forest', 'The Cure', 'https://www.youtube.com/watch?v=xik-y0xlpZ0', NULL, '2016-10-21 19:27:16', '2016-10-21 19:27:16'),
(71, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'La mandanga', 'El fari', 'https://www.youtube.com/watch?v=9l_SoSARk7A', 2017, '2016-10-21 19:30:00', '2017-10-26 16:46:23'),
(72, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'Move On Up', 'Curtis Mayfield', 'https://www.youtube.com/watch?v=6Z66wVo7uNw', NULL, '2016-10-21 19:30:30', '2016-10-21 19:30:30'),
(73, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'Boogie Wonderland', 'Earth Wind and Fire', 'https://www.youtube.com/watch?v=god7hAPv8f0', NULL, '2016-10-21 19:30:57', '2016-10-21 19:30:57'),
(74, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'Loosing My Religion', 'R.E.M.', 'https://www.youtube.com/watch?v=if-UzXIQ5vw', 2016, '2016-10-21 19:32:04', '2016-10-21 19:32:04'),
(75, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'El ritmo del garage', 'Loquillo', 'https://www.youtube.com/watch?v=qXE3J4xKYLg', NULL, '2016-10-21 19:34:05', '2016-10-21 19:34:05'),
(76, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'De agua', 'Zia', 'https://www.youtube.com/watch?v=3_gOc7KpYSI', 2016, '2016-10-21 19:35:26', '2016-10-21 19:35:26'),
(77, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'Brianstorm', 'Arctic Monkeys', 'https://www.youtube.com/watch?v=30w8DyEJ__0', NULL, '2016-10-21 19:37:03', '2016-10-21 19:37:03'),
(78, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'Cosmic girl', 'Jamiroquai ', 'https://www.youtube.com/watch?v=D-NvQ6VJYtE', 2016, '2016-10-21 19:38:17', '2016-10-21 19:38:17'),
(79, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'Quisiera ser', 'Dúo dinámico', 'https://www.youtube.com/watch?v=jkjtPG9w3lg', 2016, '2016-10-21 19:40:14', '2016-10-21 19:40:14'),
(80, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'Word up', 'Cameo', 'https://www.youtube.com/watch?v=MZjAantupsA', 2017, '2016-10-23 17:02:33', '2017-10-26 16:48:20'),
(81, 'adf9a1d3-0404-4e96-aabd-61fc06ea9476', 'Kiss with a fist', 'Florence and the Machine', 'https://www.youtube.com/watch?v=1SmxVCM39j4', 2016, '2016-10-24 17:24:48', '2016-10-24 17:24:48'),
(86, '078e48a4-5b2a-410f-8bef-33babe2378c3', 'Higher Ground', 'Stevie Wonder', 'https://www.youtube.com/watch?v=XV1DK9tSHio', 2017, '2017-10-19 11:03:41', '2017-10-26 14:52:35'),
(87, '078e48a4-5b2a-410f-8bef-33babe2378c3', 'Joker and the thief', 'Wolfmother', 'https://www.youtube.com/watch?v=8lkPfgzR6Hw', NULL, '2017-10-19 11:16:54', '2017-10-19 11:16:54'),
(88, '078e48a4-5b2a-410f-8bef-33babe2378c3', 'Somewhere', 'Soundgarden', 'https://youtu.be/E6Cnmbu0f8s', NULL, '2017-10-19 11:24:58', '2017-10-19 11:24:58'),
(89, '3c4c8b18-c991-43f6-a484-23be4b42565e', 'Show me how to live', 'Audioslave', 'https://youtu.be/97YMa9KCnrc', 2017, '2017-10-20 09:20:44', '2017-10-26 16:51:54'),
(90, '3c4c8b18-c991-43f6-a484-23be4b42565e', 'Show must go on', 'Queen', 'https://youtu.be/uKLMYZlbIb8', NULL, '2017-10-20 09:21:44', '2017-10-20 09:21:44'),
(91, '3c4c8b18-c991-43f6-a484-23be4b42565e', 'Desatame', 'Monica Naranjo', 'https://youtu.be/jpTOqGZCw10', 2017, '2017-10-20 09:25:22', '2017-10-26 14:56:58'),
(92, 'b2348b12-f6b7-4758-accd-fc0291e1cec8', 'May day ', 'Superskunk', 'https://youtu.be/A3_ftGabFHk', NULL, '2017-10-20 19:24:21', '2017-10-20 19:24:21'),
(93, 'b2348b12-f6b7-4758-accd-fc0291e1cec8', 'Soy un macarra', 'Ilegales ', 'https://youtu.be/1P5pq-k5rjw', 2017, '2017-10-20 19:25:41', '2017-10-26 15:06:54'),
(94, 'b2348b12-f6b7-4758-accd-fc0291e1cec8', 'Rulando ', 'O\'funkillo', 'https://youtu.be/MeehJ9M7DE0', NULL, '2017-10-20 19:32:11', '2017-10-20 19:32:11'),
(95, 'e1a1051a-24d8-4960-a9ee-84be21b6cfa2', 'Transmission', 'Joy division', 'https://youtu.be/6dBt3mJtgJc', NULL, '2017-10-20 23:28:39', '2017-10-20 23:47:41'),
(96, 'e1a1051a-24d8-4960-a9ee-84be21b6cfa2', 'Maria ', 'Blondie', 'https://youtu.be/abw49k3rIN0', 2017, '2017-10-20 23:29:08', '2017-10-26 16:50:13'),
(97, 'e1a1051a-24d8-4960-a9ee-84be21b6cfa2', 'L\'Amour Toujours', 'Gigi D\'Agostino', 'https://youtu.be/w15oWDh02K4', NULL, '2017-10-20 23:32:15', '2017-10-20 23:49:00'),
(98, '15685769-e355-421a-ab82-c19f0efcce87', 'Somebody told me ', 'The killers ', 'https://www.youtube.com/watch?v=Y5fBdpreJiU', 2017, '2017-10-22 13:56:03', '2017-10-31 21:11:32'),
(99, '6ae6a5b1-88b1-48fc-923d-f4dbe6b4c929', 'I Sat By the Ocean', 'Queens of the Stone Age', 'https://www.youtube.com/watch?v=Y1iMa7sB7CI', NULL, '2017-10-22 15:29:44', '2017-10-22 15:29:44'),
(100, '6ae6a5b1-88b1-48fc-923d-f4dbe6b4c929', 'Te quiero Igual', 'Andrés Calamaro', 'https://www.youtube.com/watch?v=lBPr7Uio1Tc', NULL, '2017-10-22 16:48:04', '2017-10-22 16:48:04'),
(102, '6ae6a5b1-88b1-48fc-923d-f4dbe6b4c929', 'Relojes en la Oscuridad', 'Nacha Pop', 'https://www.youtube.com/watch?v=WWfxpPWBuW0', NULL, '2017-10-22 17:50:22', '2017-10-22 17:50:22'),
(103, '18f7f0b3-e58c-49e2-ab95-fefe897de617', 'Valerie', 'Amy Winehouse', 'https://youtu.be/vfTHe64HQB0', 2017, '2017-10-22 19:08:44', '2017-10-26 16:54:26'),
(104, '18f7f0b3-e58c-49e2-ab95-fefe897de617', 'Back on the chain gang', 'The pretenders', 'https://youtu.be/ge6FM29YPlo', NULL, '2017-10-22 19:11:28', '2017-10-22 19:32:39'),
(105, '18f7f0b3-e58c-49e2-ab95-fefe897de617', 'Go your own way', 'Fleetwood Mac', 'https://youtu.be/X39RpK-qVPg', NULL, '2017-10-22 19:12:24', '2017-10-22 19:36:31'),
(106, '6192448a-c6cf-4ffa-92c7-d0b11b74f629', 'Try', 'Janis Joplin', 'https://m.youtube.com/watch?v=vU9Dsl89UGo', 2017, '2017-10-22 21:09:08', '2017-10-26 15:06:09'),
(107, '697fc2e6-dfcf-4c23-901d-ef12d760491b', 'Under cover of darkness', 'The Strokes', 'https://youtu.be/_l09H-3zzgA', NULL, '2017-10-22 21:59:01', '2017-10-26 14:49:24'),
(108, '6192448a-c6cf-4ffa-92c7-d0b11b74f629', 'Big in Japan', 'Guano Apes', 'https://m.youtube.com/watch?v=Q9jJufz9RNE', NULL, '2017-10-22 22:04:49', '2017-10-25 16:56:48'),
(109, '697fc2e6-dfcf-4c23-901d-ef12d760491b', 'Bulls on parade', 'Rage Against the Machine', 'https://youtu.be/3L4YrGaR8E4', NULL, '2017-10-22 22:10:11', '2017-10-22 22:10:11'),
(110, '6192448a-c6cf-4ffa-92c7-d0b11b74f629', 'Solo se vive una vez', 'Azucar Moreno', 'https://m.youtube.com/watch?v=LxrUkFQHYpg', 2017, '2017-10-22 22:23:17', '2017-10-30 17:02:49'),
(112, 'd882f52c-7e98-485c-b483-2a320a6e7981', 'Last Dance', 'Donna Summer', 'https://www.youtube.com/watch?v=1pE01_0cCus', NULL, '2017-10-22 22:46:52', '2017-10-26 16:47:30'),
(113, 'd882f52c-7e98-485c-b483-2a320a6e7981', 'Hoy no me puedo levantar', 'Mecano', 'https://www.youtube.com/watch?v=DtEZTXBLgng', 2017, '2017-10-22 22:51:35', '2017-10-26 14:57:50'),
(114, 'd882f52c-7e98-485c-b483-2a320a6e7981', 'Don´t bring me down', 'ELO - Electric Light Orquestra', 'https://www.youtube.com/watch?v=z9nkzaOPP6g', 2017, '2017-10-22 23:02:42', '2017-10-26 16:46:06'),
(115, '697fc2e6-dfcf-4c23-901d-ef12d760491b', 'La revolución sexual', 'La Casa Azul', 'https://youtu.be/juNxwa6H3lI', 2017, '2017-10-22 23:57:46', '2017-10-26 15:02:23'),
(116, '4303263a-0b6c-40ff-95f6-c3563de7a013', 'Paracaidas', 'Los Enemigos', 'https://www.youtube.com/watch?v=uPEcQah_xFE', NULL, '2017-10-23 06:46:29', '2017-10-23 06:46:29'),
(117, '4303263a-0b6c-40ff-95f6-c3563de7a013', 'El Salmon', 'Andrés Calamaro', 'https://www.youtube.com/watch?v=VdiQiNAiWcA', 2017, '2017-10-23 06:52:09', '2017-10-26 19:36:11'),
(118, '4303263a-0b6c-40ff-95f6-c3563de7a013', 'Yo que creo en el diablo.', 'Victor Coyote', 'https://www.youtube.com/watch?v=w9-F-L0TVWo', NULL, '2017-10-23 06:57:11', '2017-10-23 06:57:11'),
(119, '56588347-a0d7-4424-ad26-fc4fcba747eb', 'Lambada', 'Kaoma', 'https://youtu.be/iyLdoQGBchQ', 2017, '2017-10-23 11:20:20', '2017-10-26 15:04:01'),
(120, '56588347-a0d7-4424-ad26-fc4fcba747eb', 'Jackson', 'Nancy Sinatra & Lee Hazelwood', 'https://youtu.be/MB-GFmdg_eY', NULL, '2017-10-23 11:33:23', '2017-10-23 11:33:23'),
(121, '56588347-a0d7-4424-ad26-fc4fcba747eb', 'Papa loves Mambo', 'Perry Como', 'https://youtu.be/ujB-BZn3C4g', NULL, '2017-10-23 11:40:31', '2017-10-23 11:40:31'),
(122, 'e1fc5fc5-c42c-4bb5-97c0-d88a75e61f9a', 'Rossana', 'Toto', 'https://youtu.be/qmOLtTGvsbM', NULL, '2017-10-23 14:47:38', '2017-10-23 14:47:38'),
(124, 'e1fc5fc5-c42c-4bb5-97c0-d88a75e61f9a', 'Jefe de jefes', 'Los tigres del norte', 'https://www.youtube.com/watch?v=tKQwOuTiY-A', 2017, '2017-10-23 15:00:00', '2017-10-26 15:00:18'),
(125, 'e1fc5fc5-c42c-4bb5-97c0-d88a75e61f9a', 'Jesus Christ Pose', 'Soundgarden', 'https://youtu.be/14r7y6rM6zA', NULL, '2017-10-23 15:01:55', '2017-10-23 15:01:55'),
(126, 'efcf62f6-62e5-44ed-becd-af853db69a5f', 'fire ', 'jimmi hendrix', 'https://www.youtube.com/watch?v=6P37u4r4_1M', 2017, '2017-10-23 16:38:09', '2017-10-26 15:05:28'),
(127, 'efcf62f6-62e5-44ed-becd-af853db69a5f', 'Caught by the fuzz', 'Supergrass', 'https://www.youtube.com/watch?v=GNIMuvbiZcc', NULL, '2017-10-23 16:42:45', '2017-10-23 16:42:45'),
(128, 'efcf62f6-62e5-44ed-becd-af853db69a5f', 'Celebration', 'Kool and the Gang', 'https://www.youtube.com/watch?v=3GwjfUFyY6M', 2017, '2017-10-23 16:43:49', '2017-10-26 15:03:14'),
(129, '15685769-e355-421a-ab82-c19f0efcce87', 'Starlight ', 'Muse', 'https://youtu.be/Pgum6OT_VH8', NULL, '2017-10-23 17:07:09', '2017-10-23 17:08:57'),
(130, 'cfb9a2db-45ec-472f-a749-e8c6f48e266d', 'Pueblos del mundo', 'Siniestro total', '', NULL, '2017-10-23 17:46:37', '2017-10-23 17:54:15'),
(131, 'cfb9a2db-45ec-472f-a749-e8c6f48e266d', 'Like a rolling stone', 'Bob dylan', 'https://www.youtube.com/watch?v=dxLMr784l0Q', NULL, '2017-10-23 18:02:59', '2017-10-26 16:49:25'),
(132, '15685769-e355-421a-ab82-c19f0efcce87', 'What\'s up', '4 non blondes', 'https://www.youtube.com/watch?v=6NXnxTNIWkc', 2017, '2017-10-24 20:16:57', '2017-10-26 16:55:20'),
(133, 'f8265210-f8dc-4832-90fd-7625a1a76461', 'BuddY Holly', 'weezer', 'https://www.youtube.com/watch?v=kemivUKb4f4', 2017, '2017-10-25 00:46:27', '2017-10-26 14:48:59'),
(134, '03198792-5c0d-4473-b218-447a2ec7f1fd', ' Stockholm Syndrome', 'Muse', 'https://www.youtube.com/watch?v=e2ADMFaDdU4', 2017, '2017-10-25 12:37:47', '2017-10-30 16:45:10'),
(135, '03198792-5c0d-4473-b218-447a2ec7f1fd', '15 step', 'radiohead', 'https://www.youtube.com/watch?v=Qd4WI7gDb7U', NULL, '2017-10-25 12:47:03', '2017-10-25 12:47:03'),
(136, '03198792-5c0d-4473-b218-447a2ec7f1fd', 'My Own Summer', 'Deftones', 'https://www.youtube.com/watch?v=XOzs1FehYOA', NULL, '2017-10-25 20:21:24', '2017-10-25 20:21:24'),
(137, '3e3d7f52-db85-473d-9c83-3d0d8f7ef9f0', 'Don\'t Stop Believing', 'Journey', 'https://www.youtube.com/watch?v=KCy7lLQwToI', NULL, '2017-10-26 12:17:48', '2017-10-26 12:17:48'),
(138, '3e3d7f52-db85-473d-9c83-3d0d8f7ef9f0', 'American Idiot', 'Green Day', 'https://www.youtube.com/watch?v=Ee_uujKuJMI', 2017, '2017-10-26 12:18:59', '2017-10-26 15:07:47'),
(139, '3e3d7f52-db85-473d-9c83-3d0d8f7ef9f0', 'Aquí huele como que han fumao', 'Koma', 'https://www.youtube.com/watch?v=uVv6rzgH0ww', NULL, '2017-10-26 12:21:42', '2017-10-26 12:21:42'),
(140, '3e3d7f52-db85-473d-9c83-3d0d8f7ef9f0', 'We are the World', 'Michael Jackson', 'https://www.youtube.com/watch?v=M9BNoNFKCBI', 2017, '2017-10-26 14:28:36', '2017-10-26 14:57:15'),
(141, '3e3d7f52-db85-473d-9c83-3d0d8f7ef9f0', 'Come on', 'The Hives', 'https://www.youtube.com/watch?v=5SWdff6tgio', 2017, '2017-10-26 14:30:07', '2017-10-26 14:51:05'),
(142, '3e3d7f52-db85-473d-9c83-3d0d8f7ef9f0', 'Ellos dicen mierda', 'La polla records', 'https://www.youtube.com/watch?v=1Y_OZLcW2p8&t=1796s', 2017, '2017-10-26 14:33:47', '2017-10-26 15:01:26'),
(143, '3e3d7f52-db85-473d-9c83-3d0d8f7ef9f0', 'GhostBusters', 'GhostBusters', 'https://www.youtube.com/watch?v=Fe93CLbHjxQ', NULL, '2017-10-26 14:34:51', '2017-10-26 15:08:49'),
(144, '3e3d7f52-db85-473d-9c83-3d0d8f7ef9f0', 'Puto', 'Molotov', 'https://www.youtube.com/watch?v=CzEbm7yup7g', 2018, '2018-10-18 21:55:44', '2018-10-18 21:55:44'),
(145, '3e3d7f52-db85-473d-9c83-3d0d8f7ef9f0', 'Gotta go home', 'Boney M.', 'https://www.youtube.com/watch?v=5ww-pQynEDI', 2018, '2018-10-18 21:59:56', '2018-10-18 21:59:56'),
(147, '3e3d7f52-db85-473d-9c83-3d0d8f7ef9f0', 'Como una ola', 'Rocio Jurado', 'https://www.youtube.com/watch?v=nOb-DEZDY1Y', 2018, '2018-10-18 22:12:14', '2018-10-18 22:12:14'),
(148, '3e3d7f52-db85-473d-9c83-3d0d8f7ef9f0', 'Bailar pegados', 'Sergio Dalma', 'https://www.youtube.com/watch?v=zlmRGWNJE3c', 2018, '2018-10-18 22:15:03', '2018-10-18 22:15:03'),
(149, '3e3d7f52-db85-473d-9c83-3d0d8f7ef9f0', 'Un beso y una flor', 'Nino Bravo', 'https://www.youtube.com/watch?v=maEVfX9zRIE', 2018, '2018-10-18 22:15:52', '2018-10-18 22:15:52'),
(150, '3e3d7f52-db85-473d-9c83-3d0d8f7ef9f0', 'Waterloo', 'Abba', 'https://www.youtube.com/watch?v=Sj_9CiNkkn4', 2018, '2018-10-18 22:16:35', '2018-10-18 22:16:35'),
(151, '3e3d7f52-db85-473d-9c83-3d0d8f7ef9f0', 'Mi música es tu voz', 'Operación Truño', 'https://www.youtube.com/watch?v=80th_OV2I1A', 2018, '2018-10-18 22:17:28', '2018-10-18 22:17:28'),
(152, '3e3d7f52-db85-473d-9c83-3d0d8f7ef9f0', 'Footloose', 'Kenny Loggins', 'https://www.youtube.com/watch?v=wFWDGTVYqE8', 2018, '2018-10-18 22:19:05', '2018-10-18 22:19:05'),
(154, '3e3d7f52-db85-473d-9c83-3d0d8f7ef9f0', 'Jammin', 'Bob Marley', 'https://www.youtube.com/watch?v=oFRbZJXjWIA', 2018, '2018-10-18 22:20:59', '2018-10-24 20:16:18'),
(155, '3e3d7f52-db85-473d-9c83-3d0d8f7ef9f0', 'Carminha Vacaloura', 'Xabarí­n Club', 'https://www.youtube.com/watch?v=yh7S039PmWo', NULL, '2018-10-18 22:22:27', '2018-10-23 15:25:18'),
(156, '3e3d7f52-db85-473d-9c83-3d0d8f7ef9f0', 'Bienvenidos', 'Miguel Rí­os', 'https://www.youtube.com/watch?v=V61JeEpwYe0', 2018, '2018-10-18 22:25:13', '2018-10-24 20:44:12'),
(157, '3e3d7f52-db85-473d-9c83-3d0d8f7ef9f0', 'Baywatch', 'Baywatch Theme Song', 'https://www.youtube.com/watch?v=hNMNZZQJkP0', 2018, '2018-10-18 22:26:50', '2018-10-24 20:14:57'),
(158, '3e3d7f52-db85-473d-9c83-3d0d8f7ef9f0', 'Tick tick boom', 'The Hives', 'https://www.youtube.com/watch?v=1M02bAWDFkI&t=53s', 2018, '2018-10-18 22:27:38', '2018-10-18 22:27:38'),
(159, '3e3d7f52-db85-473d-9c83-3d0d8f7ef9f0', 'Tainted love', 'Soft Cell', 'https://www.youtube.com/watch?v=XZVpR3Pk-r8', 2018, '2018-10-18 22:28:18', '2018-10-18 22:28:18'),
(160, '3e3d7f52-db85-473d-9c83-3d0d8f7ef9f0', 'Acuarious', 'Rafael', 'https://www.youtube.com/watch?v=jpp0El-8G3I', 2018, '2018-10-18 22:30:18', '2018-10-18 22:31:27'),
(162, '3e3d7f52-db85-473d-9c83-3d0d8f7ef9f0', 'El campito', 'O funkillo', 'https://www.youtube.com/watch?v=kDNbNg8NTvg', 2018, '2018-10-18 22:33:51', '2018-10-24 20:51:08'),
(163, '3e3d7f52-db85-473d-9c83-3d0d8f7ef9f0', 'Bombtrack', 'Rage Against The Machine', 'https://www.youtube.com/watch?v=MUaL1FnotRQ', 2018, '2018-10-18 22:37:11', '2018-10-18 22:37:11'),
(164, '3e3d7f52-db85-473d-9c83-3d0d8f7ef9f0', 'Pretender', 'Foo Fighters', 'https://www.youtube.com/watch?v=SBjQ9tuuTJQ', 2018, '2018-10-18 22:38:31', '2018-10-18 22:38:31'),
(165, '3e3d7f52-db85-473d-9c83-3d0d8f7ef9f0', 'Paradise City', 'Guns n Roses', 'https://m.youtube.com/watch?v=T41cUjIcyug', 2018, '2018-10-18 22:39:15', '2018-10-23 16:08:41'),
(166, '3e3d7f52-db85-473d-9c83-3d0d8f7ef9f0', 'You Shook Me All Night Long', 'AC/DC', 'https://www.youtube.com/watch?v=Lo2qQmj0_h4', 2018, '2018-10-18 22:40:04', '2018-10-18 22:40:04'),
(167, '3e3d7f52-db85-473d-9c83-3d0d8f7ef9f0', 'Don diablo', 'Miguel Bose', 'https://www.youtube.com/watch?v=v3N444t7m0A', 2018, '2018-10-18 22:41:36', '2018-10-18 22:41:36'),
(168, '3e3d7f52-db85-473d-9c83-3d0d8f7ef9f0', 'You\'re the one that I want', 'Grease', 'https://www.youtube.com/watch?v=7oKPYe53h78', 2018, '2018-10-18 22:43:08', '2018-10-18 22:43:08'),
(170, '3e3d7f52-db85-473d-9c83-3d0d8f7ef9f0', 'Johnny techno ska', 'Paco Pil', 'https://www.youtube.com/watch?v=P6ShBEtZMjs', 2018, '2018-10-18 22:46:49', '2018-10-24 21:02:40'),
(171, '3e3d7f52-db85-473d-9c83-3d0d8f7ef9f0', 'Chup Chup', 'Australian Blonde', 'https://www.youtube.com/watch?v=TLaF7ErqArY', 2018, '2018-10-18 22:47:44', '2018-10-18 22:47:44'),
(172, '3e3d7f52-db85-473d-9c83-3d0d8f7ef9f0', 'Ellos las prefieren gordas', 'Orquesta Mondragon', 'https://www.youtube.com/watch?v=ZHB0A37Km34', 2018, '2018-10-18 22:49:16', '2018-10-18 22:49:16'),
(174, '3e3d7f52-db85-473d-9c83-3d0d8f7ef9f0', 'I need a hero', 'Bonnie Tyler', 'https://www.youtube.com/watch?v=OBwS66EBUcY', 2018, '2018-10-18 22:53:04', '2018-10-18 22:53:04'),
(175, '3e3d7f52-db85-473d-9c83-3d0d8f7ef9f0', 'Bohemian Rhapsody', 'Queen', 'https://www.youtube.com/watch?v=fJ9rUzIMcZQ', 2018, '2018-10-18 22:54:46', '2018-10-18 22:54:46'),
(176, '3e3d7f52-db85-473d-9c83-3d0d8f7ef9f0', 'Virtual insanity', 'Jamiroquai', 'https://www.youtube.com/watch?v=4JkIs37a2JE', 2018, '2018-10-18 22:56:04', '2018-10-18 22:56:04'),
(177, '3e3d7f52-db85-473d-9c83-3d0d8f7ef9f0', 'Wannabe', 'Spice Girls', 'https://www.youtube.com/watch?v=gJLIiF15wjQ', 2018, '2018-10-18 23:04:50', '2018-10-18 23:04:50'),
(179, '3e3d7f52-db85-473d-9c83-3d0d8f7ef9f0', 'A Cabritinha', 'Quim Barreiros', 'https://www.youtube.com/watch?v=wFpbhooe6Ns', 2018, '2018-10-18 23:12:46', '2018-10-18 23:12:46'),
(183, '30dbfe50-099a-46df-b8fb-caf4e6983c0d', 'Eres tú', 'Mocedades', 'https://www.youtube.com/watch?v=naAC37W42ro&feature=youtu.be', 2019, '2019-10-06 21:17:00', '2019-10-10 21:32:57'),
(184, '30dbfe50-099a-46df-b8fb-caf4e6983c0d', 'Ingrata', 'Café Tacuba', 'https://www.youtube.com/watch?v=kIr8hsVTCzg&feature=youtu.be', 2019, '2019-10-06 21:20:07', '2019-10-10 22:21:43'),
(185, '30dbfe50-099a-46df-b8fb-caf4e6983c0d', 'Un rayo de sol', 'Los diablos', 'https://www.youtube.com/watch?v=qc9EvsUDAcc', 2019, '2019-10-06 21:20:52', '2019-10-14 21:32:03'),
(186, '30dbfe50-099a-46df-b8fb-caf4e6983c0d', 'La puerta de Alcalá', 'Ana Belén y Victor Manuel', 'https://www.youtube.com/watch?v=5tqvQEFmFSE&feature=youtu.be', 2019, '2019-10-06 21:21:32', '2019-10-11 22:51:26'),
(187, '30dbfe50-099a-46df-b8fb-caf4e6983c0d', 'Soy una punk', 'Aerolineas Federales', 'https://www.youtube.com/watch?v=osNtIqQTPlE&feature=youtu.be', 2019, '2019-10-06 21:21:59', '2019-10-11 22:43:39'),
(188, '30dbfe50-099a-46df-b8fb-caf4e6983c0d', 'El Rey y yo', 'Los Ángeles Negros', 'https://www.youtube.com/watch?v=-drynqDSOOw', 2019, '2019-10-06 21:22:36', '2019-10-14 21:21:22'),
(189, '30dbfe50-099a-46df-b8fb-caf4e6983c0d', 'Obsesión', 'Aventura', 'https://www.youtube.com/watch?v=PfJM2YLtOXk&feature=youtu.be', 2019, '2019-10-06 21:23:23', '2019-10-11 22:57:37'),
(190, '30dbfe50-099a-46df-b8fb-caf4e6983c0d', 'Promesas que no valen nada', 'Los Piratas', 'https://www.youtube.com/watch?v=hptUE57xH74', 2019, '2019-10-06 21:23:48', '2019-10-14 21:36:39'),
(191, '30dbfe50-099a-46df-b8fb-caf4e6983c0d', 'Huesos', 'Los Burros', 'https://www.youtube.com/watch?v=OgRIWhRJrf4', 2019, '2019-10-06 21:24:11', '2019-10-14 21:24:05'),
(192, '30dbfe50-099a-46df-b8fb-caf4e6983c0d', 'Se me ha muerto el canario', 'No me pises que llevo chanclas', 'https://www.youtube.com/watch?v=8OVesjOfsME&feature=youtu.be', 2019, '2019-10-06 21:25:11', '2019-10-10 21:28:28'),
(193, '30dbfe50-099a-46df-b8fb-caf4e6983c0d', 'Bad Romance', 'Lady Gaga', 'https://www.youtube.com/watch?v=qrO4YZeyl0I', 2019, '2019-10-06 21:25:35', '2019-10-14 21:16:54'),
(194, '30dbfe50-099a-46df-b8fb-caf4e6983c0d', 'There is a light that never goes out', 'The Smiths', 'https://www.youtube.com/watch?v=siO6dkqidc4&feature=youtu.be', 2019, '2019-10-06 21:26:14', '2019-10-10 21:17:07'),
(195, '30dbfe50-099a-46df-b8fb-caf4e6983c0d', 'Starman', 'David Bowie', 'https://www.youtube.com/watch?v=tRcPA7Fzebw&feature=youtu.be', 2019, '2019-10-06 21:26:34', '2019-10-10 22:09:38'),
(196, '30dbfe50-099a-46df-b8fb-caf4e6983c0d', '99 red ballons', 'Nena', 'https://www.youtube.com/watch?v=Q86nf7mpOXk&feature=youtu.be', 2019, '2019-10-06 21:26:53', '2019-10-10 21:32:18'),
(197, '30dbfe50-099a-46df-b8fb-caf4e6983c0d', 'Go your Own Way', 'Fleetwood Mac', 'https://www.youtube.com/watch?v=6ul-cZyuYq4&feature=youtu.be', 2019, '2019-10-06 21:27:54', '2019-10-10 21:51:31'),
(198, '30dbfe50-099a-46df-b8fb-caf4e6983c0d', 'God Save the Queen ', 'Sex Pistols', 'https://www.youtube.com/watch?v=yqrAPOZxgzU&feature=youtu.be', 2019, '2019-10-06 21:28:29', '2019-10-10 21:21:08'),
(199, '30dbfe50-099a-46df-b8fb-caf4e6983c0d', 'Girls just wanna have fun', 'Cindy Lauper', 'https://www.youtube.com/watch?v=GCtmDqUnrTw&feature=youtu.be', 2019, '2019-10-06 21:29:22', '2019-10-09 23:17:51'),
(200, '30dbfe50-099a-46df-b8fb-caf4e6983c0d', 'Enjoy the silence', 'Depeche Mode', 'https://www.youtube.com/watch?v=aGSKrC7dGcY&feature=youtu.be', 2019, '2019-10-06 21:29:50', '2019-10-10 22:04:35'),
(201, '30dbfe50-099a-46df-b8fb-caf4e6983c0d', 'My Sweet Lord', 'G. Harrison', 'https://www.youtube.com/watch?v=8qJTJNfzvr8&feature=youtu.be', 2019, '2019-10-06 21:30:18', '2019-10-14 21:07:28'),
(202, '30dbfe50-099a-46df-b8fb-caf4e6983c0d', 'A Little Less Conversation', 'Elvis Presley', 'https://www.youtube.com/watch?v=WWVMXLSS1cA&feature=youtu.be', 2019, '2019-10-06 21:31:13', '2019-10-14 22:16:03'),
(203, '30dbfe50-099a-46df-b8fb-caf4e6983c0d', 'Macho Man', 'Village People', 'https://www.youtube.com/watch?v=AO43p2Wqc08&feature=youtu.be', 2019, '2019-10-06 21:32:23', '2019-10-14 22:16:26'),
(204, '30dbfe50-099a-46df-b8fb-caf4e6983c0d', 'High', 'Lighthouse Family', 'https://www.youtube.com/watch?v=taOL5HJdx1A', 2019, '2019-10-06 21:32:47', '2019-10-14 21:18:21'),
(205, '30dbfe50-099a-46df-b8fb-caf4e6983c0d', 'Strutter', 'Kiss', 'https://www.youtube.com/watch?v=IbiFkS4XwG8&feature=youtu.be', 2019, '2019-10-06 21:35:00', '2019-10-14 21:12:57'),
(206, '30dbfe50-099a-46df-b8fb-caf4e6983c0d', 'Little Green Bag', 'B.S.O Reservoir Dogs', 'https://www.youtube.com/watch?v=Y8yQuivSEio&feature=youtu.be', 2019, '2019-10-06 21:40:57', '2019-10-10 22:32:42'),
(207, '30dbfe50-099a-46df-b8fb-caf4e6983c0d', 'I want it that way', 'Backstreet Boys ', 'https://www.youtube.com/watch?v=4fndeDfaWCg&feature=youtu.be', 2019, '2019-10-06 21:41:38', '2019-10-10 22:29:11'),
(208, '30dbfe50-099a-46df-b8fb-caf4e6983c0d', 'Breaking the law', 'Judast Priest', 'https://www.youtube.com/watch?v=L397TWLwrUU', 2019, '2019-10-06 21:42:32', '2019-10-14 19:57:05'),
(209, '30dbfe50-099a-46df-b8fb-caf4e6983c0d', 'Time of my life', 'B.S.O Dirty Dancing', 'https://www.youtube.com/watch?v=eMoMBFhMdDw&feature=youtu.be', 2019, '2019-10-06 21:43:40', '2019-10-10 22:35:46'),
(210, '30dbfe50-099a-46df-b8fb-caf4e6983c0d', 'Don´t go breaking my heart', 'Elton John', 'https://www.youtube.com/watch?v=1XcWRl8l9vE&feature=youtu.be', 2019, '2019-10-06 21:44:33', '2019-10-10 21:58:16'),
(211, '30dbfe50-099a-46df-b8fb-caf4e6983c0d', 'Heart', 'Barracuda', 'https://www.youtube.com/watch?v=VdOkQ6THDVw&feature=youtu.be', 2019, '2019-10-06 21:44:57', '2019-10-10 22:26:24'),
(212, '30dbfe50-099a-46df-b8fb-caf4e6983c0d', 'Man in a box ', 'Alice in Chains', 'https://www.youtube.com/watch?v=46zC6iR2wsg&feature=youtu.be', 2019, '2019-10-06 21:46:28', '2019-10-11 23:00:02'),
(213, '30dbfe50-099a-46df-b8fb-caf4e6983c0d', 'Shake it off', 'Taylor Swift', 'https://www.youtube.com/watch?v=nfWlot6h_JM&feature=youtu.be', 2019, '2019-10-06 21:47:05', '2019-10-10 21:20:10'),
(214, '30dbfe50-099a-46df-b8fb-caf4e6983c0d', 'Last nite', 'The Strokes', 'https://www.youtube.com/watch?v=TOypSnKFHrE&feature=youtu.be', 2019, '2019-10-06 21:47:42', '2019-10-10 21:13:52'),
(215, '30dbfe50-099a-46df-b8fb-caf4e6983c0d', 'Faith', 'George Michael', 'https://www.youtube.com/watch?v=6Cs3Pvmmv0E&feature=youtu.be', 2019, '2019-10-06 21:48:13', '2019-10-14 19:56:09'),
(216, '30dbfe50-099a-46df-b8fb-caf4e6983c0d', 'Age Of Pamparius', 'Turbonegro', 'https://www.youtube.com/watch?v=YtCvz6-7D7g&feature=youtu.be', 2019, '2019-10-06 21:49:39', '2019-10-10 21:09:03'),
(217, '30dbfe50-099a-46df-b8fb-caf4e6983c0d', 'Tanto la quería', 'Andy y Lucas', 'https://www.youtube.com/watch?v=Zx1XAZ5z8GI&feature=youtu.be', 2019, '2019-10-06 21:51:41', '2019-10-11 22:54:31'),
(218, '30dbfe50-099a-46df-b8fb-caf4e6983c0d', 'Runaways', 'Cherry Bomb', 'https://www.youtube.com/watch?v=WNxKpYOOYvM&feature=youtu.be', 2019, '2019-10-06 21:54:52', '2019-10-10 22:16:23'),
(219, '30dbfe50-099a-46df-b8fb-caf4e6983c0d', 'Amores de barra', 'Ella baila sola', 'https://www.youtube.com/watch?v=xmgiCLdD4EQ&feature=youtu.be', 2019, '2019-10-06 21:55:41', '2019-10-10 22:01:34'),
(220, '30dbfe50-099a-46df-b8fb-caf4e6983c0d', 'El imperio contraataca', 'Los Nikis', 'https://www.youtube.com/watch?v=rS-_EZgRb0w', 2019, '2019-10-06 21:56:35', '2019-10-14 21:35:04'),
(221, '30dbfe50-099a-46df-b8fb-caf4e6983c0d', 'Desesperada', 'Marta Sánchez', 'https://www.youtube.com/watch?v=d3fXmjU3DSY&feature=youtu.be', 2019, '2019-10-06 21:57:22', '2019-10-10 21:37:00'),
(223, '30dbfe50-099a-46df-b8fb-caf4e6983c0d', 'Voy a mil', 'Olé Olé', 'https://www.youtube.com/watch?v=43vT1uOMy3M', 2019, '2019-10-06 21:58:04', '2019-10-14 21:41:45'),
(224, '30dbfe50-099a-46df-b8fb-caf4e6983c0d', 'Baila Casanova', 'Paulina Rubio', 'https://www.youtube.com/watch?v=ABq9Kp5dia4&feature=youtu.be', 2019, '2019-10-06 21:59:02', '2019-10-10 21:23:03'),
(225, '30dbfe50-099a-46df-b8fb-caf4e6983c0d', 'Beso en la boca es cosa del pasado', 'Axe Bahía', 'https://www.youtube.com/watch?v=HL_PQwMw6a0&feature=youtu.be', 2019, '2019-10-06 22:00:06', '2019-10-11 22:39:25'),
(226, '30dbfe50-099a-46df-b8fb-caf4e6983c0d', 'Embrujada', 'Tino Casal', 'https://www.youtube.com/watch?v=kNd-kSzYhtQ&feature=youtu.be', 2019, '2019-10-06 22:01:09', '2019-10-11 22:44:28'),
(227, '30dbfe50-099a-46df-b8fb-caf4e6983c0d', 'Gimme', 'Abba', 'https://www.youtube.com/watch?v=XEjLoHdbVeE&feature=youtu.be', 2019, '2019-10-06 22:02:03', '2019-10-10 21:42:35'),
(228, '30dbfe50-099a-46df-b8fb-caf4e6983c0d', 'Kick Out The Jams', 'MCS', 'https://www.youtube.com/watch?v=yvJGQ_piwI0&feature=youtu.be', 2019, '2019-10-06 22:02:55', '2019-10-10 21:35:30'),
(229, '30dbfe50-099a-46df-b8fb-caf4e6983c0d', 'hit the road Jack', 'Ray Charles', 'https://www.youtube.com/watch?v=Q8Tiz6INF7I', 2019, '2019-10-14 21:49:54', '2019-10-14 21:49:54'),
(230, '30dbfe50-099a-46df-b8fb-caf4e6983c0d', 'Time to pretend', 'MGMT', 'https://www.youtube.com/watch?v=PQ5D4pT4boQ', 2019, '2019-10-14 21:52:02', '2019-10-14 21:53:42');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` char(36) NOT NULL,
  `id_old` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `first_surname` varchar(50) DEFAULT NULL,
  `second_surname` varchar(50) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `token_expires` datetime DEFAULT NULL,
  `api_token` varchar(255) DEFAULT NULL,
  `activation_date` datetime DEFAULT NULL,
  `secret` varchar(32) DEFAULT NULL,
  `secret_verified` tinyint(1) DEFAULT NULL,
  `tos_date` datetime DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `is_superuser` tinyint(1) NOT NULL DEFAULT '0',
  `role` varchar(255) DEFAULT 'user',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `additional_data` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `id_old`, `username`, `email`, `password`, `name`, `first_surname`, `second_surname`, `phone`, `token`, `token_expires`, `api_token`, `activation_date`, `secret`, `secret_verified`, `tos_date`, `active`, `is_superuser`, `role`, `created`, `modified`, `additional_data`) VALUES
('03198792-5c0d-4473-b218-447a2ec7f1fd', 46, 'reynier', 'reynieral@gmail.com', '$2y$10$0KJvMBXwaHA8fROFRNTQfe5iyPM6lYattbZ5afjC2xfYQMA8IMgWC', 'Reynier', '', '', '622404130', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2016-10-24 18:06:19', '2019-10-27 13:58:21', NULL),
('05388740-d3f7-4308-9d9d-16039387cab7', 66, 'Javi Millo', 'javiermontenegroguillan@gmail.com', '$2y$10$OFsGkCGn7O3sE77TNejzuuW3PqfCUgtpdBP2Nq9lne7u0Ndwhwa9i', 'Javi Millo', '', '', '620233326', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2019-10-15 12:42:21', '2019-10-19 10:53:28', NULL),
('078e48a4-5b2a-410f-8bef-33babe2378c3', 31, 'tono', 'tono23_9_88@hotmail.com', '$2y$10$oMLYnUN7l3cWCY86ywqhdOqe.jkyyhAjgaAVRstUSk/tV1N2I5L8O', 'tonho', '', '', '665914619', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'user', '2016-10-21 17:21:48', '2019-10-19 07:43:36', NULL),
('0bf111e7-6ff1-4261-96e7-3f33cfa8d4aa', 16, 'diego', 'diegogandara92@gmail.com', '$2a$10$E0SFVjXE17eoYLD6OhP9XeO/8PNR/QjFXruKOa.OWvkrGa0Q6XOCe', 'Diego', NULL, NULL, '639710785', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2016-10-21 17:14:06', '2017-10-28 14:12:50', NULL),
('1310f7d5-0bf8-4406-b57b-12a588cf0e18', 17, 'babi', 'yahvericon@hotmail.com', '$2y$10$KEx7awllPngA9hxM8E.UnuU4GSDkCfoxXcIx6kst15eZJDQw39f1u', 'babi', '', '', '661214889', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2016-10-21 17:15:10', '2019-10-20 17:03:45', NULL),
('15685769-e355-421a-ab82-c19f0efcce87', 44, 'martin', 'homeclaro@hotmail.es', '$2y$10$Xp7u6wZ2nO01vEicfIXIwOphBy04r.DqYwRwZEeXMzbcqofoJ4IAe', 'martin', '', '', '630970430', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2016-10-23 13:11:10', '2019-10-23 21:03:30', NULL),
('159775bb-fd04-4560-b12d-ded09b36ebd8', 53, 'manubaixo', 'manubaixo@gmail.com', '$2a$10$UZQ4EO/z79jrPAjcacZb.OrSdAgegXo7gY64pxUQx91LTpr3.O0E.', 'Manu', NULL, NULL, '626686500', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2018-10-10 13:29:19', '2018-10-22 20:20:43', NULL),
('18f7f0b3-e58c-49e2-ab95-fefe897de617', 32, 'monica', 'trasnoseica@gmail.com', '$2y$10$fCqD4Wwp5ocqrWLAVKfOf.kTzEep3GRdu1icvq0PeD6qN6OGnNLNC', 'monica', '', '', '999468606', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2016-10-21 17:22:03', '2019-10-27 21:09:56', NULL),
('1d7ab30b-16ec-470a-aead-7f51346dcebb', 0, 'Adri', 'adrianrios.rr@gmail.com', '$2y$10$cTrDrRdgGoUFYEiO4UvguOid777yukAmvaX5kLUE7skBPBUnP6NPG', 'Adri', 'Ríos', '', '647190021', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2019-10-23 13:24:19', '2019-10-28 20:26:07', NULL),
('1e93668c-8b5d-4988-a3d1-bffcd8a7417e', 62, 'lore', 'lorelaiali@hotmail.com', '$2y$10$f4hnA/GQ174QHKmLkFbbvuGGIVAA.JV79ySN7.hqZC6764M1GcUBe', 'lore', '', '', '666666666', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2018-10-24 20:29:44', '2019-10-28 15:55:34', NULL),
('30dbfe50-099a-46df-b8fb-caf4e6983c0d', 64, 'emma', 'holaemmaovin@hotmail.com', '$2y$10$It.QTz1E2GDJe3Z5QPNv2OJRzZpVtDH7ZIGYZR01usqerQ7I3EUC6', 'emma', '', '', '676116654', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'user', '2019-10-06 19:43:38', '2019-10-27 11:53:17', NULL),
('33d81274-7b8a-48ad-82cf-3087de06dac0', 28, 'fran', 'elchatarreroelectronico@gmail.com', '$2y$10$P35zVZYqRlBmBYZREa285OaUqjlu1TbZDepKcx6dPtloZSCtf6tLm', 'Fran', '', '', '616768298', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2016-10-21 17:19:50', '2019-10-19 11:55:52', NULL),
('3c4c8b18-c991-43f6-a484-23be4b42565e', 35, 'willy', 'williamboubeta@gmail.com', '$2y$10$XzF5fFih/FQtHfmLWQmLxO0fbvQ.LsdHJ164VffszFtuymSsBdouu', 'Willy', '', '', '683127391', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2016-10-21 17:24:49', '2019-10-25 12:02:48', NULL),
('3e3d7f52-db85-473d-9c83-3d0d8f7ef9f0', 1, 'max', 'maxgomezparada@gmail.com', '$2y$10$BRJqdN/J2uz27QnrRdTRdOgWzkyYelV9e/w2FhseA9aLvy4Q/AcvS', 'Max', '', '', '650123804', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'user', '2016-10-19 00:45:55', '2019-10-19 15:04:12', NULL),
('4303263a-0b6c-40ff-95f6-c3563de7a013', 49, 'jero', 'jerop152@gmail.com', '$2y$10$b7HhS.zviEkEhDgGgdGdXexrAjfj2kCMTw.CKpujdr.IZNcZpt.Tu', 'jero', '', '', '666468602', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2017-10-21 09:51:34', '2019-10-19 09:25:18', NULL),
('4de03e76-2628-4061-889e-19911f7cc01e', 54, 'santi', 'santiago.paredes.cruz@gmail.com', '$2a$10$8ASRqO1sH16Mwok12rmV.uYI7jThtbCvNnQJ.gzeWZZS17Gbzubsm', 'Santi Traficanti', NULL, NULL, '608265384', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2018-10-10 13:42:55', '2018-10-11 13:53:26', NULL),
('53f2bfa4-e4aa-429d-b177-2dea148fdeb3', 0, 'chema', 'chemavafer@gmail.com', '$2y$10$8Tl1tC35oxZv1UIxyLxoMu3Aq72Bet5k76XHjcEp.NugT6VvqX6Z6', '', '', '', '625380512', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2019-10-25 07:48:08', '2019-10-25 19:58:51', NULL),
('56588347-a0d7-4424-ad26-fc4fcba747eb', 39, 'iria', 'iriapisa@gmail.com', '$2y$10$/TlV8/VQUI0/GzFQqPVfM.XLxBSvrBjnd3lIju4haJOD0q6.llud2', 'iria', '', '', '633340988', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2016-10-21 17:26:58', '2019-10-28 08:43:31', NULL),
('56c7c858-df60-49a9-9bd9-f5040a1f952a', 52, 'cebrian', '', '$2a$10$xtwG6kMfD8HncRaG6LyjmugkS0PkDsi93S6fVZGH4amh3ll0FzGMK', 'cebrian', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2018-10-10 13:28:52', '2018-10-10 13:28:52', NULL),
('58f41b46-8897-4ac0-8e33-a4c79172c9fb', 59, 'loli', '', '$2a$10$BUMYZvVPli6g6ztpJ31Ci.1TwsdwywriVKIuCRcsieYjr2dqW5s7G', 'loli', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2018-10-20 15:35:52', '2018-10-20 15:35:52', NULL),
('594260bf-cd34-4c8a-b9fd-58b29bcb2842', 34, 'dani', 'danicomes@hotmail.com', '$2y$10$qG4Y1SxfRwqpxa/J32jyie39pPO5Bh7FP3WxDdEhJZOy4gH6Z2Pe2', 'dani', '', '', '679947094', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2016-10-21 17:23:50', '2019-10-19 12:50:57', NULL),
('5a3d04ca-acf8-4110-95e8-6f8f79232060', 51, 'gabi', 'galigab@hotmail.es', '$2y$10$ghiaon7e9EV5Kl6jQ101geSjiMmCpwlbzgnkcRhO2qM3s..F6zGvG', 'gabi', '', '', '660062568', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2017-10-28 13:11:55', '2019-10-27 20:57:29', NULL),
('5c16538a-0d0a-4183-b2ea-a4cb2cfcfe2a', 48, 'sara', 'sara.choren@gmail.com', '$2a$10$aWccLDZLw3pRBR08sHd/7utrXOtiKysffi.995Cc7S3ElhpCApUCe', 'Sara', NULL, NULL, '649024290', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2016-11-02 18:55:13', '2016-11-02 23:46:27', NULL),
('5c2f81fd-780f-4fd0-99af-23a813ea79b3', 21, 'herbello', 'alexherbello@hotmail.com', '$2y$10$t/XTS7WATr95/ckc0BPy0O4uOz0yImW/rcwbPePK6yLw7vmSgw4Xy', 'Álex ', 'Herbello', '', '616505797', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2016-10-21 17:16:24', '2019-10-27 14:31:11', NULL),
('5cbbd8ab-32a8-41b4-8633-b2302a73f60d', 63, 'baalhug', 'baalhug@gmail.com', '$2y$10$ElmulRocBqjAg7.PcQUR/OcMDGmuCZq0iGNSQbdsz7moMPDv9hDfC', 'Fran', 'Gil', 'González', '625209027', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2018-10-24 21:41:05', '2019-10-19 15:16:54', NULL),
('6192448a-c6cf-4ffa-92c7-d0b11b74f629', 15, 'rebe', 'larshem1@gmail.com', '$2y$10$R/Rq1z4PoJbbk5wdmkGkNOd9wHnaolqYYamF/mDhbfIA2Zge9G6FW', 'Rebe', '', '', '655027220', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2016-10-21 12:19:25', '2019-10-22 13:18:22', NULL),
('63d24792-9c5a-45e7-af41-19184caa3fff', 26, 'marques', 'markes.10@hotmail.com', '$2y$10$FN9vo2V2gB8f9IaoNN0.O.NvdOix2r7xt7XPq6MY6pUgHvdDVzQw2', 'Marqués ', '', '', '661240455', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2016-10-21 17:18:43', '2019-10-19 12:33:10', NULL),
('682cf312-9a70-4b40-a7e3-6dd6d4afb25e', 65, 'adriana', 'pemadri@gmail.com', '$2y$10$VNgxaRW2DVHrHTwBo2vk9exPjM2xwR.zJtSlLkOQR2A6/Msq77OMG', 'Adriana', '', '', '663449228', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2019-10-15 12:26:32', '2019-10-20 11:00:21', NULL),
('697fc2e6-dfcf-4c23-901d-ef12d760491b', 33, 'puro', 'javiseijas81@gmail.com', '$2y$10$b4rCxK2qyNpdBRrGQPSor.aNFM2yK/RY.TOWlwiZyWucBK9qfYhOe', 'puro', '', '', '630033200', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'user', '2016-10-21 17:23:21', '2019-10-19 07:38:42', NULL),
('6ae6a5b1-88b1-48fc-923d-f4dbe6b4c929', 27, 'denis', 'sometimesoccurs@hotmail.com', '$2y$10$2iJLAt0nVNHhr3YzdeMqCODsx3.vpXCZ118ZPIN0QHztbHE.dpHqq', 'denis', '', '', '661361258', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2016-10-21 17:19:26', '2019-10-19 09:04:06', NULL),
('707a332a-7931-4527-9a1f-cfc3f114e1d7', 37, 'maria', 'mariafaltri@gmail.com', '$2y$10$WHLtHLRBp5vPmoTcBQDROermCBhxhPmeRFSbzPvStrZ9kjuNf1NUO', 'maria', '', '', '658633143', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2016-10-21 17:25:28', '2019-10-28 20:47:13', NULL),
('7858508c-b4fe-4004-982e-0e88ea4ad4a2', 19, 'carlos', 'carlos-bc@hotmail.es', '$2y$10$bgRsLCLcUhIziApdADZu4O9UqSK7xPCUf46FFWn8hF0z56jVY4hlO', 'carlos', '', '', '678222130', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2016-10-21 17:15:48', '2019-10-28 16:13:16', NULL),
('8c052cad-30aa-4049-91d0-5b2429d7c581', 25, 'jorge', 'macjuncal@gmail.com', '$2y$10$x/bB9HlYSLZ3CyNAilunVeayomvNi9NLy4Jz6c2yYHF8YkjOTX1ey', 'jorge', '', '', '609431838', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2016-10-21 17:18:15', '2019-10-27 17:17:50', NULL),
('9030222b-8d63-414e-ba94-ca7efd14ac3e', 20, 'oscar', 'chokitosdelaria@hotmail.com', '$2a$10$C0e7556v3Mne1XJ77W3yFOp56U9ai7PSBN5i7EjDLUXqlyTN0/kHW', 'oscar', NULL, NULL, '607988728', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2016-10-21 17:16:09', '2018-10-11 17:47:54', NULL),
('9610c544-4b60-4f77-8100-bfb1b2b77018', 42, 'xan', 'xancampos@gmail.com', '$2a$10$4H4WYeBVD/.vgqHkph0WGOdiMgK.XtRZHnqaHzDbDePIx3GXd19HO', 'xan', NULL, NULL, '620178961', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2016-10-21 17:28:19', '2016-10-25 12:56:58', NULL),
('9a0ae66b-7f0a-4644-ab3b-bbc338387185', 61, 'gdjazz', '', '$2a$10$4HPAMITsEw163WEpKTc9z.maHavlWyE1U0qKuBYZ3P6of1mMEn8ay', 'gdjazz', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2018-10-23 23:10:54', '2018-10-23 23:10:54', NULL),
('adf9a1d3-0404-4e96-aabd-61fc06ea9476', 0, 'superadmin', 'admin@admin.com', '$2y$10$BRJqdN/J2uz27QnrRdTRdOgWzkyYelV9e/w2FhseA9aLvy4Q/AcvS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'user', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
('afa7577f-59bb-4df0-b750-fd9dea6ed46a', 50, 'saul', 'saulchapela@gmail.com', '$2a$10$mba2VgVsEdeBWNDe1SiRReGNi20Ey1MCQl2uIvK/RF2tRFHtRoMBu', 'Saúl', NULL, NULL, '617113541', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2017-10-27 14:19:02', '2017-11-01 11:47:29', NULL),
('b2348b12-f6b7-4758-accd-fc0291e1cec8', 36, 'chame', 'chame644@gmail.com', '$2y$10$M1JRM952bz0e1a8HbvOHd.iSLH0T3OrIWCMEJddCeyot49jj/OfB6', 'chame', '', '', '647641571', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2016-10-21 17:25:09', '2019-10-19 07:35:32', NULL),
('b2efde4c-e55e-446d-9146-8cfbaeca72c3', 58, 'aaron', 'aaron.seijas@gmail.com', '$2y$10$I3vzHH9PCYT5uDxNwehly.Qax8okNYnFS5CYy8RPYhl8klI7/DkT6', 'aaron', 'Primo', 'Derribera', '659697379', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2018-10-19 12:28:20', '2019-10-19 19:36:33', NULL),
('b57e7c2f-9f6f-49cc-a99d-fad0da0de1c0', 57, 'paul', '', '$2a$10$JpLAtVc9T5iA9ssIXAXmBuRJqWLDmrnUvqYWmI2x4vFO2y/lm5EKi', 'paul', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2018-10-18 23:16:28', '2018-10-18 23:16:28', NULL),
('ce797503-e948-4bac-b53f-198fca6fbbd4', 55, 'iago', 'iagopousaben@gmail.com', '$2a$10$tVaKJRG30CVMzQ7F7dG4veDq/ZL/jm66aWoIqqNXOXLNlsVDToYoS', 'iago', NULL, NULL, '645468190', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2018-10-18 23:14:50', '2018-10-28 18:17:13', NULL),
('cfb9a2db-45ec-472f-a749-e8c6f48e266d', 40, 'jose', 'xo-shu@hotmail.com', '$2y$10$kqPN4beyisGlRK2551VgTu68kMmT6gYlcVqXG1qWiiQbLQiIH0Kfa', 'jose', '', '', '627352451', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2016-10-21 17:27:13', '2019-10-27 21:50:27', NULL),
('d12c031e-3628-4585-9c11-0557f086b835', 24, 'juanma', 'juanmaphantomons@gmail.com', '$2y$10$Eor62EdJmq3k6KLFIUA6vuMPJFhXJo7M76o6oPE/LYqDLgrxp.SP.', 'juanma', '', '', '690286254', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2016-10-21 17:17:48', '2019-10-28 08:40:48', NULL),
('d2b4a380-de23-4823-b6b3-7117eb950b56', 56, 'victor', 'victormfv.phc@gmail.com', '$2a$10$2BjrRrAZ./5TR.lt.mgjY.oyNms/Npnk1Wa30PPXgbmb9nEBuErtq', 'victor', NULL, NULL, '653681183', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2018-10-18 23:15:49', '2018-10-27 11:36:56', NULL),
('d882f52c-7e98-485c-b483-2a320a6e7981', 30, 'noemi', 'noemfv@yahoo.es', '$2y$10$eiUg7ZT6.6JjZV.vxF/txOto3gLJfA2OkpCBx9WCrRJn1B7U4D0dq', 'noemi', '', '', '699212878', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2016-10-21 17:21:24', '2019-10-27 21:24:03', NULL),
('e1a1051a-24d8-4960-a9ee-84be21b6cfa2', 38, 'berto', 'meddlebraxas@gmail.com', '$2y$10$2vCW7QXZ71U6ks95pex7F.Qj/PMmvA2Hsm/S2qCXHDZDOUtY545nC', 'berto', '', '', '659720456', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2016-10-21 17:25:53', '2019-10-20 18:11:30', NULL),
('e1fc5fc5-c42c-4bb5-97c0-d88a75e61f9a', 23, 'nacho', 'rockswell77@gmail.com', '$2y$10$U5Dv33wqYAMXzaGCqdy3YuPR0gdplQfkVyCrefpkyXjCU6Jnr723W', 'nacho', '', '', '693739193', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2016-10-21 17:17:23', '2019-10-24 22:18:23', NULL),
('eed947e5-8779-4e98-9c0a-b283465963d1', 0, 'ruben', '', '$2y$10$ahG.vwuLOXRrU/hwMmDPyOjxOQdsLnkRyQI9N7BodoKiwH5MYNjam', 'Rubén', 'Fernández', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2019-10-25 10:36:02', '2019-10-27 21:44:32', NULL),
('efcf62f6-62e5-44ed-becd-af853db69a5f', 29, 'juansy', 'juansy1978@hotmail.com', '$2y$10$qhgRpirgWITis/wb9q/sNOy3iUo108ZlUgg0t3vrKTyztjpecltqa', 'juansy', '', '', '678993333', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2016-10-21 17:21:10', '2019-10-27 21:23:36', NULL),
('f26b98db-4590-4335-a3ad-e2340b8b2de3', 18, 'kurt', 'oneno.ruben@hotmail.com', '$2y$10$lTxG8zqhrJZY50/8ggzxF.BNJzp/7LoPDe9x4/vEbJ5MvVsAVGja6', 'kurtis', '', '', '669799519', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'user', '2016-10-21 17:15:30', '2019-10-19 08:01:37', NULL),
('f5616091-2207-4e7f-baab-1ffabff8301f', 60, 'elena', '', '$2a$10$XMTHdfK5VAK3WBAel730wuhpI3q5gA8pT6.PrtGgyVhXpB6GEbjdC', 'elena', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2018-10-21 01:28:14', '2018-10-21 01:28:14', NULL),
('f8265210-f8dc-4832-90fd-7625a1a76461', 4, 'cara', 'oscarrivas_@hotmail.com', '$2y$10$NMoOhXfeAOf3oCO.xQ6Tvu85vhnFvLyJ5A4Cyp4UB9DYjJoFNnoNO', 'Cara', '', '', '674026204', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'user', '2016-10-19 02:41:22', '2019-10-20 16:56:22', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users_old`
--

CREATE TABLE `users_old` (
  `id` char(36) NOT NULL,
  `id_old` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `first_surname` varchar(50) DEFAULT NULL,
  `second_surname` varchar(50) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `token_expires` datetime DEFAULT NULL,
  `api_token` varchar(255) DEFAULT NULL,
  `activation_date` datetime DEFAULT NULL,
  `secret` varchar(32) DEFAULT NULL,
  `secret_verified` tinyint(1) DEFAULT NULL,
  `tos_date` datetime DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `is_superuser` tinyint(1) NOT NULL DEFAULT '0',
  `role` varchar(255) DEFAULT 'user',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `additional_data` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `users_old`
--

INSERT INTO `users_old` (`id`, `id_old`, `username`, `email`, `password`, `name`, `first_surname`, `second_surname`, `phone`, `token`, `token_expires`, `api_token`, `activation_date`, `secret`, `secret_verified`, `tos_date`, `active`, `is_superuser`, `role`, `created`, `modified`, `additional_data`) VALUES
('03198792-5c0d-4473-b218-447a2ec7f1fd', 46, 'reynier', 'reynieral@gmail.com', '$2y$10$fTKHpSjinuUYdKEeu3z/XOBGYbM9kku9A6OoyvTvkhV6bDr9H.GFq', 'Reynier', NULL, NULL, '622404130', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2016-10-24 18:06:19', '2018-10-29 09:32:40', NULL),
('05388740-d3f7-4308-9d9d-16039387cab7', 66, 'Javi Millo', 'javiermontenegroguillan@gmail.com', '$2y$10$.TVDWRy7wfW7tbL.elcqcufDqgU5lSVJok6wLjmbOpEgDLilQpFIi', 'Javi Millo', NULL, NULL, '620233326', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2019-10-15 12:42:21', '2019-10-15 16:49:58', NULL),
('078e48a4-5b2a-410f-8bef-33babe2378c3', 31, 'tono', 'tono23_9_88@hotmail.com', '$2y$10$MYaqBEfGXjX8jDK5WObBC.bWyvF0CSvw20X.v2cLXC/jERxd7zMuS', 'tonho', NULL, NULL, '665914619', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2016-10-21 17:21:48', '2018-10-17 10:54:11', NULL),
('0bf111e7-6ff1-4261-96e7-3f33cfa8d4aa', 16, 'diego', 'diegogandara92@gmail.com', '$2y$10$FEdRq6uTKurJFNsTW3GsKOWtKgUmhm/BZBVszLaGfzhYgyyk8Y3l.', 'Diego', NULL, NULL, '639710785', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2016-10-21 17:14:06', '2017-10-28 14:12:50', NULL),
('1310f7d5-0bf8-4406-b57b-12a588cf0e18', 17, 'babi', 'yahvericon@hotmail.com', '$2y$10$bkWI4Ox3xUlCN.x7YPxJm.NkxMJ0OEjm1Jr3YOpDY7r42UNmOYjw6', 'babi', NULL, NULL, '661214889', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2016-10-21 17:15:10', '2019-10-15 21:58:20', NULL),
('15685769-e355-421a-ab82-c19f0efcce87', 44, 'martin', 'homeclaro@hotmail.es', '$2y$10$pq2DIgPuR8Gx.Lykn9kzc.qCFWrPiovY2K1dsEascSQPYHtglJBVm', 'martin', NULL, NULL, '630970430', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2016-10-23 13:11:10', '2018-10-12 09:11:06', NULL),
('159775bb-fd04-4560-b12d-ded09b36ebd8', 53, 'manubaixo', 'manubaixo@gmail.com', '$2y$10$oMv39QUDncjgg46sIg7keuQ1LKWKILEpRvjghZPciHGdTLzJlEsri', 'Manu', NULL, NULL, '626686500', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2018-10-10 13:29:19', '2018-10-22 20:20:43', NULL),
('18f7f0b3-e58c-49e2-ab95-fefe897de617', 32, 'monica', 'trasnoseica@gmail.com', '$2y$10$LpWXymAbcUJwnFerV/QVDe2XFICRVY5D1EDYhZCEisoIbDo8X68uC', 'monica', NULL, NULL, '999468606', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2016-10-21 17:22:03', '2017-10-22 18:29:51', NULL),
('1e93668c-8b5d-4988-a3d1-bffcd8a7417e', 62, 'lore', 'lorelaiali@hotmail.com', '$2y$10$eMQGfyDSx4jjma0RmjJW/uxmcaGYMWRaRm6THB3ZTbv3fKvgNzFUK', 'lore', NULL, NULL, '666666666', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2018-10-24 20:29:44', '2018-10-24 21:27:09', NULL),
('30dbfe50-099a-46df-b8fb-caf4e6983c0d', 64, 'emma', 'holaemmaovin@hotmail.com', '$2y$10$6xCkLBdnHeHepoNIYsoIxO1zOSSrEood42DNmCk2IokpQO.G1wEb6', 'emma', NULL, NULL, '676116654', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2019-10-06 19:43:38', '2019-10-06 19:45:29', NULL),
('33d81274-7b8a-48ad-82cf-3087de06dac0', 28, 'fran', 'elchatarreroelectronico@gmail.com', '$2y$10$R4dbXKcZODtmXPUvF/2fXu8hozCAfRPt3RxH2blAEbawKZidfekTm', 'Fran', NULL, NULL, '616768298', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2016-10-21 17:19:50', '2018-10-19 14:10:15', NULL),
('3c4c8b18-c991-43f6-a484-23be4b42565e', 35, 'willy', 'williamboubeta@gmail.com', '$2y$10$BDpM12j.tEOBGXDE6A03subHThWmUjcoXraSpGaGBnlInJHNEMQ5S', 'Willy', NULL, NULL, '683127391', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2016-10-21 17:24:49', '2018-10-11 12:32:18', NULL),
('3e3d7f52-db85-473d-9c83-3d0d8f7ef9f0', 1, 'max', 'maxgomezparada@gmail.com', '$2y$10$Q7JNU6HT6/v5FXIpW6jEzeRSiRV4DWbJiUnHzkuk9xbiXNWnMEumC', 'Max', '', '', '650123804', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2016-10-19 00:45:55', '2019-10-18 14:14:18', NULL),
('4303263a-0b6c-40ff-95f6-c3563de7a013', 49, 'jero', 'jerop152@gmail.com', '$2y$10$FUHRu8VenK3zgHOnwO1wQuNMyEAV1o.MO/.fw.Rb/.mWpq2hNPyIi', 'jero', NULL, NULL, '666468602', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2017-10-21 09:51:34', '2018-10-29 06:51:25', NULL),
('4de03e76-2628-4061-889e-19911f7cc01e', 54, 'santi', 'santiago.paredes.cruz@gmail.com', '$2y$10$gSlCFIheIi./u7LUJ0phjuYtiLMmEYNZLt6TgT6dcjwDk5j3Jpc26', 'Santi Traficanti', NULL, NULL, '608265384', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2018-10-10 13:42:55', '2018-10-11 13:53:26', NULL),
('56588347-a0d7-4424-ad26-fc4fcba747eb', 39, 'iria', 'iriapisa@gmail.com', '$2y$10$MPOvJak/hJy9FG3NY6ZNkOBlj3AOFo84OpDoNaXP2EhvkSSV06kH.', 'iria', NULL, NULL, '633340988', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2016-10-21 17:26:58', '2017-10-23 23:20:06', NULL),
('56c7c858-df60-49a9-9bd9-f5040a1f952a', 52, 'cebrian', '', '$2y$10$fxTtzgDnY8Qz92AxXrI9luOmEGHUBDmubrdP5gLHq57QkgXdla3ZG', 'cebrian', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2018-10-10 13:28:52', '2018-10-10 13:28:52', NULL),
('58f41b46-8897-4ac0-8e33-a4c79172c9fb', 59, 'loli', '', '$2y$10$aN2xQ/xUOrn4iNJU2XVbouOjFa8iCLh7xBugQjf.hYoSmnRd5UMF2', 'loli', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2018-10-20 15:35:52', '2018-10-20 15:35:52', NULL),
('594260bf-cd34-4c8a-b9fd-58b29bcb2842', 34, 'dani', 'danicomes@hotmail.com', '$2y$10$LFzeUk4WKD07zvN9kaTdq.Rf6xUU4HRCoiE4YiRJfdsC4Zd2C7j0.', 'dani', NULL, NULL, '679947094', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2016-10-21 17:23:50', '2018-10-18 12:18:40', NULL),
('5a3d04ca-acf8-4110-95e8-6f8f79232060', 51, 'gabi', 'galigab@hotmail.es', '$2y$10$U8DGvNiOGP4HY2OS1vGX9OvtSRQRCHFVJpIWyf6bzxNO9jf74gf3S', 'gabi', NULL, NULL, '660062568', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2017-10-28 13:11:55', '2018-10-20 16:13:11', NULL),
('5c16538a-0d0a-4183-b2ea-a4cb2cfcfe2a', 48, 'sara', 'sara.choren@gmail.com', '$2y$10$K8klu/TxlnV/HJvFbXYvleiKjSHPpT101lA.p9kzM.E.W6NlwjlUm', 'Sara', NULL, NULL, '649024290', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2016-11-02 18:55:13', '2016-11-02 23:46:27', NULL),
('5c2f81fd-780f-4fd0-99af-23a813ea79b3', 21, 'herbello', 'alexherbello@hotmail.com', '$2y$10$Z7OLiiPFfQlQN42uytG6TOvPaVoLs7uT6ZxRAvRugATV2/nwrCtfK', 'Álex ', NULL, NULL, '616505797', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2016-10-21 17:16:24', '2018-10-31 13:34:36', NULL),
('5cbbd8ab-32a8-41b4-8633-b2302a73f60d', 63, 'baalhug', 'qwe@qwe.com', '$2y$10$x1VU82/u32LAo74kGgTX3ep/jsEbDeEVJ0.R8ROPI5f12Eucrz4uG', 'baalhug', NULL, NULL, '654321987', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2018-10-24 21:41:05', '2018-10-26 12:28:46', NULL),
('6192448a-c6cf-4ffa-92c7-d0b11b74f629', 15, 'rebe', 'larshem1@gmail.com', '$2y$10$Ujpf..VmKkO3FWqK8xCM..bYN/uDWLMk9Us6NRdpn7047Vc28GoBi', 'Rebe', NULL, NULL, '655027220', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2016-10-21 12:19:25', '2019-10-18 14:15:36', NULL),
('63d24792-9c5a-45e7-af41-19184caa3fff', 26, 'marques', 'markes.10@hotmail.com', '$2y$10$g3BG9mwGAnHn.AZ8jV9Ynu96v4ZKMyWoop55kJmFJlZXSgXYzqqmm', 'Marqués ', NULL, NULL, '661240455', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2016-10-21 17:18:43', '2018-10-11 13:04:07', NULL),
('682cf312-9a70-4b40-a7e3-6dd6d4afb25e', 65, 'adriana', 'pemadri@gmail.com', '$2y$10$KOk2yj5jmgRxLSkFMYcOIOrEIWQdgfEp1A0dcMzslSwkIJJMezZii', 'adriana', NULL, NULL, '663449228', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2019-10-15 12:26:32', '2019-10-15 12:38:47', NULL),
('697fc2e6-dfcf-4c23-901d-ef12d760491b', 33, 'puro', 'javiseijas81@gmail.com', '$2y$10$O0OJYx/szzrDggUdMeegUuxQMHK8s20E1jDHIe4RCVxDWdvaZPbPy', 'puro', NULL, NULL, '630033200', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2016-10-21 17:23:21', '2019-10-06 19:33:35', NULL),
('6ae6a5b1-88b1-48fc-923d-f4dbe6b4c929', 27, 'denis', 'sometimesoccurs@hotmail.com', '$2y$10$Vh2o9pNPFjxIxRId3PIc5.lQLHf9dQKnEros3B2ZVSFSn7Q2Q4SwS', 'denis', NULL, NULL, '661361258', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2016-10-21 17:19:26', '2018-10-19 11:19:00', NULL),
('707a332a-7931-4527-9a1f-cfc3f114e1d7', 37, 'maria', 'mariafaltri@gmail.com', '$2y$10$s6rH6mQZQa1scDw3/vF5yupJZMw4bi3sDWqzlOdkKKWTkpJvKViGa', 'maria', NULL, NULL, '658633143', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2016-10-21 17:25:28', '2018-10-16 21:57:33', NULL),
('7858508c-b4fe-4004-982e-0e88ea4ad4a2', 19, 'carlos', 'carlos-bc@hotmail.es', '$2y$10$yWon2QyFD0vkF0tOB2CdW.q5yZ269jPJy7ctdCp6mC4.nMji65Ziy', 'carlos', NULL, NULL, '678222130', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2016-10-21 17:15:48', '2018-10-19 12:54:14', NULL),
('8c052cad-30aa-4049-91d0-5b2429d7c581', 25, 'jorge', 'macjuncal@gmail.com', '$2y$10$8P12.67/vssnNU2.92A0d.Wxjmk/tWvpQaKPyX66Xdwh2vW2RapD6', 'jorge', NULL, NULL, '609431838', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2016-10-21 17:18:15', '2018-10-17 18:28:56', NULL),
('9030222b-8d63-414e-ba94-ca7efd14ac3e', 20, 'oscar', 'chokitosdelaria@hotmail.com', '$2y$10$sCTRXfhfvpD1xhXFjyiPouYAZ1rohO0gXQ9lDVohT.UmMgsT9dXVq', 'oscar', NULL, NULL, '607988728', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2016-10-21 17:16:09', '2018-10-11 17:47:54', NULL),
('9610c544-4b60-4f77-8100-bfb1b2b77018', 42, 'xan', 'xancampos@gmail.com', '$2y$10$2phQyxKBG8jZSttv5kDrvOe6DH3NghyeBXNdROZ.gd4fwLytlwKQO', 'xan', NULL, NULL, '620178961', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2016-10-21 17:28:19', '2016-10-25 12:56:58', NULL),
('9a0ae66b-7f0a-4644-ab3b-bbc338387185', 61, 'gdjazz', '', '$2y$10$zPFaknNmeSqXFtmoZRI.MeOYQyJbxx9mxhX1KUR6g6jRdKRkAxOrW', 'gdjazz', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2018-10-23 23:10:54', '2018-10-23 23:10:54', NULL),
('adf9a1d3-0404-4e96-aabd-61fc06ea9476', 0, 'superadmin', 'superadmin@example.com', '$2y$10$VvEiMcdXc8iGXM6aZhq5UetJneUB4gZ.NwQq79bfSLAohPctcE5du', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'superuser', '2019-10-09 11:29:05', '2019-10-18 13:56:15', NULL),
('afa7577f-59bb-4df0-b750-fd9dea6ed46a', 50, 'saul', 'saulchapela@gmail.com', '$2y$10$IJtyM7sO68b/XCnh4UkGo.Fc3BAIlchTjwy1379t9YsCknZtr8xiq', 'Saúl', NULL, NULL, '617113541', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2017-10-27 14:19:02', '2017-11-01 11:47:29', NULL),
('b2348b12-f6b7-4758-accd-fc0291e1cec8', 36, 'chame', 'chame644@gmail.com', '$2y$10$RyVK2BqvWcCF/ySdSUu0zOq1gHCG0BmkrxBFKwiHGRxKTjUpJ8IuW', 'chame', NULL, NULL, '647641571', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2016-10-21 17:25:09', '2018-10-28 16:56:24', NULL),
('b2efde4c-e55e-446d-9146-8cfbaeca72c3', 58, 'aaron', 'aaron.seijas@gmail.com', '$2y$10$Cvs55LX3petPy5f/rwUZy./fFNAr5ql6LFAXaikwv/rZYebIdgn6e', 'aaron', NULL, NULL, '659697379', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2018-10-19 12:28:20', '2018-10-19 15:38:24', NULL),
('b57e7c2f-9f6f-49cc-a99d-fad0da0de1c0', 57, 'paul', '', '$2y$10$uRkzcnlTDdr1NJ9ST3aH2exXP5ZrHFis3LzqpQVJeVAhWZ0q23YJq', 'paul', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2018-10-18 23:16:28', '2018-10-18 23:16:28', NULL),
('ce797503-e948-4bac-b53f-198fca6fbbd4', 55, 'iago', 'iagopousaben@gmail.com', '$2y$10$3UtNV.J8RZVJp6j4CVkaSOArBLcN/jPsbIGtMgSglsBNb3OuUUkFO', 'iago', NULL, NULL, '645468190', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2018-10-18 23:14:50', '2018-10-28 18:17:13', NULL),
('cfb9a2db-45ec-472f-a749-e8c6f48e266d', 40, 'jose', 'xo-shu@hotmail.com', '$2y$10$I1DYDsoPjE4uXazRCN7u..h0CrUoF43sHJpMjbyTFpkoWZ0FgbRW2', 'jose', NULL, NULL, '627352451', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2016-10-21 17:27:13', '2018-10-28 16:56:30', NULL),
('d12c031e-3628-4585-9c11-0557f086b835', 24, 'juanma', 'juanmaphantomons@gmail.com', '$2y$10$66nuxyZwOuTLNnF9hiAMSONQ1EI/I/rr91UzpSyxDpz6x8ir5TpPu', 'juanma', NULL, NULL, '690286254', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2016-10-21 17:17:48', '2018-10-19 11:44:22', NULL),
('d2b4a380-de23-4823-b6b3-7117eb950b56', 56, 'victor', 'victormfv.phc@gmail.com', '$2y$10$8dZVgAZQpES.IooLvSzkdO/dOr.4tSLuRvGbhCTCQNwdIaAD9OHn2', 'victor', NULL, NULL, '653681183', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2018-10-18 23:15:49', '2018-10-27 11:36:56', NULL),
('d882f52c-7e98-485c-b483-2a320a6e7981', 30, 'noemi', 'noemfv@yahoo.es', '$2y$10$sJ2sl5V4oU5ivcZ3wvU.wuS9mEIj3IiIOCQG8VxNIVG1b/2zHptVm', 'noemi', NULL, NULL, '699212878', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2016-10-21 17:21:24', '2018-10-28 18:22:01', NULL),
('e1a1051a-24d8-4960-a9ee-84be21b6cfa2', 38, 'berto', 'meddlebraxas@gmail.com', '$2y$10$Qv0z6cSIC.7SlAjQeUAB3.uEe.2ZE32W/edaojFclRJ96Rpjnp7NW', 'berto', NULL, NULL, '659720456', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2016-10-21 17:25:53', '2018-10-11 09:44:17', NULL),
('e1fc5fc5-c42c-4bb5-97c0-d88a75e61f9a', 23, 'nacho', 'rockswell77@gmail.com', '$2y$10$m9v81CagUzjucI4RT5e3t..TI0u4ZHu0Np2PiFUiNZAKUX4di8CyO', 'nacho', NULL, NULL, '693739193', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2016-10-21 17:17:23', '2018-10-19 12:43:54', NULL),
('efcf62f6-62e5-44ed-becd-af853db69a5f', 29, 'juansy', 'juansy1978@hotmail.com', '$2y$10$cBXiwIAc8wp.2uGW4zENzeErNcxNR42hV1RDmuUnryZIPrf8yHJ7G', 'juansy', NULL, NULL, '678993333', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2016-10-21 17:21:10', '2016-10-24 19:41:40', NULL),
('f26b98db-4590-4335-a3ad-e2340b8b2de3', 18, 'kurt', 'oneno.ruben@hotmail.com', '$2y$10$V8iW0X9xtKhLNIuH5/7G9.7k/MxiCfEN4eKTtqtgVPxTnPTOuftmO', 'kurtis', NULL, NULL, '669799519', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2016-10-21 17:15:30', '2019-10-11 22:01:54', NULL),
('f5616091-2207-4e7f-baab-1ffabff8301f', 60, 'elena', '', '$2y$10$jj8vtMWSFYRhQpaC7CdsEe8oBNk9l6Nz.jEhmAt3L2s7FMft/57RO', 'elena', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2018-10-21 01:28:14', '2018-10-21 01:28:14', NULL),
('f8265210-f8dc-4832-90fd-7625a1a76461', 4, 'cara', 'oscarrivas_@hotmail.com', '$2y$10$hEdtZHW3Gj93Ttj9QfJ0F.T.LqakzjZFTy1nGflB0Rq3VgLlZIKZK', 'Cara', NULL, NULL, '674026204', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'user', '2016-10-19 02:41:22', '2017-10-19 10:13:08', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users_selected_tunes`
--

CREATE TABLE `users_selected_tunes` (
  `user_id` char(36) NOT NULL,
  `tune_id` bigint(20) NOT NULL,
  `instrument_id` bigint(20) NOT NULL,
  `edition_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `users_selected_tunes`
--

INSERT INTO `users_selected_tunes` (`user_id`, `tune_id`, `instrument_id`, `edition_id`) VALUES
('03198792-5c0d-4473-b218-447a2ec7f1fd', 183, 2, 4),
('03198792-5c0d-4473-b218-447a2ec7f1fd', 184, 2, 4),
('03198792-5c0d-4473-b218-447a2ec7f1fd', 184, 5, 4),
('03198792-5c0d-4473-b218-447a2ec7f1fd', 186, 1, 4),
('03198792-5c0d-4473-b218-447a2ec7f1fd', 186, 5, 4),
('03198792-5c0d-4473-b218-447a2ec7f1fd', 189, 5, 4),
('03198792-5c0d-4473-b218-447a2ec7f1fd', 190, 4, 4),
('03198792-5c0d-4473-b218-447a2ec7f1fd', 194, 3, 4),
('03198792-5c0d-4473-b218-447a2ec7f1fd', 223, 5, 4),
('03198792-5c0d-4473-b218-447a2ec7f1fd', 230, 1, 4),
('078e48a4-5b2a-410f-8bef-33babe2378c3', 183, 2, 4),
('078e48a4-5b2a-410f-8bef-33babe2378c3', 187, 4, 4),
('078e48a4-5b2a-410f-8bef-33babe2378c3', 191, 4, 4),
('078e48a4-5b2a-410f-8bef-33babe2378c3', 193, 2, 4),
('078e48a4-5b2a-410f-8bef-33babe2378c3', 197, 4, 4),
('078e48a4-5b2a-410f-8bef-33babe2378c3', 202, 1, 4),
('078e48a4-5b2a-410f-8bef-33babe2378c3', 203, 1, 4),
('078e48a4-5b2a-410f-8bef-33babe2378c3', 204, 1, 4),
('078e48a4-5b2a-410f-8bef-33babe2378c3', 205, 4, 4),
('078e48a4-5b2a-410f-8bef-33babe2378c3', 207, 2, 4),
('078e48a4-5b2a-410f-8bef-33babe2378c3', 217, 4, 4),
('078e48a4-5b2a-410f-8bef-33babe2378c3', 218, 4, 4),
('078e48a4-5b2a-410f-8bef-33babe2378c3', 225, 2, 4),
('078e48a4-5b2a-410f-8bef-33babe2378c3', 228, 1, 4),
('1310f7d5-0bf8-4406-b57b-12a588cf0e18', 184, 2, 4),
('1310f7d5-0bf8-4406-b57b-12a588cf0e18', 190, 6, 4),
('1310f7d5-0bf8-4406-b57b-12a588cf0e18', 195, 6, 4),
('1310f7d5-0bf8-4406-b57b-12a588cf0e18', 196, 6, 4),
('1310f7d5-0bf8-4406-b57b-12a588cf0e18', 223, 6, 4),
('15685769-e355-421a-ab82-c19f0efcce87', 189, 1, 4),
('15685769-e355-421a-ab82-c19f0efcce87', 207, 2, 4),
('15685769-e355-421a-ab82-c19f0efcce87', 219, 1, 4),
('18f7f0b3-e58c-49e2-ab95-fefe897de617', 183, 2, 4),
('18f7f0b3-e58c-49e2-ab95-fefe897de617', 187, 1, 4),
('18f7f0b3-e58c-49e2-ab95-fefe897de617', 195, 1, 4),
('18f7f0b3-e58c-49e2-ab95-fefe897de617', 197, 1, 4),
('18f7f0b3-e58c-49e2-ab95-fefe897de617', 209, 2, 4),
('18f7f0b3-e58c-49e2-ab95-fefe897de617', 223, 2, 4),
('18f7f0b3-e58c-49e2-ab95-fefe897de617', 229, 2, 4),
('1d7ab30b-16ec-470a-aead-7f51346dcebb', 192, 6, 4),
('1d7ab30b-16ec-470a-aead-7f51346dcebb', 229, 6, 4),
('1e93668c-8b5d-4988-a3d1-bffcd8a7417e', 193, 2, 4),
('1e93668c-8b5d-4988-a3d1-bffcd8a7417e', 194, 6, 4),
('1e93668c-8b5d-4988-a3d1-bffcd8a7417e', 197, 2, 4),
('1e93668c-8b5d-4988-a3d1-bffcd8a7417e', 199, 6, 4),
('1e93668c-8b5d-4988-a3d1-bffcd8a7417e', 200, 6, 4),
('1e93668c-8b5d-4988-a3d1-bffcd8a7417e', 204, 2, 4),
('1e93668c-8b5d-4988-a3d1-bffcd8a7417e', 227, 2, 4),
('33d81274-7b8a-48ad-82cf-3087de06dac0', 191, 5, 4),
('33d81274-7b8a-48ad-82cf-3087de06dac0', 195, 5, 4),
('33d81274-7b8a-48ad-82cf-3087de06dac0', 202, 5, 4),
('33d81274-7b8a-48ad-82cf-3087de06dac0', 203, 2, 4),
('33d81274-7b8a-48ad-82cf-3087de06dac0', 206, 5, 4),
('33d81274-7b8a-48ad-82cf-3087de06dac0', 225, 5, 4),
('3c4c8b18-c991-43f6-a484-23be4b42565e', 183, 2, 4),
('3c4c8b18-c991-43f6-a484-23be4b42565e', 209, 2, 4),
('3c4c8b18-c991-43f6-a484-23be4b42565e', 227, 1, 4),
('3e3d7f52-db85-473d-9c83-3d0d8f7ef9f0', 184, 6, 4),
('3e3d7f52-db85-473d-9c83-3d0d8f7ef9f0', 191, 6, 4),
('3e3d7f52-db85-473d-9c83-3d0d8f7ef9f0', 197, 6, 4),
('3e3d7f52-db85-473d-9c83-3d0d8f7ef9f0', 198, 6, 4),
('3e3d7f52-db85-473d-9c83-3d0d8f7ef9f0', 209, 6, 4),
('3e3d7f52-db85-473d-9c83-3d0d8f7ef9f0', 211, 6, 4),
('3e3d7f52-db85-473d-9c83-3d0d8f7ef9f0', 228, 6, 4),
('4303263a-0b6c-40ff-95f6-c3563de7a013', 203, 3, 4),
('4303263a-0b6c-40ff-95f6-c3563de7a013', 206, 4, 4),
('4303263a-0b6c-40ff-95f6-c3563de7a013', 211, 4, 4),
('4303263a-0b6c-40ff-95f6-c3563de7a013', 228, 4, 4),
('4303263a-0b6c-40ff-95f6-c3563de7a013', 230, 4, 4),
('53f2bfa4-e4aa-429d-b177-2dea148fdeb3', 183, 3, 4),
('53f2bfa4-e4aa-429d-b177-2dea148fdeb3', 199, 3, 4),
('53f2bfa4-e4aa-429d-b177-2dea148fdeb3', 200, 4, 4),
('53f2bfa4-e4aa-429d-b177-2dea148fdeb3', 223, 3, 4),
('53f2bfa4-e4aa-429d-b177-2dea148fdeb3', 226, 3, 4),
('53f2bfa4-e4aa-429d-b177-2dea148fdeb3', 230, 3, 4),
('56588347-a0d7-4424-ad26-fc4fcba747eb', 183, 2, 4),
('56588347-a0d7-4424-ad26-fc4fcba747eb', 189, 2, 4),
('56588347-a0d7-4424-ad26-fc4fcba747eb', 225, 1, 4),
('56588347-a0d7-4424-ad26-fc4fcba747eb', 229, 5, 4),
('56588347-a0d7-4424-ad26-fc4fcba747eb', 230, 2, 4),
('594260bf-cd34-4c8a-b9fd-58b29bcb2842', 201, 1, 4),
('594260bf-cd34-4c8a-b9fd-58b29bcb2842', 207, 2, 4),
('594260bf-cd34-4c8a-b9fd-58b29bcb2842', 208, 1, 4),
('5a3d04ca-acf8-4110-95e8-6f8f79232060', 187, 3, 4),
('5a3d04ca-acf8-4110-95e8-6f8f79232060', 188, 3, 4),
('5a3d04ca-acf8-4110-95e8-6f8f79232060', 191, 3, 4),
('5a3d04ca-acf8-4110-95e8-6f8f79232060', 228, 3, 4),
('5c2f81fd-780f-4fd0-99af-23a813ea79b3', 189, 3, 4),
('5c2f81fd-780f-4fd0-99af-23a813ea79b3', 192, 3, 4),
('5c2f81fd-780f-4fd0-99af-23a813ea79b3', 193, 3, 4),
('5c2f81fd-780f-4fd0-99af-23a813ea79b3', 209, 3, 4),
('5c2f81fd-780f-4fd0-99af-23a813ea79b3', 217, 2, 4),
('5c2f81fd-780f-4fd0-99af-23a813ea79b3', 217, 3, 4),
('5cbbd8ab-32a8-41b4-8633-b2302a73f60d', 195, 7, 4),
('5cbbd8ab-32a8-41b4-8633-b2302a73f60d', 227, 2, 4),
('5cbbd8ab-32a8-41b4-8633-b2302a73f60d', 227, 7, 4),
('6192448a-c6cf-4ffa-92c7-d0b11b74f629', 183, 2, 4),
('6192448a-c6cf-4ffa-92c7-d0b11b74f629', 197, 2, 4),
('6192448a-c6cf-4ffa-92c7-d0b11b74f629', 209, 1, 4),
('6192448a-c6cf-4ffa-92c7-d0b11b74f629', 212, 1, 4),
('6192448a-c6cf-4ffa-92c7-d0b11b74f629', 223, 1, 4),
('6192448a-c6cf-4ffa-92c7-d0b11b74f629', 229, 1, 4),
('63d24792-9c5a-45e7-af41-19184caa3fff', 188, 5, 4),
('63d24792-9c5a-45e7-af41-19184caa3fff', 192, 5, 4),
('63d24792-9c5a-45e7-af41-19184caa3fff', 200, 5, 4),
('63d24792-9c5a-45e7-af41-19184caa3fff', 215, 5, 4),
('682cf312-9a70-4b40-a7e3-6dd6d4afb25e', 187, 9, 4),
('682cf312-9a70-4b40-a7e3-6dd6d4afb25e', 196, 7, 4),
('682cf312-9a70-4b40-a7e3-6dd6d4afb25e', 199, 7, 4),
('682cf312-9a70-4b40-a7e3-6dd6d4afb25e', 229, 7, 4),
('697fc2e6-dfcf-4c23-901d-ef12d760491b', 183, 2, 4),
('697fc2e6-dfcf-4c23-901d-ef12d760491b', 184, 2, 4),
('697fc2e6-dfcf-4c23-901d-ef12d760491b', 191, 2, 4),
('697fc2e6-dfcf-4c23-901d-ef12d760491b', 192, 2, 4),
('697fc2e6-dfcf-4c23-901d-ef12d760491b', 194, 1, 4),
('697fc2e6-dfcf-4c23-901d-ef12d760491b', 195, 2, 4),
('697fc2e6-dfcf-4c23-901d-ef12d760491b', 198, 1, 4),
('697fc2e6-dfcf-4c23-901d-ef12d760491b', 203, 2, 4),
('697fc2e6-dfcf-4c23-901d-ef12d760491b', 204, 2, 4),
('697fc2e6-dfcf-4c23-901d-ef12d760491b', 207, 2, 4),
('697fc2e6-dfcf-4c23-901d-ef12d760491b', 212, 2, 4),
('6ae6a5b1-88b1-48fc-923d-f4dbe6b4c929', 209, 5, 4),
('6ae6a5b1-88b1-48fc-923d-f4dbe6b4c929', 216, 5, 4),
('6ae6a5b1-88b1-48fc-923d-f4dbe6b4c929', 219, 5, 4),
('6ae6a5b1-88b1-48fc-923d-f4dbe6b4c929', 226, 5, 4),
('6ae6a5b1-88b1-48fc-923d-f4dbe6b4c929', 230, 5, 4),
('707a332a-7931-4527-9a1f-cfc3f114e1d7', 183, 2, 4),
('707a332a-7931-4527-9a1f-cfc3f114e1d7', 187, 2, 4),
('707a332a-7931-4527-9a1f-cfc3f114e1d7', 190, 2, 4),
('707a332a-7931-4527-9a1f-cfc3f114e1d7', 196, 1, 4),
('707a332a-7931-4527-9a1f-cfc3f114e1d7', 199, 2, 4),
('707a332a-7931-4527-9a1f-cfc3f114e1d7', 200, 1, 4),
('707a332a-7931-4527-9a1f-cfc3f114e1d7', 206, 1, 4),
('707a332a-7931-4527-9a1f-cfc3f114e1d7', 215, 1, 4),
('707a332a-7931-4527-9a1f-cfc3f114e1d7', 219, 4, 4),
('707a332a-7931-4527-9a1f-cfc3f114e1d7', 229, 2, 4),
('7858508c-b4fe-4004-982e-0e88ea4ad4a2', 193, 6, 4),
('7858508c-b4fe-4004-982e-0e88ea4ad4a2', 202, 6, 4),
('7858508c-b4fe-4004-982e-0e88ea4ad4a2', 205, 6, 4),
('7858508c-b4fe-4004-982e-0e88ea4ad4a2', 206, 6, 4),
('7858508c-b4fe-4004-982e-0e88ea4ad4a2', 225, 2, 4),
('7858508c-b4fe-4004-982e-0e88ea4ad4a2', 227, 6, 4),
('8c052cad-30aa-4049-91d0-5b2429d7c581', 183, 7, 4),
('8c052cad-30aa-4049-91d0-5b2429d7c581', 184, 7, 4),
('8c052cad-30aa-4049-91d0-5b2429d7c581', 192, 7, 4),
('8c052cad-30aa-4049-91d0-5b2429d7c581', 200, 3, 4),
('8c052cad-30aa-4049-91d0-5b2429d7c581', 206, 3, 4),
('8c052cad-30aa-4049-91d0-5b2429d7c581', 209, 7, 4),
('8c052cad-30aa-4049-91d0-5b2429d7c581', 215, 4, 4),
('8c052cad-30aa-4049-91d0-5b2429d7c581', 226, 7, 4),
('b2348b12-f6b7-4758-accd-fc0291e1cec8', 190, 1, 4),
('b2348b12-f6b7-4758-accd-fc0291e1cec8', 207, 2, 4),
('b2348b12-f6b7-4758-accd-fc0291e1cec8', 217, 1, 4),
('b2348b12-f6b7-4758-accd-fc0291e1cec8', 226, 1, 4),
('b2efde4c-e55e-446d-9146-8cfbaeca72c3', 199, 1, 4),
('b2efde4c-e55e-446d-9146-8cfbaeca72c3', 219, 2, 4),
('cfb9a2db-45ec-472f-a749-e8c6f48e266d', 187, 5, 4),
('cfb9a2db-45ec-472f-a749-e8c6f48e266d', 190, 5, 4),
('cfb9a2db-45ec-472f-a749-e8c6f48e266d', 197, 5, 4),
('cfb9a2db-45ec-472f-a749-e8c6f48e266d', 217, 5, 4),
('cfb9a2db-45ec-472f-a749-e8c6f48e266d', 218, 5, 4),
('d12c031e-3628-4585-9c11-0557f086b835', 183, 2, 4),
('d12c031e-3628-4585-9c11-0557f086b835', 184, 3, 4),
('d12c031e-3628-4585-9c11-0557f086b835', 195, 3, 4),
('d12c031e-3628-4585-9c11-0557f086b835', 198, 3, 4),
('d12c031e-3628-4585-9c11-0557f086b835', 201, 3, 4),
('d12c031e-3628-4585-9c11-0557f086b835', 202, 3, 4),
('d12c031e-3628-4585-9c11-0557f086b835', 227, 3, 4),
('d12c031e-3628-4585-9c11-0557f086b835', 229, 4, 4),
('d882f52c-7e98-485c-b483-2a320a6e7981', 183, 1, 4),
('d882f52c-7e98-485c-b483-2a320a6e7981', 193, 1, 4),
('d882f52c-7e98-485c-b483-2a320a6e7981', 211, 1, 4),
('d882f52c-7e98-485c-b483-2a320a6e7981', 227, 2, 4),
('d882f52c-7e98-485c-b483-2a320a6e7981', 229, 2, 4),
('e1a1051a-24d8-4960-a9ee-84be21b6cfa2', 187, 7, 4),
('e1a1051a-24d8-4960-a9ee-84be21b6cfa2', 193, 7, 4),
('e1a1051a-24d8-4960-a9ee-84be21b6cfa2', 194, 5, 4),
('e1a1051a-24d8-4960-a9ee-84be21b6cfa2', 197, 3, 4),
('e1a1051a-24d8-4960-a9ee-84be21b6cfa2', 199, 5, 4),
('e1a1051a-24d8-4960-a9ee-84be21b6cfa2', 200, 7, 4),
('e1a1051a-24d8-4960-a9ee-84be21b6cfa2', 203, 7, 4),
('e1a1051a-24d8-4960-a9ee-84be21b6cfa2', 205, 5, 4),
('e1a1051a-24d8-4960-a9ee-84be21b6cfa2', 217, 7, 4),
('e1a1051a-24d8-4960-a9ee-84be21b6cfa2', 230, 7, 4),
('e1fc5fc5-c42c-4bb5-97c0-d88a75e61f9a', 188, 1, 4),
('e1fc5fc5-c42c-4bb5-97c0-d88a75e61f9a', 188, 7, 4),
('e1fc5fc5-c42c-4bb5-97c0-d88a75e61f9a', 190, 3, 4),
('e1fc5fc5-c42c-4bb5-97c0-d88a75e61f9a', 191, 1, 4),
('e1fc5fc5-c42c-4bb5-97c0-d88a75e61f9a', 193, 5, 4),
('e1fc5fc5-c42c-4bb5-97c0-d88a75e61f9a', 196, 3, 4),
('e1fc5fc5-c42c-4bb5-97c0-d88a75e61f9a', 203, 5, 4),
('e1fc5fc5-c42c-4bb5-97c0-d88a75e61f9a', 205, 1, 4),
('e1fc5fc5-c42c-4bb5-97c0-d88a75e61f9a', 205, 3, 4),
('e1fc5fc5-c42c-4bb5-97c0-d88a75e61f9a', 208, 3, 4),
('e1fc5fc5-c42c-4bb5-97c0-d88a75e61f9a', 211, 3, 4),
('e1fc5fc5-c42c-4bb5-97c0-d88a75e61f9a', 217, 6, 4),
('e1fc5fc5-c42c-4bb5-97c0-d88a75e61f9a', 219, 3, 4),
('e1fc5fc5-c42c-4bb5-97c0-d88a75e61f9a', 227, 5, 4),
('eed947e5-8779-4e98-9c0a-b283465963d1', 183, 2, 4),
('eed947e5-8779-4e98-9c0a-b283465963d1', 187, 6, 4),
('eed947e5-8779-4e98-9c0a-b283465963d1', 200, 2, 4),
('efcf62f6-62e5-44ed-becd-af853db69a5f', 183, 5, 4),
('efcf62f6-62e5-44ed-becd-af853db69a5f', 196, 5, 4),
('efcf62f6-62e5-44ed-becd-af853db69a5f', 198, 5, 4),
('efcf62f6-62e5-44ed-becd-af853db69a5f', 228, 5, 4),
('f26b98db-4590-4335-a3ad-e2340b8b2de3', 183, 6, 4),
('f26b98db-4590-4335-a3ad-e2340b8b2de3', 188, 6, 4),
('f26b98db-4590-4335-a3ad-e2340b8b2de3', 203, 6, 4),
('f26b98db-4590-4335-a3ad-e2340b8b2de3', 212, 6, 4),
('f26b98db-4590-4335-a3ad-e2340b8b2de3', 219, 6, 4),
('f26b98db-4590-4335-a3ad-e2340b8b2de3', 226, 6, 4),
('f26b98db-4590-4335-a3ad-e2340b8b2de3', 230, 6, 4),
('f8265210-f8dc-4832-90fd-7625a1a76461', 184, 1, 4),
('f8265210-f8dc-4832-90fd-7625a1a76461', 192, 1, 4),
('f8265210-f8dc-4832-90fd-7625a1a76461', 211, 5, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users_tunes_instruments_2016`
--

CREATE TABLE `users_tunes_instruments_2016` (
  `id` bigint(20) NOT NULL,
  `tunes_instrument_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `users_tunes_instruments_2016`
--

INSERT INTO `users_tunes_instruments_2016` (`id`, `tunes_instrument_id`, `user_id`, `created`, `updated`) VALUES
(1, 63, 4, '2016-10-23 00:37:32', '2016-10-23 00:37:32'),
(2, 322, 4, '2016-10-23 00:40:46', '2016-10-23 00:40:46'),
(3, 307, 4, '2016-10-23 00:41:09', '2016-10-23 00:41:09'),
(4, 53, 38, '2016-10-23 00:43:07', '2016-10-23 00:43:07'),
(5, 59, 27, '2016-10-23 00:44:41', '2016-10-23 00:44:41'),
(6, 61, 33, '2016-10-23 00:45:05', '2016-10-23 00:45:05'),
(7, 65, 30, '2016-10-23 00:45:25', '2016-10-23 00:45:25'),
(8, 89, 35, '2016-10-23 00:46:52', '2016-10-23 00:46:52'),
(12, 92, 40, '2016-10-23 00:47:51', '2016-10-23 00:47:51'),
(14, 151, 31, '2016-10-23 00:48:42', '2016-10-23 00:48:42'),
(15, 154, 27, '2016-10-23 00:48:57', '2016-10-23 00:48:57'),
(17, 155, 18, '2016-10-23 00:50:12', '2016-10-23 00:50:12'),
(18, 170, 15, '2016-10-23 00:50:36', '2016-10-23 00:50:36'),
(19, 175, 15, '2016-10-23 00:50:53', '2016-10-23 00:50:53'),
(20, 233, 1, '2016-10-23 00:51:39', '2016-10-23 00:51:39'),
(21, 234, 15, '2016-10-23 00:52:21', '2016-10-23 00:52:21'),
(22, 237, 26, '2016-10-23 00:52:34', '2016-10-23 00:52:34'),
(23, 238, 17, '2016-10-23 00:52:46', '2016-10-23 00:52:46'),
(24, 265, 15, '2016-10-23 00:53:24', '2016-10-23 00:53:24'),
(25, 267, 24, '2016-10-23 00:53:35', '2016-10-23 00:53:35'),
(26, 345, 38, '2016-10-23 00:53:54', '2016-10-23 00:53:54'),
(27, 347, 33, '2016-10-23 00:54:16', '2016-10-23 00:54:16'),
(28, 378, 26, '2016-10-23 00:54:56', '2016-10-23 00:54:56'),
(30, 381, 35, '2016-10-23 00:55:47', '2016-10-23 00:55:47'),
(32, 391, 36, '2016-10-23 00:56:24', '2016-10-23 00:56:24'),
(35, 263, 38, '2016-10-23 02:00:31', '2016-10-23 02:00:31'),
(37, 138, 38, '2016-10-23 02:01:22', '2016-10-23 02:01:22'),
(38, 136, 33, '2016-10-23 02:02:05', '2016-10-23 02:02:05'),
(39, 40, 33, '2016-10-23 02:04:08', '2016-10-23 02:04:08'),
(40, 403, 26, '2016-10-23 02:22:42', '2016-10-23 02:22:42'),
(41, 394, 26, '2016-10-23 02:25:30', '2016-10-23 02:25:30'),
(42, 395, 18, '2016-10-23 02:30:12', '2016-10-23 02:30:12'),
(43, 55, 34, '2016-10-23 03:13:36', '2016-10-23 03:13:36'),
(44, 376, 30, '2016-10-23 05:42:30', '2016-10-23 05:42:30'),
(50, 131, 30, '2016-10-23 06:15:44', '2016-10-23 06:15:44'),
(51, 309, 30, '2016-10-23 06:17:49', '2016-10-23 06:17:49'),
(52, 236, 31, '2016-10-23 09:36:55', '2016-10-23 09:36:55'),
(53, 140, 31, '2016-10-23 09:52:51', '2016-10-23 09:52:51'),
(55, 235, 21, '2016-10-23 10:19:36', '2016-10-23 10:19:36'),
(56, 402, 21, '2016-10-23 10:20:07', '2016-10-23 10:20:07'),
(57, 393, 21, '2016-10-23 10:20:47', '2016-10-23 10:20:47'),
(58, 349, 24, '2016-10-23 10:43:53', '2016-10-23 10:43:53'),
(59, 85, 24, '2016-10-23 10:44:03', '2016-10-23 10:44:03'),
(60, 171, 24, '2016-10-23 10:44:17', '2016-10-23 10:44:17'),
(62, 187, 24, '2016-10-23 10:44:57', '2016-10-23 10:44:57'),
(63, 306, 24, '2016-10-23 10:45:01', '2016-10-23 10:45:01'),
(64, 146, 24, '2016-10-23 10:45:22', '2016-10-23 10:45:22'),
(66, 290, 24, '2016-10-23 10:45:41', '2016-10-23 10:45:41'),
(67, 300, 24, '2016-10-23 10:45:57', '2016-10-23 10:45:57'),
(68, 142, 24, '2016-10-23 10:46:25', '2016-10-23 10:46:25'),
(69, 202, 24, '2016-10-23 10:46:29', '2016-10-23 10:46:29'),
(70, 132, 24, '2016-10-23 10:46:46', '2016-10-23 10:46:46'),
(71, 261, 24, '2016-10-23 10:47:38', '2016-10-23 10:47:38'),
(72, 404, 17, '2016-10-23 11:25:12', '2016-10-23 11:25:12'),
(75, 141, 39, '2016-10-23 12:35:13', '2016-10-23 12:35:13'),
(76, 245, 39, '2016-10-23 12:37:57', '2016-10-23 12:37:57'),
(77, 382, 38, '2016-10-23 12:44:16', '2016-10-23 12:44:16'),
(79, 90, 20, '2016-10-23 12:44:51', '2016-10-23 12:44:51'),
(80, 383, 35, '2016-10-23 12:45:11', '2016-10-23 12:45:11'),
(81, 91, 35, '2016-10-23 12:45:13', '2016-10-23 12:45:13'),
(82, 40, 4, '2016-10-23 12:47:22', '2016-10-23 12:47:22'),
(83, 246, 4, '2016-10-23 12:48:26', '2016-10-23 12:48:26'),
(84, 228, 4, '2016-10-23 12:57:07', '2016-10-23 12:57:07'),
(85, 193, 27, '2016-10-23 13:40:29', '2016-10-23 13:40:29'),
(86, 54, 18, '2016-10-23 14:18:22', '2016-10-23 14:18:22'),
(87, 145, 36, '2016-10-23 15:10:32', '2016-10-23 15:10:32'),
(88, 400, 44, '2016-10-23 17:07:55', '2016-10-23 17:07:55'),
(93, 84, 39, '2016-10-23 21:06:21', '2016-10-23 21:06:21'),
(94, 266, 39, '2016-10-23 21:15:23', '2016-10-23 21:15:23'),
(96, 254, 1, '2016-10-23 22:16:30', '2016-10-23 22:16:30'),
(97, 220, 1, '2016-10-23 22:16:33', '2016-10-23 22:16:33'),
(98, 173, 1, '2016-10-23 22:17:01', '2016-10-23 22:17:01'),
(99, 249, 1, '2016-10-23 22:17:49', '2016-10-23 22:17:49'),
(100, 246, 15, '2016-10-23 22:18:07', '2016-10-23 22:18:07'),
(101, 338, 15, '2016-10-23 22:23:19', '2016-10-23 22:23:19'),
(103, 418, 15, '2016-10-23 22:39:27', '2016-10-23 22:39:27'),
(104, 418, 1, '2016-10-23 22:41:39', '2016-10-23 22:41:39'),
(105, 385, 17, '2016-10-23 23:02:08', '2016-10-23 23:02:08'),
(106, 291, 1, '2016-10-24 09:21:58', '2016-10-24 09:21:58'),
(111, 416, 1, '2016-10-24 12:29:50', '2016-10-24 12:29:50'),
(112, 293, 44, '2016-10-24 13:25:11', '2016-10-24 13:25:11'),
(114, 246, 23, '2016-10-24 14:48:04', '2016-10-24 14:48:04'),
(119, 270, 16, '2016-10-24 16:51:56', '2016-10-24 16:51:56'),
(120, 148, 16, '2016-10-24 16:53:54', '2016-10-24 16:53:54'),
(121, 68, 16, '2016-10-24 17:01:25', '2016-10-24 17:01:25'),
(122, 419, 30, '2016-10-24 17:15:26', '2016-10-24 17:15:26'),
(123, 110, 32, '2016-10-24 17:16:09', '2016-10-24 17:16:09'),
(124, 216, 4, '2016-10-24 17:16:35', '2016-10-24 17:16:35'),
(125, 422, 32, '2016-10-24 17:25:22', '2016-10-24 17:25:22'),
(126, 419, 33, '2016-10-24 17:31:53', '2016-10-24 17:31:53'),
(127, 39, 31, '2016-10-24 17:41:57', '2016-10-24 17:41:57'),
(128, 298, 31, '2016-10-24 17:43:27', '2016-10-24 17:43:27'),
(129, 251, 46, '2016-10-24 18:07:22', '2016-10-24 18:07:22'),
(131, 252, 46, '2016-10-24 18:07:33', '2016-10-24 18:07:33'),
(132, 269, 28, '2016-10-24 18:31:52', '2016-10-24 18:31:52'),
(133, 86, 28, '2016-10-24 18:34:44', '2016-10-24 18:34:44'),
(134, 219, 28, '2016-10-24 18:37:34', '2016-10-24 18:37:34'),
(137, 185, 30, '2016-10-24 18:48:36', '2016-10-24 18:48:36'),
(138, 343, 30, '2016-10-24 18:49:03', '2016-10-24 18:49:03'),
(140, 191, 30, '2016-10-24 18:54:12', '2016-10-24 18:54:12'),
(141, 50, 30, '2016-10-24 18:59:03', '2016-10-24 18:59:03'),
(142, 93, 16, '2016-10-24 19:16:55', '2016-10-24 19:16:55'),
(143, 424, 4, '2016-10-24 19:32:20', '2016-10-24 19:32:20'),
(144, 172, 29, '2016-10-24 19:42:20', '2016-10-24 19:42:20'),
(145, 253, 29, '2016-10-24 19:43:05', '2016-10-24 19:43:05'),
(146, 213, 29, '2016-10-24 19:45:14', '2016-10-24 19:45:14'),
(147, 384, 40, '2016-10-24 20:12:15', '2016-10-24 20:12:15'),
(148, 43, 19, '2016-10-24 20:56:28', '2016-10-24 20:56:28'),
(149, 264, 19, '2016-10-24 20:57:54', '2016-10-24 20:57:54'),
(150, 302, 19, '2016-10-24 20:58:09', '2016-10-24 20:58:09'),
(151, 375, 23, '2016-10-24 21:59:31', '2016-10-24 21:59:31'),
(153, 262, 23, '2016-10-24 22:04:53', '2016-10-24 22:04:53'),
(155, 260, 33, '2016-10-25 01:13:13', '2016-10-25 01:13:13'),
(158, 304, 34, '2016-10-25 08:54:59', '2016-10-25 08:54:59'),
(159, 149, 34, '2016-10-25 08:55:16', '2016-10-25 08:55:16'),
(161, 392, 34, '2016-10-25 08:56:44', '2016-10-25 08:56:44'),
(162, 325, 29, '2016-10-25 09:43:30', '2016-10-25 09:43:30'),
(165, 210, 37, '2016-10-25 13:57:47', '2016-10-25 13:57:47'),
(166, 212, 25, '2016-10-25 13:59:12', '2016-10-25 13:59:12'),
(167, 408, 25, '2016-10-25 14:05:04', '2016-10-25 14:05:04'),
(169, 112, 25, '2016-10-25 14:06:19', '2016-10-25 14:06:19'),
(172, 52, 25, '2016-10-25 14:11:30', '2016-10-25 14:11:30'),
(173, 406, 37, '2016-10-25 14:12:47', '2016-10-25 14:12:47'),
(174, 51, 37, '2016-10-25 14:12:52', '2016-10-25 14:12:52'),
(175, 111, 37, '2016-10-25 14:12:56', '2016-10-25 14:12:56'),
(176, 190, 44, '2016-10-25 16:48:59', '2016-10-25 16:48:59'),
(178, 266, 33, '2016-10-26 10:06:50', '2016-10-26 10:06:50'),
(179, 326, 18, '2016-10-26 17:24:44', '2016-10-26 17:24:44'),
(180, 87, 16, '2016-10-26 17:25:22', '2016-10-26 17:25:22'),
(181, 64, 16, '2016-10-26 17:27:56', '2016-10-26 17:27:56'),
(182, 297, 16, '2016-10-26 17:28:47', '2016-10-26 17:28:47'),
(183, 114, 19, '2016-10-26 17:31:45', '2016-10-26 17:31:45'),
(184, 425, 19, '2016-10-26 17:31:57', '2016-10-26 17:31:57'),
(185, 346, 19, '2016-10-26 17:33:03', '2016-10-26 17:33:03'),
(186, 292, 19, '2016-10-26 17:35:07', '2016-10-26 17:35:07'),
(187, 194, 19, '2016-10-26 17:36:11', '2016-10-26 17:36:11'),
(188, 410, 1, '2016-10-26 17:36:35', '2016-10-26 17:36:35'),
(189, 308, 1, '2016-10-26 17:36:50', '2016-10-26 17:36:50'),
(190, 189, 1, '2016-10-26 17:36:51', '2016-10-26 17:36:51'),
(191, 214, 1, '2016-10-26 17:37:07', '2016-10-26 17:37:07'),
(192, 188, 27, '2016-10-26 17:42:27', '2016-10-26 17:42:27'),
(194, 113, 27, '2016-10-26 17:43:31', '2016-10-26 17:43:31'),
(195, 147, 26, '2016-10-26 17:45:19', '2016-10-26 17:45:19'),
(196, 42, 28, '2016-10-26 17:45:52', '2016-10-26 17:45:52'),
(197, 296, 28, '2016-10-26 17:46:03', '2016-10-26 17:46:03'),
(198, 409, 29, '2016-10-26 17:46:14', '2016-10-26 17:46:14'),
(199, 67, 27, '2016-10-26 17:46:55', '2016-10-26 17:46:55'),
(200, 301, 26, '2016-10-26 17:49:21', '2016-10-26 17:49:21'),
(201, 288, 28, '2016-10-26 17:52:12', '2016-10-26 17:52:12'),
(202, 289, 4, '2016-10-26 17:52:28', '2016-10-26 17:52:28'),
(203, 295, 23, '2016-10-26 17:54:08', '2016-10-26 17:54:08'),
(206, 153, 23, '2016-10-26 18:02:40', '2016-10-26 18:02:40'),
(207, 152, 46, '2016-10-26 18:03:08', '2016-10-26 18:03:08'),
(209, 344, 46, '2016-10-26 18:04:13', '2016-10-26 18:04:13'),
(211, 218, 23, '2016-10-26 18:06:52', '2016-10-26 18:06:52'),
(212, 423, 21, '2016-10-26 18:07:58', '2016-10-26 18:07:58'),
(213, 62, 23, '2016-10-26 18:08:31', '2016-10-26 18:08:31'),
(214, 66, 38, '2016-10-26 18:09:31', '2016-10-26 18:09:31'),
(215, 342, 4, '2016-10-26 18:10:00', '2016-10-26 18:10:00'),
(216, 41, 25, '2016-10-26 20:34:22', '2016-10-26 20:34:22'),
(217, 343, 38, '2016-10-26 22:11:08', '2016-10-26 22:11:08'),
(219, 407, 32, '2016-10-27 17:34:18', '2016-10-27 17:34:18'),
(220, 246, 32, '2016-10-27 17:34:42', '2016-10-27 17:34:42'),
(221, 305, 32, '2016-10-27 17:34:58', '2016-10-27 17:34:58'),
(224, 217, 23, '2016-10-28 21:52:00', '2016-10-28 21:52:00'),
(225, 246, 34, '2016-10-29 17:06:43', '2016-10-29 17:06:43'),
(226, 411, 42, '2016-10-30 11:14:59', '2016-10-30 11:14:59'),
(227, 250, 42, '2016-10-30 11:15:09', '2016-10-30 11:15:09'),
(228, 195, 42, '2016-10-30 11:15:24', '2016-10-30 11:15:24'),
(229, 221, 42, '2016-10-30 11:15:53', '2016-10-30 11:15:53'),
(230, 405, 42, '2016-10-30 11:17:05', '2016-10-30 11:17:05'),
(231, 323, 46, '2016-10-30 11:17:42', '2016-10-30 11:17:42'),
(233, 247, 25, '2016-11-01 17:47:18', '2016-11-01 17:47:18'),
(235, 248, 38, '2016-11-01 17:58:41', '2016-11-01 17:58:41'),
(236, 192, 46, '2016-11-01 18:11:35', '2016-11-01 18:11:35'),
(238, 44, 48, '2016-11-03 09:50:43', '2016-11-03 09:50:43'),
(239, 303, 48, '2016-11-03 09:50:48', '2016-11-03 09:50:48');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users_tunes_instruments_2017`
--

CREATE TABLE `users_tunes_instruments_2017` (
  `id` bigint(20) NOT NULL,
  `tunes_instrument_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `users_tunes_instruments_2017`
--

INSERT INTO `users_tunes_instruments_2017` (`id`, `tunes_instrument_id`, `user_id`, `created`, `updated`) VALUES
(2, 426, 4, '2017-10-26 17:03:42', '2017-10-26 17:03:42'),
(3, 428, 4, '2017-10-26 17:03:53', '2017-10-26 17:03:53'),
(4, 528, 15, '2017-10-26 17:04:24', '2017-10-26 17:04:24'),
(5, 563, 15, '2017-10-26 17:04:53', '2017-10-26 17:04:53'),
(6, 500, 23, '2017-10-26 17:05:17', '2017-10-26 17:05:17'),
(7, 502, 23, '2017-10-26 17:05:18', '2017-10-26 17:05:18'),
(8, 526, 29, '2017-10-26 17:05:49', '2017-10-26 17:05:49'),
(9, 515, 29, '2017-10-26 17:05:55', '2017-10-26 17:05:55'),
(10, 546, 30, '2017-10-26 17:06:28', '2017-10-26 17:06:28'),
(11, 540, 30, '2017-10-26 17:06:31', '2017-10-26 17:06:31'),
(12, 494, 30, '2017-10-26 17:06:39', '2017-10-26 17:06:39'),
(13, 477, 31, '2017-10-26 17:07:15', '2017-10-26 17:07:15'),
(14, 579, 32, '2017-10-26 17:07:35', '2017-10-26 17:07:35'),
(15, 506, 33, '2017-10-26 17:07:53', '2017-10-26 17:07:53'),
(17, 569, 35, '2017-10-26 17:08:24', '2017-10-26 17:08:24'),
(19, 534, 36, '2017-10-26 17:08:51', '2017-10-26 17:08:51'),
(20, 560, 38, '2017-10-26 17:09:14', '2017-10-26 17:09:14'),
(21, 518, 39, '2017-10-26 17:09:33', '2017-10-26 17:09:33'),
(23, 483, 46, '2017-10-26 17:10:39', '2017-10-26 17:10:39'),
(24, 484, 46, '2017-10-26 17:10:46', '2017-10-26 17:10:46'),
(25, 575, 49, '2017-10-26 17:10:59', '2017-10-26 17:10:59'),
(26, 449, 4, '2017-10-26 17:12:15', '2017-10-26 17:12:15'),
(27, 574, 4, '2017-10-26 17:29:57', '2017-10-26 17:29:57'),
(28, 116, 4, '2017-10-26 17:30:48', '2017-10-26 17:30:48'),
(29, 438, 17, '2017-10-26 17:31:16', '2017-10-26 17:32:10'),
(30, 505, 25, '2017-10-26 17:33:10', '2017-10-26 17:33:10'),
(31, 523, 25, '2017-10-26 17:33:19', '2017-10-26 17:33:19'),
(32, 456, 31, '2017-10-26 17:36:30', '2017-10-26 17:36:30'),
(34, 116, 39, '2017-10-26 17:38:38', '2017-10-26 17:38:38'),
(36, 451, 31, '2017-10-26 17:42:24', '2017-10-26 17:42:24'),
(37, 513, 31, '2017-10-26 17:42:40', '2017-10-26 17:42:40'),
(39, 504, 18, '2017-10-26 17:45:15', '2017-10-26 17:45:15'),
(40, 489, 23, '2017-10-26 17:48:56', '2017-10-26 17:48:56'),
(42, 476, 23, '2017-10-26 17:52:27', '2017-10-26 17:52:27'),
(45, 70, 30, '2017-10-26 17:56:20', '2017-10-26 17:56:20'),
(47, 69, 23, '2017-10-26 17:56:43', '2017-10-26 17:56:43'),
(48, 71, 23, '2017-10-26 17:56:47', '2017-10-26 17:56:47'),
(49, 453, 18, '2017-10-26 18:00:37', '2017-10-26 18:00:37'),
(50, 501, 30, '2017-10-26 18:00:38', '2017-10-26 18:00:38'),
(51, 452, 23, '2017-10-26 18:01:15', '2017-10-26 18:01:15'),
(53, 427, 20, '2017-10-26 18:03:33', '2017-10-26 18:03:33'),
(54, 524, 31, '2017-10-26 18:08:05', '2017-10-26 18:08:05'),
(55, 415, 27, '2017-10-26 18:10:54', '2017-10-26 18:10:54'),
(57, 437, 27, '2017-10-26 18:14:15', '2017-10-26 18:14:15'),
(61, 116, 15, '2017-10-26 18:27:00', '2017-10-26 18:27:00'),
(66, 541, 33, '2017-10-26 18:56:08', '2017-10-26 18:56:08'),
(67, 363, 33, '2017-10-26 18:58:24', '2017-10-26 18:58:24'),
(69, 435, 36, '2017-10-26 19:05:10', '2017-10-26 19:05:10'),
(71, 572, 26, '2017-10-26 19:39:33', '2017-10-26 19:39:33'),
(72, 480, 26, '2017-10-26 19:40:20', '2017-10-26 19:40:20'),
(77, 589, 25, '2017-10-26 19:45:47', '2017-10-26 19:45:47'),
(80, 471, 44, '2017-10-26 20:28:20', '2017-10-26 23:17:05'),
(81, 543, 38, '2017-10-26 20:38:57', '2017-10-26 20:38:57'),
(82, 531, 38, '2017-10-26 20:39:09', '2017-10-26 20:39:09'),
(83, 497, 38, '2017-10-26 20:39:34', '2017-10-26 20:39:34'),
(84, 476, 38, '2017-10-26 20:39:55', '2017-10-26 20:39:55'),
(86, 436, 20, '2017-10-26 20:45:34', '2017-10-26 20:45:34'),
(87, 429, 40, '2017-10-26 23:20:39', '2017-10-26 23:20:39'),
(88, 510, 1, '2017-10-26 23:33:34', '2017-10-26 23:33:34'),
(89, 444, 23, '2017-10-26 23:49:38', '2017-10-26 23:49:38'),
(90, 503, 4, '2017-10-26 23:54:00', '2017-10-26 23:54:00'),
(91, 507, 4, '2017-10-26 23:55:20', '2017-10-26 23:55:20'),
(92, 444, 4, '2017-10-26 23:55:50', '2017-10-26 23:55:50'),
(93, 469, 33, '2017-10-27 00:53:50', '2017-10-27 00:53:50'),
(94, 366, 27, '2017-10-27 00:54:54', '2017-10-27 00:54:54'),
(97, 475, 17, '2017-10-27 01:15:16', '2017-10-27 01:15:16'),
(98, 444, 35, '2017-10-27 09:07:24', '2017-10-27 09:07:24'),
(99, 512, 37, '2017-10-27 09:17:37', '2017-10-27 09:17:37'),
(100, 557, 37, '2017-10-27 09:22:43', '2017-10-27 09:22:43'),
(103, 559, 25, '2017-10-27 10:38:42', '2017-10-27 10:38:42'),
(104, 514, 25, '2017-10-27 10:42:20', '2017-10-27 10:42:20'),
(108, 586, 23, '2017-10-27 12:53:59', '2017-10-27 12:53:59'),
(110, 444, 36, '2017-10-27 14:06:24', '2017-10-27 14:06:24'),
(112, 444, 15, '2017-10-27 14:26:41', '2017-11-02 16:33:44'),
(113, 509, 28, '2017-10-27 16:42:38', '2017-10-27 16:42:38'),
(114, 72, 28, '2017-10-27 16:46:18', '2017-10-27 16:46:18'),
(115, 474, 28, '2017-10-27 16:47:14', '2017-10-27 16:47:14'),
(116, 566, 28, '2017-10-27 16:49:01', '2017-10-27 16:49:01'),
(117, 491, 38, '2017-10-27 20:17:05', '2017-10-27 20:17:05'),
(118, 115, 44, '2017-10-27 22:50:35', '2017-10-27 22:50:35'),
(119, 454, 31, '2017-10-27 22:53:19', '2017-10-27 22:53:19'),
(120, 444, 31, '2017-10-27 23:40:29', '2017-10-27 23:40:29'),
(123, 582, 18, '2017-10-28 00:20:16', '2017-10-28 00:20:16'),
(124, 516, 18, '2017-10-28 00:21:37', '2017-10-28 00:21:37'),
(125, 492, 18, '2017-10-28 00:22:00', '2017-10-28 00:22:00'),
(127, 584, 44, '2017-10-28 02:18:21', '2017-10-28 02:18:21'),
(129, 571, 51, '2017-10-28 13:20:54', '2017-10-28 13:20:54'),
(130, 525, 51, '2017-10-28 13:21:59', '2017-10-28 13:21:59'),
(131, 580, 51, '2017-10-28 13:23:56', '2017-10-28 13:23:56'),
(132, 444, 46, '2017-10-28 13:29:40', '2017-10-28 13:29:40'),
(133, 570, 21, '2017-10-28 14:06:31', '2017-10-28 14:06:31'),
(134, 479, 21, '2017-10-28 14:07:43', '2017-10-28 14:07:43'),
(135, 490, 21, '2017-10-28 14:08:06', '2017-10-28 14:08:06'),
(136, 564, 30, '2017-10-28 14:16:13', '2017-10-28 14:16:13'),
(137, 430, 17, '2017-10-28 14:26:59', '2017-10-28 14:26:59'),
(138, 544, 16, '2017-10-28 14:34:29', '2017-10-28 14:34:29'),
(139, 588, 16, '2017-10-28 15:02:45', '2017-10-28 15:02:45'),
(140, 587, 29, '2017-10-28 17:10:41', '2017-10-28 17:10:41'),
(141, 457, 29, '2017-10-28 17:14:21', '2017-10-28 17:14:21'),
(142, 486, 29, '2017-10-28 17:14:28', '2017-10-28 17:14:28'),
(143, 444, 32, '2017-10-28 17:36:27', '2017-10-28 17:36:27'),
(145, 487, 1, '2017-10-28 18:33:56', '2017-10-28 18:33:56'),
(146, 530, 49, '2017-10-28 20:01:36', '2017-10-28 20:01:36'),
(147, 542, 49, '2017-10-28 20:56:36', '2017-10-28 20:56:36'),
(148, 581, 46, '2017-10-28 22:59:33', '2017-10-28 22:59:33'),
(149, 520, 46, '2017-10-28 23:03:25', '2017-10-28 23:03:25'),
(150, 513, 46, '2017-10-28 23:03:52', '2017-10-28 23:03:52'),
(151, 519, 37, '2017-10-29 00:23:43', '2017-10-29 00:23:43'),
(152, 116, 37, '2017-10-29 00:24:26', '2017-10-29 00:24:26'),
(153, 501, 37, '2017-10-29 00:24:58', '2017-10-29 00:24:58'),
(154, 444, 37, '2017-10-29 00:25:17', '2017-10-29 00:25:17'),
(155, 495, 37, '2017-10-29 00:28:05', '2017-10-29 00:28:05'),
(156, 565, 23, '2017-10-29 15:39:07', '2017-10-29 15:39:07'),
(157, 538, 33, '2017-10-29 15:59:56', '2017-10-29 15:59:56'),
(158, 513, 32, '2017-10-29 16:11:34', '2017-10-29 16:11:34'),
(159, 70, 32, '2017-10-29 16:14:53', '2017-10-29 16:14:53'),
(160, 450, 4, '2017-10-29 18:07:49', '2017-10-29 18:07:49'),
(161, 558, 32, '2017-10-29 19:56:18', '2017-10-29 19:56:18'),
(162, 482, 50, '2017-10-29 23:17:56', '2017-10-29 23:17:56'),
(163, 517, 50, '2017-10-29 23:23:35', '2017-10-29 23:23:35'),
(164, 493, 50, '2017-10-29 23:25:08', '2017-10-29 23:25:08'),
(165, 448, 50, '2017-10-29 23:52:55', '2017-10-29 23:52:55'),
(166, 511, 50, '2017-10-30 00:40:11', '2017-10-30 00:40:11'),
(167, 412, 35, '2017-10-30 16:12:48', '2017-10-30 16:12:48'),
(168, 414, 23, '2017-10-30 16:13:11', '2017-10-30 16:13:11'),
(169, 413, 37, '2017-10-30 16:13:34', '2017-10-30 16:13:34'),
(170, 413, 15, '2017-10-30 16:13:46', '2017-10-30 16:13:46'),
(171, 416, 1, '2017-10-30 16:13:57', '2017-10-30 16:13:57'),
(173, 561, 16, '2017-10-30 16:24:06', '2017-10-30 16:24:06'),
(176, 458, 17, '2017-10-30 16:27:51', '2017-10-30 16:27:51'),
(177, 367, 1, '2017-10-30 16:32:27', '2017-10-30 16:32:27'),
(178, 567, 1, '2017-10-30 16:33:03', '2017-10-30 16:33:03'),
(179, 537, 16, '2017-10-30 16:33:32', '2017-10-30 16:33:32'),
(180, 577, 23, '2017-10-30 16:33:44', '2017-10-30 16:33:44'),
(181, 532, 1, '2017-10-30 16:34:21', '2017-10-30 16:34:21'),
(182, 527, 1, '2017-10-30 16:34:24', '2017-10-30 16:34:24'),
(183, 522, 1, '2017-10-30 16:35:18', '2017-10-30 16:35:18'),
(184, 119, 1, '2017-10-30 16:36:03', '2017-10-30 16:36:03'),
(185, 498, 18, '2017-10-30 16:36:19', '2017-10-30 16:36:19'),
(187, 447, 1, '2017-10-30 16:37:45', '2017-10-30 16:37:45'),
(188, 521, 26, '2017-10-30 16:42:12', '2017-10-30 16:42:12'),
(189, 446, 26, '2017-10-30 16:42:34', '2017-10-30 16:42:34'),
(190, 118, 27, '2017-10-30 16:43:30', '2017-10-30 16:43:30'),
(192, 508, 21, '2017-10-30 16:47:09', '2017-10-30 16:47:09'),
(193, 365, 21, '2017-10-30 16:47:33', '2017-10-30 16:47:33'),
(194, 455, 49, '2017-10-30 16:48:23', '2017-10-30 16:48:23'),
(195, 117, 51, '2017-10-30 16:49:11', '2017-10-30 16:49:11'),
(196, 472, 51, '2017-10-30 16:49:23', '2017-10-30 16:49:23'),
(197, 576, 40, '2017-10-30 16:52:38', '2017-10-30 16:52:38'),
(198, 536, 40, '2017-10-30 16:54:09', '2017-10-30 16:54:09'),
(201, 445, 46, '2017-10-30 16:57:08', '2017-10-30 16:57:08'),
(202, 535, 49, '2017-10-30 16:58:41', '2017-10-30 16:58:41'),
(203, 496, 51, '2017-10-30 17:00:09', '2017-10-30 17:00:09'),
(205, 590, 25, '2017-10-30 17:03:22', '2017-10-30 17:03:22'),
(206, 499, 25, '2017-10-30 17:08:43', '2017-10-30 17:08:43'),
(207, 585, 20, '2017-10-30 17:09:45', '2017-10-30 17:09:45'),
(211, 533, 50, '2017-10-30 17:11:40', '2017-10-30 17:11:40'),
(212, 74, 50, '2017-10-30 17:11:48', '2017-10-30 17:11:48'),
(213, 583, 38, '2017-10-30 20:24:43', '2017-10-30 20:24:43'),
(214, 417, 38, '2017-10-30 20:24:55', '2017-10-30 20:24:55'),
(215, 368, 38, '2017-10-30 20:25:00', '2017-10-30 20:25:00'),
(216, 591, 38, '2017-10-31 21:12:31', '2017-10-31 21:12:31'),
(217, 481, 17, '2017-11-01 00:37:21', '2017-11-01 00:37:21'),
(218, 573, 18, '2017-11-01 00:38:31', '2017-11-01 00:38:31'),
(219, 73, 17, '2017-11-01 13:03:37', '2017-11-01 13:03:37'),
(220, 513, 33, '2017-11-02 13:10:28', '2017-11-02 13:10:28'),
(221, 444, 33, '2017-11-02 16:34:45', '2017-11-02 16:34:45');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users_tunes_instruments_2018`
--

CREATE TABLE `users_tunes_instruments_2018` (
  `id` bigint(20) NOT NULL,
  `tunes_instrument_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `users_tunes_instruments_2018`
--

INSERT INTO `users_tunes_instruments_2018` (`id`, `tunes_instrument_id`, `user_id`, `created`, `updated`) VALUES
(3, 694, 36, '2018-10-19 05:24:15', '2018-10-19 05:24:15'),
(4, 613, 36, '2018-10-19 05:24:31', '2018-10-19 05:24:31'),
(5, 647, 36, '2018-10-19 05:27:12', '2018-10-19 05:27:12'),
(7, 745, 20, '2018-10-19 09:20:55', '2018-10-19 09:20:55'),
(8, 625, 20, '2018-10-19 09:23:39', '2018-10-19 09:23:39'),
(9, 702, 20, '2018-10-19 09:24:46', '2018-10-19 09:24:46'),
(10, 744, 44, '2018-10-19 11:05:04', '2018-10-19 11:05:04'),
(11, 673, 38, '2018-10-19 11:17:58', '2018-10-19 11:17:58'),
(12, 709, 27, '2018-10-19 11:19:38', '2018-10-19 11:19:38'),
(13, 654, 27, '2018-10-19 11:20:50', '2018-10-19 11:20:50'),
(15, 630, 31, '2018-10-19 11:22:29', '2018-10-19 11:22:29'),
(16, 669, 31, '2018-10-19 11:22:48', '2018-10-19 11:22:48'),
(19, 746, 31, '2018-10-19 11:25:52', '2018-10-19 11:25:52'),
(21, 635, 31, '2018-10-19 11:27:00', '2018-10-19 11:27:00'),
(22, 776, 27, '2018-10-19 11:27:02', '2018-10-19 11:27:02'),
(23, 664, 31, '2018-10-19 11:27:14', '2018-10-19 11:27:14'),
(24, 703, 40, '2018-10-19 11:30:15', '2018-10-19 11:30:15'),
(25, 600, 40, '2018-10-19 11:30:45', '2018-10-19 11:30:45'),
(26, 741, 27, '2018-10-19 11:30:48', '2018-10-19 11:30:48'),
(27, 700, 35, '2018-10-19 11:34:58', '2018-10-19 11:34:58'),
(28, 766, 35, '2018-10-19 11:35:01', '2018-10-19 11:35:01'),
(29, 779, 37, '2018-10-19 11:43:24', '2018-10-19 11:43:24'),
(32, 767, 37, '2018-10-19 11:45:37', '2018-10-19 11:45:37'),
(33, 594, 24, '2018-10-19 11:45:45', '2018-10-19 11:45:45'),
(34, 630, 37, '2018-10-19 11:45:57', '2018-10-19 11:45:57'),
(36, 608, 24, '2018-10-19 11:46:46', '2018-10-19 11:46:46'),
(37, 793, 24, '2018-10-19 11:46:54', '2018-10-19 11:46:54'),
(38, 760, 37, '2018-10-19 11:46:58', '2018-10-19 11:46:58'),
(40, 665, 24, '2018-10-19 11:47:45', '2018-10-19 11:47:45'),
(44, 716, 15, '2018-10-19 12:28:22', '2018-10-19 12:28:22'),
(45, 711, 15, '2018-10-19 12:29:16', '2018-10-19 12:29:16'),
(46, 780, 15, '2018-10-19 12:29:56', '2018-10-19 12:29:56'),
(47, 630, 34, '2018-10-19 12:30:45', '2018-10-19 12:30:45'),
(49, 630, 15, '2018-10-19 12:32:03', '2018-10-19 12:32:03'),
(51, 607, 30, '2018-10-19 12:47:34', '2018-10-19 12:47:34'),
(54, 780, 30, '2018-10-19 12:48:50', '2018-10-19 12:48:50'),
(55, 624, 30, '2018-10-19 12:49:35', '2018-10-19 12:49:35'),
(56, 767, 34, '2018-10-19 12:53:11', '2018-10-19 12:53:11'),
(58, 663, 34, '2018-10-19 12:55:12', '2018-10-19 12:55:12'),
(59, 618, 34, '2018-10-19 12:56:01', '2018-10-19 12:56:01'),
(61, 704, 19, '2018-10-19 12:57:20', '2018-10-19 12:57:20'),
(63, 674, 19, '2018-10-19 12:57:39', '2018-10-19 12:57:39'),
(64, 701, 23, '2018-10-19 12:57:55', '2018-10-19 12:57:55'),
(66, 762, 23, '2018-10-19 13:24:21', '2018-10-19 13:24:21'),
(68, 780, 23, '2018-10-19 13:24:54', '2018-10-19 13:24:54'),
(69, 670, 38, '2018-10-19 13:25:02', '2018-10-19 13:25:02'),
(70, 597, 23, '2018-10-19 13:25:07', '2018-10-19 13:25:07'),
(71, 648, 23, '2018-10-19 13:25:45', '2018-10-19 13:25:45'),
(76, 718, 23, '2018-10-19 13:27:34', '2018-10-19 13:27:34'),
(78, 653, 58, '2018-10-19 13:42:43', '2018-10-19 13:42:43'),
(79, 698, 18, '2018-10-19 13:43:11', '2018-10-19 13:43:11'),
(80, 794, 58, '2018-10-19 13:44:46', '2018-10-19 13:44:46'),
(81, 630, 36, '2018-10-19 13:46:29', '2018-10-19 13:46:29'),
(82, 661, 18, '2018-10-19 13:46:32', '2018-10-19 13:46:32'),
(83, 624, 37, '2018-10-19 14:00:17', '2018-10-19 14:00:17'),
(86, 721, 33, '2018-10-19 14:08:40', '2018-10-19 14:08:40'),
(88, 695, 33, '2018-10-19 14:08:54', '2018-10-19 14:08:54'),
(89, 630, 33, '2018-10-19 14:08:58', '2018-10-19 14:08:58'),
(90, 675, 33, '2018-10-19 14:09:49', '2018-10-19 14:09:49'),
(91, 664, 33, '2018-10-19 14:11:32', '2018-10-19 14:11:32'),
(93, 592, 33, '2018-10-19 14:12:33', '2018-10-19 14:12:33'),
(94, 767, 33, '2018-10-19 14:12:56', '2018-10-19 14:12:56'),
(95, 595, 28, '2018-10-19 14:16:20', '2018-10-19 14:16:20'),
(97, 752, 28, '2018-10-19 14:16:29', '2018-10-19 14:16:29'),
(99, 770, 28, '2018-10-19 14:18:51', '2018-10-19 14:18:51'),
(100, 627, 17, '2018-10-19 14:23:28', '2018-10-19 14:23:28'),
(101, 771, 17, '2018-10-19 14:26:21', '2018-10-19 14:26:21'),
(102, 681, 58, '2018-10-19 14:35:56', '2018-10-19 14:35:56'),
(103, 717, 21, '2018-10-19 14:48:03', '2018-10-19 14:48:03'),
(104, 619, 21, '2018-10-19 14:48:23', '2018-10-19 14:48:23'),
(105, 768, 21, '2018-10-19 14:48:30', '2018-10-19 14:48:30'),
(106, 614, 21, '2018-10-19 14:48:40', '2018-10-19 14:48:40'),
(108, 710, 55, '2018-10-19 15:45:50', '2018-10-19 15:45:50'),
(109, 719, 58, '2018-10-19 15:56:13', '2018-10-19 15:56:13'),
(110, 679, 55, '2018-10-19 16:01:02', '2018-10-19 16:01:02'),
(112, 601, 55, '2018-10-19 16:08:18', '2018-10-19 16:08:18'),
(113, 626, 58, '2018-10-19 16:10:30', '2018-10-19 16:10:30'),
(114, 598, 30, '2018-10-19 16:11:24', '2018-10-19 16:11:24'),
(115, 725, 55, '2018-10-19 16:13:23', '2018-10-19 16:13:23'),
(117, 636, 37, '2018-10-19 21:07:49', '2018-10-19 21:07:49'),
(119, 748, 17, '2018-10-19 22:40:58', '2018-10-19 22:40:58'),
(120, 670, 33, '2018-10-19 23:37:46', '2018-10-19 23:37:46'),
(121, 708, 25, '2018-10-20 09:04:42', '2018-10-20 09:04:42'),
(122, 707, 49, '2018-10-20 09:36:34', '2018-10-20 09:36:34'),
(123, 658, 49, '2018-10-20 09:38:09', '2018-10-20 09:38:09'),
(124, 650, 40, '2018-10-20 11:38:12', '2018-10-20 11:38:12'),
(126, 671, 51, '2018-10-20 16:16:39', '2018-10-20 16:16:39'),
(127, 775, 51, '2018-10-20 16:30:13', '2018-10-20 16:30:13'),
(128, 678, 58, '2018-10-20 16:53:44', '2018-10-20 16:53:44'),
(132, 705, 32, '2018-10-22 09:51:03', '2018-10-22 09:51:03'),
(133, 630, 32, '2018-10-22 09:52:48', '2018-10-22 09:52:48'),
(134, 767, 32, '2018-10-22 09:53:01', '2018-10-22 09:53:01'),
(135, 780, 32, '2018-10-22 09:53:31', '2018-10-22 09:53:31'),
(136, 728, 32, '2018-10-22 09:54:11', '2018-10-22 09:54:11'),
(137, 761, 32, '2018-10-22 09:54:22', '2018-10-22 09:54:22'),
(138, 598, 32, '2018-10-22 09:54:33', '2018-10-22 09:54:33'),
(139, 623, 32, '2018-10-22 09:54:59', '2018-10-22 09:54:59'),
(140, 598, 15, '2018-10-22 10:45:11', '2018-10-22 10:45:11'),
(142, 730, 53, '2018-10-22 20:26:51', '2018-10-22 20:26:51'),
(143, 660, 53, '2018-10-22 20:28:58', '2018-10-22 20:28:58'),
(145, 637, 49, '2018-10-23 09:26:46', '2018-10-23 09:26:46'),
(147, 706, 37, '2018-10-23 12:25:28', '2018-10-23 12:25:28'),
(148, 676, 37, '2018-10-23 12:28:30', '2018-10-23 12:28:30'),
(149, 628, 25, '2018-10-23 13:31:35', '2018-10-23 13:31:35'),
(150, 765, 25, '2018-10-23 13:34:30', '2018-10-23 13:34:30'),
(151, 781, 25, '2018-10-23 13:40:00', '2018-10-23 13:40:00'),
(152, 747, 4, '2018-10-23 15:14:59', '2018-10-23 15:14:59'),
(153, 593, 4, '2018-10-23 15:16:12', '2018-10-23 15:16:12'),
(154, 630, 4, '2018-10-23 15:17:00', '2018-10-23 15:17:00'),
(155, 749, 4, '2018-10-23 15:17:16', '2018-10-23 15:17:16'),
(156, 738, 4, '2018-10-23 15:17:20', '2018-10-23 15:17:20'),
(159, 742, 1, '2018-10-23 15:19:22', '2018-10-23 15:19:22'),
(162, 731, 1, '2018-10-23 15:20:21', '2018-10-23 15:20:21'),
(163, 624, 4, '2018-10-23 15:20:27', '2018-10-23 15:20:27'),
(165, 657, 34, '2018-10-23 15:27:12', '2018-10-23 15:27:12'),
(166, 722, 30, '2018-10-23 16:31:00', '2018-10-23 16:31:00'),
(167, 713, 49, '2018-10-23 17:30:34', '2018-10-23 17:30:34'),
(168, 638, 4, '2018-10-23 18:36:06', '2018-10-23 18:36:06'),
(169, 666, 4, '2018-10-23 18:52:13', '2018-10-23 18:52:13'),
(171, 799, 33, '2018-10-23 20:26:01', '2018-10-23 20:26:01'),
(172, 640, 25, '2018-10-23 23:47:07', '2018-10-23 23:47:07'),
(173, 712, 24, '2018-10-24 14:21:49', '2018-10-24 14:21:49'),
(174, 631, 24, '2018-10-24 14:23:38', '2018-10-24 14:23:38'),
(176, 769, 24, '2018-10-24 14:24:05', '2018-10-24 14:24:05'),
(177, 763, 26, '2018-10-24 14:27:34', '2018-10-24 14:27:34'),
(178, 620, 26, '2018-10-24 14:28:04', '2018-10-24 14:28:04'),
(179, 724, 26, '2018-10-24 14:29:35', '2018-10-24 14:29:35'),
(180, 697, 46, '2018-10-24 15:01:45', '2018-10-24 15:01:45'),
(182, 714, 46, '2018-10-24 15:08:36', '2018-10-24 15:08:36'),
(183, 667, 19, '2018-10-24 20:15:09', '2018-10-24 20:15:09'),
(184, 651, 56, '2018-10-24 20:17:52', '2018-10-24 20:17:52'),
(185, 602, 25, '2018-10-24 20:23:01', '2018-10-24 20:23:01'),
(186, 599, 21, '2018-10-24 20:24:37', '2018-10-24 20:24:37'),
(187, 764, 1, '2018-10-24 20:24:59', '2018-10-24 20:24:59'),
(188, 727, 34, '2018-10-24 20:26:42', '2018-10-24 20:26:42'),
(189, 732, 25, '2018-10-24 20:27:06', '2018-10-24 20:27:06'),
(190, 778, 34, '2018-10-24 20:28:20', '2018-10-24 20:28:20'),
(192, 720, 62, '2018-10-24 20:29:56', '2018-10-24 20:29:56'),
(193, 774, 15, '2018-10-24 20:34:39', '2018-10-24 20:34:39'),
(194, 774, 34, '2018-10-24 20:34:48', '2018-10-24 20:34:48'),
(195, 777, 1, '2018-10-24 20:35:02', '2018-10-24 20:35:02'),
(196, 639, 19, '2018-10-24 20:37:45', '2018-10-24 20:37:45'),
(198, 726, 25, '2018-10-24 20:43:06', '2018-10-24 20:43:06'),
(199, 596, 18, '2018-10-24 20:45:05', '2018-10-24 20:45:05'),
(200, 621, 1, '2018-10-24 20:47:10', '2018-10-24 20:47:10'),
(201, 622, 25, '2018-10-24 20:48:50', '2018-10-24 20:48:50'),
(202, 696, 21, '2018-10-24 20:51:01', '2018-10-24 20:51:01'),
(203, 633, 1, '2018-10-24 20:51:47', '2018-10-24 20:51:47'),
(204, 632, 4, '2018-10-24 20:51:59', '2018-10-24 20:51:59'),
(205, 634, 34, '2018-10-24 20:52:50', '2018-10-24 20:52:50'),
(206, 753, 18, '2018-10-24 20:54:00', '2018-10-24 20:54:00'),
(208, 754, 38, '2018-10-24 20:57:59', '2018-10-24 20:57:59'),
(209, 740, 38, '2018-10-24 21:02:17', '2018-10-24 21:02:17'),
(210, 796, 25, '2018-10-24 21:04:20', '2018-10-24 21:04:20'),
(211, 791, 4, '2018-10-24 21:05:32', '2018-10-24 21:05:32'),
(212, 773, 44, '2018-10-24 21:07:22', '2018-10-24 21:07:22'),
(213, 722, 32, '2018-10-24 21:08:07', '2018-10-24 21:08:07'),
(214, 774, 32, '2018-10-24 21:08:28', '2018-10-24 21:08:28'),
(215, 686, 17, '2018-10-24 21:12:14', '2018-10-24 21:12:14'),
(216, 685, 46, '2018-10-24 21:13:22', '2018-10-24 21:13:22'),
(217, 655, 1, '2018-10-24 21:24:40', '2018-10-24 21:24:40'),
(218, 783, 18, '2018-10-24 21:29:04', '2018-10-24 21:29:04'),
(219, 616, 17, '2018-10-24 21:30:11', '2018-10-24 21:30:11'),
(220, 610, 1, '2018-10-24 21:30:43', '2018-10-24 21:30:43'),
(221, 715, 19, '2018-10-24 21:34:06', '2018-10-24 21:34:06'),
(222, 609, 28, '2018-10-24 21:39:23', '2018-10-24 21:39:23'),
(224, 782, 53, '2018-10-24 21:48:20', '2018-10-24 21:48:20'),
(225, 659, 57, '2018-10-24 21:50:55', '2018-10-24 21:50:55'),
(226, 649, 57, '2018-10-24 21:51:32', '2018-10-24 21:51:32'),
(229, 611, 38, '2018-10-30 17:59:49', '2018-10-30 17:59:49'),
(231, 687, 38, '2018-10-30 17:59:52', '2018-10-30 17:59:52'),
(232, 772, 38, '2018-10-30 17:59:54', '2018-10-30 17:59:54'),
(233, 680, 38, '2018-10-30 17:59:57', '2018-10-30 17:59:57'),
(234, 617, 34, '2018-10-31 13:36:27', '2018-10-31 13:36:27'),
(235, 615, 28, '2018-10-31 13:36:36', '2018-10-31 13:36:36'),
(236, 795, 56, '2018-10-31 13:37:36', '2018-10-31 13:37:36'),
(237, 677, 51, '2018-10-31 14:11:09', '2018-10-31 14:11:09'),
(238, 751, 51, '2018-10-31 14:11:12', '2018-10-31 14:11:12'),
(239, 672, 57, '2018-11-01 15:45:44', '2018-11-01 15:45:44'),
(240, 630, 30, '2018-11-01 18:18:43', '2018-11-01 18:18:43'),
(241, 630, 35, '2018-11-01 18:18:54', '2018-11-01 18:18:54'),
(242, 630, 23, '2018-11-01 18:19:06', '2018-11-01 18:19:06'),
(243, 630, 44, '2018-11-01 18:19:16', '2018-11-01 18:19:16'),
(244, 630, 58, '2018-11-01 18:20:17', '2018-11-01 18:20:17'),
(245, 723, 23, '2018-11-01 21:50:32', '2018-11-01 21:50:32'),
(246, 683, 23, '2018-11-01 21:50:47', '2018-11-01 21:50:47');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `available_schedules`
--
ALTER TABLE `available_schedules`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `cake_d_c_forum_phinxlog`
--
ALTER TABLE `cake_d_c_forum_phinxlog`
  ADD PRIMARY KEY (`version`);

--
-- Indices de la tabla `cake_d_c_users_phinxlog`
--
ALTER TABLE `cake_d_c_users_phinxlog`
  ADD PRIMARY KEY (`version`);

--
-- Indices de la tabla `config`
--
ALTER TABLE `config`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `editions`
--
ALTER TABLE `editions`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `editions_tunes`
--
ALTER TABLE `editions_tunes`
  ADD PRIMARY KEY (`tune_id`,`edition_id`);

--
-- Indices de la tabla `editions_users`
--
ALTER TABLE `editions_users`
  ADD PRIMARY KEY (`user_id`,`edition_id`);

--
-- Indices de la tabla `forum_categories`
--
ALTER TABLE `forum_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent_id` (`parent_id`),
  ADD KEY `lft` (`lft`),
  ADD KEY `rght` (`rght`),
  ADD KEY `slug` (`slug`),
  ADD KEY `is_visible` (`is_visible`),
  ADD KEY `title` (`title`),
  ADD KEY `last_post_id` (`last_post_id`);

--
-- Indices de la tabla `forum_likes`
--
ALTER TABLE `forum_likes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indices de la tabla `forum_moderators`
--
ALTER TABLE `forum_moderators`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indices de la tabla `forum_posts`
--
ALTER TABLE `forum_posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `parent_id` (`parent_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `slug` (`slug`),
  ADD KEY `is_sticky` (`is_sticky`),
  ADD KEY `title` (`title`),
  ADD KEY `last_reply_created` (`last_reply_created`),
  ADD KEY `last_reply_id` (`last_reply_id`);

--
-- Indices de la tabla `forum_reports`
--
ALTER TABLE `forum_reports`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indices de la tabla `instruments`
--
ALTER TABLE `instruments`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `instruments_tunes`
--
ALTER TABLE `instruments_tunes`
  ADD PRIMARY KEY (`tune_id`,`instrument_id`);

--
-- Indices de la tabla `instruments_users`
--
ALTER TABLE `instruments_users`
  ADD PRIMARY KEY (`user_id`,`instrument_id`);

--
-- Indices de la tabla `schedules`
--
ALTER TABLE `schedules`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `schedules_exceptions`
--
ALTER TABLE `schedules_exceptions`
  ADD PRIMARY KEY (`tune_id`,`user_id`,`edition_id`);

--
-- Indices de la tabla `social_accounts`
--
ALTER TABLE `social_accounts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indices de la tabla `tunes`
--
ALTER TABLE `tunes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users_old`
--
ALTER TABLE `users_old`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users_selected_tunes`
--
ALTER TABLE `users_selected_tunes`
  ADD PRIMARY KEY (`user_id`,`tune_id`,`instrument_id`);

--
-- Indices de la tabla `users_tunes_instruments_2016`
--
ALTER TABLE `users_tunes_instruments_2016`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `users_tunes_instruments_2017`
--
ALTER TABLE `users_tunes_instruments_2017`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `users_tunes_instruments_2018`
--
ALTER TABLE `users_tunes_instruments_2018`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `available_schedules`
--
ALTER TABLE `available_schedules`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `config`
--
ALTER TABLE `config`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `editions`
--
ALTER TABLE `editions`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `forum_categories`
--
ALTER TABLE `forum_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT de la tabla `forum_likes`
--
ALTER TABLE `forum_likes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `forum_moderators`
--
ALTER TABLE `forum_moderators`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `forum_posts`
--
ALTER TABLE `forum_posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `forum_reports`
--
ALTER TABLE `forum_reports`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `instruments`
--
ALTER TABLE `instruments`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `schedules`
--
ALTER TABLE `schedules`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=157;

--
-- AUTO_INCREMENT de la tabla `tunes`
--
ALTER TABLE `tunes`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=231;

--
-- AUTO_INCREMENT de la tabla `users_tunes_instruments_2016`
--
ALTER TABLE `users_tunes_instruments_2016`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=240;

--
-- AUTO_INCREMENT de la tabla `users_tunes_instruments_2017`
--
ALTER TABLE `users_tunes_instruments_2017`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=222;

--
-- AUTO_INCREMENT de la tabla `users_tunes_instruments_2018`
--
ALTER TABLE `users_tunes_instruments_2018`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=247;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `forum_categories`
--
ALTER TABLE `forum_categories`
  ADD CONSTRAINT `forum_categories_ibfk_1` FOREIGN KEY (`last_post_id`) REFERENCES `forum_posts` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Filtros para la tabla `forum_likes`
--
ALTER TABLE `forum_likes`
  ADD CONSTRAINT `forum_likes_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `forum_posts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `forum_moderators`
--
ALTER TABLE `forum_moderators`
  ADD CONSTRAINT `forum_moderators_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `forum_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `forum_posts`
--
ALTER TABLE `forum_posts`
  ADD CONSTRAINT `forum_posts_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `forum_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `forum_posts_ibfk_2` FOREIGN KEY (`parent_id`) REFERENCES `forum_posts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `forum_posts_ibfk_3` FOREIGN KEY (`last_reply_id`) REFERENCES `forum_posts` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Filtros para la tabla `forum_reports`
--
ALTER TABLE `forum_reports`
  ADD CONSTRAINT `forum_reports_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `forum_posts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `social_accounts`
--
ALTER TABLE `social_accounts`
  ADD CONSTRAINT `social_accounts_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users_old` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
