<?php

return [
    'inputContainer' => '<div class="form-group {{type}}{{required}}">{{content}}<div class="help-block"></div></div>',
    'input' => '<input class="form-control" type="{{type}}" name="{{name}}"{{attrs}}/>',
    'textarea' => '<textarea class="form-control" name="{{name}}"{{attrs}}>{{value}}</textarea>',
    'select' => '<select class="form-control" name="{{name}}"{{attrs}}>{{content}}</select>',
];