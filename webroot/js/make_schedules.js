$(() => {
  // data = [
  //   [{userId: 100, name: 'Max'},{userId: 200, name: 'Rebe'}, {userId: 300, name: 'Juansy'}, {userId: 400, name: 'Rubén'} ],
  //   [{tuneId: 1, name: 'Canción 1', artist: 'qwe1', schedules: {1: {start: null, end: null}, 2: {start: null, end: null}, 3: {start: null, end: null}, 4: {start: null, end: null}}}, true, true, false, false],
  //   [{tuneId: 2, name: 'Canción 2', artist: 'qwe2', schedules: {1: {start: null, end: null}, 2: {start: null, end: null}, 3: {start: null, end: null}, 4: {start: null, end: null}}}, true, true, true, false],
  //   [{tuneId: 3, name: 'Canción 3', artist: 'qwe3', schedules: {1: {start: null, end: null}, 2: {start: null, end: null}, 3: {start: null, end: null}, 4: {start: null, end: null}}}, false, true, true, false],
  //   [{tuneId: 4, name: 'Canción 4', artist: 'qwe4', schedules: {1: {start: null, end: null}, 2: {start: null, end: null}, 3: {start: null, end: null}, 4: {start: null, end: null}}}, true, true, false, true],
  //   [{tuneId: 5, name: 'Canción 5', artist: 'qwe5', schedules: {1: {start: null, end: null}, 2: {start: null, end: null}, 3: {start: null, end: null}, 4: {start: null, end: null}}}, true, false, false, true]
  // ];
  // console.log(data)

  let innerData = data.slice();

  // Table drag-n-drop
  const makeDragnDrop = () => {
    $('.table').dragableColumns({
      drag: true,
      dragClass: 'drag',
      overClass: 'over',
      movedContainerSelector: '.dnd-moved',
      onDragEnd: refreshInnerData
    });

    $('table tbody').sortable({
      handle: '.handle-sortable',
      stop: refreshInnerData
    });

    $('.locked').on('mousedown', (e) => {
      e.stopPropagation();
      e.preventDefault();
    });
  }

  //refreshInnerData
  const refreshInnerData = () => {
    const headers = $('.table thead th');
    const BLANK_HEADERS = 3;

    innerData = [];
    innerData[0] = [];

    for(let cell of headers.slice(BLANK_HEADERS, headers.length)) {
      innerData[0].push({userId: $(cell).data('user-id'), name: $(cell).text()});
    }

    const rows = $('.table tbody tr');

    for (let i = 0; i < rows.length; i++) {
      innerData[i + 1] = [
        {
          tuneId: $(rows[i]).data('tune-id'), 
          name: $(rows[i]).data('tune-name'), 
          artist: $(rows[i]).data('tune-artist'), 
          schedules: $(rows[i]).data('tune-schedules')
        }
      ];

      const cells = $(rows[i]).find('td');
      
      for (let j = BLANK_HEADERS, t = 1; j < cells.length; j++, t++) {
        let val = false;
        if($(cells[j]).hasClass('matches')) {
          val = true;
        }else if($(cells[j]).hasClass('excluded')) {
          val = -1;
        }

        innerData[i + 1][t] = val;
      }
    }
  }

  const saveInnerData = () => {
    const users = innerData[0].map(data => data.userId);
    const tunes = innerData.slice(1, innerData.length).map(row => row[0].tuneId);

    $.ajax({
      method: "POST",
      url: '/schedules/saveMakedSchedule',
      headers: {
        'X-CSRF-Token': CSRF_TOKEN
      },
      data: {users, tunes},
      dataType: 'json',
      success: function (response) {
        if(!response['success']) {
          toastr.error('Houbo un erro o gardar os datos, proba de novo.');
        }

        location.reload();
      },
      error: function (xhr) {
        toastr.error('Houbo un erro o gardar os datos, proba de novo.');
        console.log(xhr);
      }
    });
  }

  $('#save-data').on('click', (e) => {
    e.preventDefault();
    e.stopPropagation();

    saveInnerData();
  });

  const reOrder = sorter => {
    let orderedByColumn = [[]];
    for (const userId of sorter[0]) {
      const index = innerData[0].findIndex(user => user.userId == userId);

      orderedByColumn[0].push(innerData[0][index]);

      for (let i=1; i<innerData.length; i++) {
        const row = innerData[i];

        if(orderedByColumn[i] == undefined) {
          orderedByColumn[i] = [innerData[i][0]];
        }
        
        orderedByColumn[i].push(row[index +1]);
      }
    }

    let orderedByRow = [orderedByColumn[0]];
    for(const tuneId of sorter[1]) {
      orderedByRow.push(orderedByColumn.find(row => row[0] && row[0].tuneId == tuneId));
    }

    innerData = orderedByRow.slice();
    createTable();
  }

  const createTable = () => {
    createHeaders(innerData[0]);
    createRows(innerData.slice(1, innerData.length));

    makeDragnDrop();
  }

  const createHeaders = (headers) => {
    let columns = `<th class="locked" draggable="false">Día 2</th>
                   <th class="locked" draggable="false">Día 3</th>
                   <th class="locked" draggable="false"></th>`;

    for (let user of headers) {
      columns += `<th data-user-id="${user.userId}" draggable="true">${user.name}</th>`;
    }

    $('.table thead tr').empty().append($(columns));
  }

  const createRows = (tunes) => {
    let rows = '';
    const HEADER_CELLS = 1;

    for(tune of tunes) {
      rows += `<tr class="dnd-moved" data-tune-id="${tune[0].tuneId}" data-tune-name="${tune[0].name}" data-tune-artist="${tune[0].artist}" data-tune-schedules='${JSON.stringify(tune[0].schedules)}'>`;
      rows += `<td class="handle-sortable tune-schedule"><p>${_getTime(tune[0].schedules[1].start)} - ${_getTime(tune[0].schedules[1].end)}</p> <p>${_getTime(tune[0].schedules[2].start)} - ${_getTime(tune[0].schedules[2].end)}</p></td>`;
      rows += `<td class="handle-sortable tune-schedule"><p>${_getTime(tune[0].schedules[3].start)} - ${_getTime(tune[0].schedules[3].end)}</p> <p>${_getTime(tune[0].schedules[4].start)} - ${_getTime(tune[0].schedules[4].end)}</p></td>`;
      rows += `<td class="handle-sortable tune-name">${tune[0].name} <span>(${tune[0].artist})</span></td>`;

      for (let match of tune.slice(HEADER_CELLS, tune.length)) {
        switch(match) {
          case true:
            match = 'class="matches"';
            break;
          case -1:
            match = 'class="excluded"';
            break;
          default:
            match = false;
            break;
        }

        rows += `<td ${match}></td>`;
      }
      
      rows += '</tr>';
    }

    $('.table tbody').empty().append($(rows));
  }

  const _getTime = (date) => {
    if(date == null) {
      return null;
    }

    return moment(date).format('HH:mm');
  }

  // Initialize
  if(sorter && sorter.users.length !== 0) {
    reOrder([JSON.parse(sorter.users), JSON.parse(sorter.tunes)]);
  }else {
    createTable();  
  }
});