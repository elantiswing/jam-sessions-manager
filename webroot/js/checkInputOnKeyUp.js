//sendData(field, value) is the function with the ajax call (put it in view)
$(function(){
    $(".inputToCheck").bind('keyup', function(e) {
        var keyCode = e.keyCode || e.which; 

        if (keyCode === 9 || keyCode === 13) {
            e.preventDefault();
        }else {
            var that = $(this); 
            delay(function(){ getData(that)}, 500);
        }
    });

    var delay = (function(){
        var timer = 0;
        return function(callback, ms){
            clearTimeout (timer);
            timer = setTimeout(callback, ms);
           };
    })();

    var getData = function(el){
        if(el.hasClass('notSend') || el.attr('id') == 'image_file')
            return false;

        var field = el.attr('name').split('[')[2].split(']')[0];
        var value = el.val();

        if($('#'+el.attr('id')).valid()){
            sendData(field, value);
        }
    }
});