$(() => {
  //Listeners
  $('.selector .custom-checkbox input').on('change', (e) => {
    const $el = $(e.target);
    const $tuneEl = $el.closest('.tune');
    const tuneId = $tuneEl.data('tune-id');
    const $instrumentEl = $el.closest('.instrument');
    const instrumentId = $instrumentEl.data('instrument-id');
    const isChecked = $instrumentEl.find('.custom-checkbox input').prop('checked');
    const userId = $('.row[data-user-id]').data('user-id');
    const username = $('.row[data-user-username]').data('user-username');

    saveSelection(tuneId, instrumentId, isChecked, userId, username);
  });

  $('#users-selector select').on('change', (e) => {
    const userId = $(e.target).val();

    window.location.href = window.location.origin + '/tunes/select/' + userId;
  });

  const saveSelection = (tuneId, instrumentId, state, userId, username) => {
    $.ajax({
      method: "POST",
      url: '/tunes/saveSelection',
      headers: {
        'X-CSRF-Token': CSRF_TOKEN
      },
      data: {
        tune_id: tuneId,
        instrument_id: instrumentId,
        state,
        user_id: userId
      },
      dataType: 'json',
      success: function (response) {
        if(!response.success) {
          toastr.error('Houbo un erro o gardar a selección, proba de novo.');
          return false;
        }

        const $instrumentEl = $('.tune[data-tune-id="' + tuneId + '"] .instrument[data-instrument-id="' + instrumentId + '"]');
        let musicians = $instrumentEl.find('.musicians').text();

        if(response.status === 'exists') {
          toastr.error('Xa hai un músico inscrito nese instrumento');
          $instrumentEl.find('.custom-checkbox').remove();
          $instrumentEl.find('.musicians').text(response.username);

          return false;
        }else {
          toastr.success('Selección gardada correctamente.');
        }
        
        if(response.status === 'saved') {
          if(musicians.length == 0) {
            musicians = username;
          }else {
            musicians = musicians + ', ' + username;
          }
        }else if(response.status === 'removed'){
          musiciansArray = musicians.split(', ');
          if(musiciansArray.indexOf(username) !== -1) {
            musiciansArray.splice(musiciansArray.indexOf(username));
          }

          musicians = musiciansArray.join(', ');
        }

        $instrumentEl.find('.musicians').text(musicians);

        refreshUserSelections(userId);
      },
      error: function (xhr) {
        toastr.error('Houbo un erro o gardar a selección, proba de novo.');
        console.log(xhr);
      }
    });
  }

  const refreshUserSelections = (userId) => {
    const isOpen = $('#user-selections').hasClass('open');

    $.ajax({
      method: "GET",
      url: '/my-users/getUserSelections/' + userId,
      headers: {
        'X-CSRF-Token': CSRF_TOKEN
      },
      success: function (response) {
        const $html = $(response);

        if(isOpen) {
          $html.addClass('open');
        }

        $('#user-selections').replaceWith($html);
      },
      error: function (xhr) {
        toastr.error('Houbo un erro o gardar a selección, proba de novo.');
        console.log(xhr);
      }
    });
  }
});