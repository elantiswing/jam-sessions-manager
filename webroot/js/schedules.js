$(() => {
  // Event listeners
  $('.schedule select').on('change', (e) => {
    const $selector = $(e.target);
    
    setOppositeValues($selector)
  });

  $('.schedule .schedule-unavailable input').on('change', (e) => {
    const $selector = $(e.target);
    const isChecked = $selector.prop('checked');
    const inputs = $selector.closest('.schedule').find('select');
    let disabled;

    if(isChecked) {
      disabled = true;
    }else {
      disabled = false;
    }
    
    inputs.prop('disabled', disabled);
  })

  const checkIfNeedToReSelect = (valueSelected, oppositeValueSelected, type) => {
    if(type === 'start') {
      return moment(oppositeValueSelected).isSameOrBefore(valueSelected);
    }else {
      return moment(oppositeValueSelected).isSameOrAfter(valueSelected);
    }
  }

  const getOppositeSelector = ($selector, type) => {
    let oppositeType;

    if(type === 'start') {
      oppositeType = 'end';
    }else {
      oppositeType = 'start';
    }

    return $selector.closest('.schedule').find('[data-type="' + oppositeType + '"]');
  }

  const setOppositeValues = ($selector) => {
    const valueSelected = $selector.val();
    const type = $selector.data('type');
    const $opposite = getOppositeSelector($selector, type);
    const needToReSelect = checkIfNeedToReSelect(valueSelected, $opposite.val(), type);

    const options = $opposite.find('option');
    for (let i = 0; i < options.length; i++) {
      const $option = $(options[i]);
      let disabled = false;

      if(type === 'start') {
        disabled = moment($option.val()).isSameOrBefore(valueSelected);
      }else {
        disabled = moment($option.val()).isSameOrAfter(valueSelected)
      }

      if(needToReSelect) {
        $option.attr('selected', false);
        if(moment($option.val()).isSame(valueSelected)) {
          $(options[i +1]).attr('selected', true);
        }
      }

      $option.attr('disabled', disabled);
    }
  }

  // Execute on init
  $('.schedule select').each(function(k,v){
    const $selector = $(v);
    
    setOppositeValues($selector)
  });
});