$(() => {
  $('.selectpicker').on('hide.bs.select', (e) => {
    const users = $(e.target).selectpicker('val');
    const tune_id = $(e.target).data('tune-id');

    $.ajax({
      method: "POST",
      url: '/schedules-exceptions/saveExceptions',
      headers: {
        'X-CSRF-Token': CSRF_TOKEN
      },
      data: {tune_id, users},
      dataType: 'json',
      success: function (response) { console.log(response)
        if(response['success']) {
          location.reload();
        }
      },
      error: function (xhr) {
        toastr.error('Houbo un erro o gardar a selección, proba de novo.');
        console.log(xhr);
      }
    });
  });
});