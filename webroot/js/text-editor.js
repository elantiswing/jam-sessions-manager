$(() => {
  if($('.forum-message-form textarea').length) {
    CKEDITOR.replace($('.forum-message-form textarea')[0], {
      extraAllowedContent: 'blockquote'
    });

    $('.forum-posts a[href="#reply"]').on('click', (e) => {
      const $el = $(e.target);
      let message = $el.closest('.panel').find('.panel-body').data('content');

      CKEDITOR.instances.message.setData(`<blockquote>${message}</blockquote><p></p>`);
    });
  }
});
