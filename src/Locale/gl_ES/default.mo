��    l      |  �   �      0	     1	     9	     F	  	   T	  
   ^	     i	  &   z	  &   �	  
   �	     �	     �	     �	     �	     �	     
     
     &
  '   2
  '   Z
     �
     �
  
   �
     �
     �
     �
     �
  	   �
  	   �
  
   �
     �
     �
     �
     �
               *     7     >  	   F  
   P     [     g     p     u  	   �     �     �     �  	   �  
   �     �     �     �     �     �                    $     9     B     J  
   W     b     i  5   p  3   �  3   �          -     J  /   g     �  6   �  4   �          :  2   Z  0   �     �     �  3   �  1   (     Z     w  3   �  1   �  1   �     *     G     b     }     �     �     �     �     �  
   �     �     �  
   �     �     �     �     �     �     �  5       H     Q     d     v     �     �  !   �  !   �     �     �  
             !     *     >     P     ^  '   k  $   �     �     �     �     �     �     �     �             
        )     9     F     O     d     y     �  	   �     �  	   �     �     �  
   �     �     �     �     �               /  	   =     G     K     g     |     �  	   �  	   �     �     �  	   �     �     �     �            >     <   Z  <   �  &   �     �  $     8   9      r  =   �  ;   �          '  <   B  :     $   �  "   �  ;     9   >     x  !   �  8   �  5   �  6   "     Y     o     �     �     �     �     �     �     �     �     �     �     �     �     �  	   �                    ;   "   @       f                           '   /           &   4              [      F       j   b   l   
      H   \   P   9   C                    O           J   c       )   <   -      	       ?          T       i       ^   K   g       G   +   A           D             `   :   d      5       $               0   Q               B             #   U   8      W       Z          I   1           =       L   .       V   ]      S   R          ,          !   _   E   M   N   a   7              X   3   e      k      (      h          2       >   *   6      Y   %    Actions Add Category Add Moderator Add Reply Add Thread Add sub category Are you sure you want to delete "{0}"? Are you sure you want to delete # {0}? Categories Categories List Category Created Delete Delete Category Delete Reply Delete Thread Description Discussion in "{0}" started by {1}, {2} Do you really want to delete this post? Edit Edit Category Edit Reply Edit Thread Filter Forum Hidden Is Locked Is Sticky Is Visible Last Message Latest: {0} Like List Categories List Moderators List Replies List Threads Locked Message Moderator Moderators Moderators: Modified Move Move Thread: {0} Move down Move up My Conversations New Category New Reply New Thread No No categories found No moderators. No replies. No reports found. Replies Reply Report Report post in "{0}" Reported Reports Reports List Start Date Sticky Submit The category could not be deleted. Please, try again. The category could not be moved. Please, try again. The category could not be saved. Please, try again. The category has been deleted. The category has been moved. The category has been saved. The like could not be saved. Please, try again. The like has been saved. The moderator could not be deleted. Please, try again. The moderator could not be saved. Please, try again. The moderator has been added. The moderator has been deleted. The reply could not be deleted. Please, try again. The reply could not be saved. Please, try again. The reply has been deleted. The reply has been saved. The report could not be deleted. Please, try again. The report could not be saved. Please, try again. The report has been deleted. The report has been saved. The thread could not be deleted. Please, try again. The thread could not be moved. Please, try again. The thread could not be saved. Please, try again. The thread has been deleted. The thread has been moved. The thread has been saved. Thread Thread: Threads Title User View View Reply View Thread Yes [Category] [Is Locked] [Is Sticky] [Is Visible] next previous {0} liked this post. Project-Id-Version: PROJECT VERSION
PO-Revision-Date: 2019-10-26 14:59+0200
Language-Team: LANGUAGE <EMAIL@ADDRESS>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.0.6
Last-Translator: 
Language: gl_ES
 Accións Engadir categoría Engadir Moderador Engadir Resposta Engadir Fío Engadir subcategoría Seguro que queres eliminar "{0}"? Seguro que queres eliminar "{0}"? Categorías Lista de Categorías Categoría Creado Eliminar Eliminar categoría Eliminar Resposta Eliminar Fío Descripción Conversa en "{0}" comezada por {1}, {2} Realmente queres eliminar este post? Editar Editar categoría Editar Resposta Editar Fío Filtro Foro Oculto Está Bloqueado Está Anclado É visible Última mensaxe Último: {0} Gústame Lista de categorías Lista de Moderadores Lista de Respostas Lista de Fíos Bloqueado Mensaxe Moderador Moderadores Moderadores: Modificado Mover Mover Fío: {0} Mover abaixo Mover arriba As miñas conversas Nova categoría Nova Resposta Novo Fío Non Non se atoparon categorías Non hai moderadores. Sin respostas. Non se atoparon reportes. Respostas Responder Reportar Reportar post in "{0}" Reportado Reporte Lista de reportes Data de inicio Anclado Gardar A categoría non puido ser eliminada. Por favor proba de novo. A categoría non puido ser movida. Por favor, proba de novo. A categoría non puido ser gardada. Por favor proba de novo. A categoría eliminouse correctamente. A categoría foi movida. A categoría gardouse correctamente. O "like" non puido ser gardado. Por favor proba de novo. O "like" gardouse correctamente. O moderador non puido ser eliminado. Por favor proba de novo. O moderador non puido ser gardado. Por favor proba de novo. O moderador foi engadido. O moderador foi eliminado. A resposta non puido ser eliminada. Por favor proba de novo. A resposta non puido ser gardada. Por favor proba de novo. A resposta eliminouse correctamente. A resposta gardouse correctamente. O reporte non puido ser eliminado. Por favor proba de novo. O reporte non puido ser gardado. Por favor proba de novo. O reporte foi eliminado. O reporte gardouse correctamente. O fío non puido ser eliminado. Por favor proba de novo. O fío non puido ser movido. Por favor proba de novo. O fío non puido ser gardado. Por favor proba de novo. O fío foi eliminado. O fío foi movido. O fío foi gardado. Fío Fío: Fíos Título Usuario Ver Ver Resposta Ver Fío Si [Categoría] [Bloqueado] [Está Anclado] [Visible] seguinte anterior {0} gustoulles o post. 