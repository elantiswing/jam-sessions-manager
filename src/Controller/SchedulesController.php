<?php

namespace App\Controller;

use Cake\Core\Configure;

class SchedulesController extends AppController
{

    public function availableSchedulesForTunes() {
        $this->paginate = [
            'sortWhitelist' => ['name', 'artist'],
            'limit' => 50
        ];

        $availableSchedules = $this->Schedules->AvailableSchedules->find()->toArray();
        
        // $this->loadModel('SchedulesExceptions');
        // $exceptions = $this->SchedulesExceptions->find()
        //                 ->where(['SchedulesExceptions.edition_id' => Configure::read('edition')['edition_id']]);

        $query = $this->Schedules->find('availablesTunes');
        $tunes = $this->paginate($query);

        $this->set(compact(['tunes', 'availableSchedules']));
    }

    public function makeSchedules() {
        $data = $this->Schedules->find('dataForTableMaker');

        $this->loadModel('MakedSchedules');
        $sorter = $this->MakedSchedules->find()->last();

        // debug($data);
        // exit;

        $this->set(compact('data', 'sorter'));
    }

    public function saveMakedSchedule() {
        $this->request->allowMethod(['AJAX']);

        $this->loadModel('MakedSchedules');

        $entity = $this->MakedSchedules->newEntity([
            'edition_id' => Configure::read('edition')['edition_id'],
            'users' => json_encode($this->request->getData('users')),
            'tunes' => json_encode($this->request->getData('tunes'))
        ]);

        $response = ['success' => false];
        if($this->MakedSchedules->save($entity)) {
            $response = ['success' => true];
        }
        
        $this->response->body(json_encode($response));
        return $this->response;
    }
}
