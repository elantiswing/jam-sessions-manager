<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Core\Configure;
use Cake\Event\Event;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false,
        ]);
        $this->loadComponent('Flash');
        $this->loadComponent('CakeDC/Users.UsersAuth');

        /*
         * Enable the following component for recommended CakePHP security settings.
         * see https://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        //$this->loadComponent('Security');

        $this->loadModel('Config');
        $config = $this->Config->getConfig()->toArray();
        Configure::write('config', $config);

        $this->set([
            'loginData' => $this->Auth->user(),
            'config' => $config
        ]);

        $this->_setEditionInSession();

        $this->_checkIfUserHasSchedules();
    }

    private function _checkIfUserHasSchedules() {
        $controller = $this->request->getParam('controller');
        $action = $this->request->getParam('action');
        $url = $controller . '/' . $action;
        
        $allowed = ['MyUsers/edit', 'Users/login', 'Users/logout'];

        if(in_array($url, $allowed)) {
            return true;
        }

        if(!empty($this->Auth->user())) {
            $this->loadModel('Schedules');
            $schedules = $this->Schedules->find()
                            ->where([
                                'Schedules.user_id' => $this->Auth->user('id')
                            ])
                            ->count();
            
            $numberOfAvailableSchedules = 4;
            if($schedules < $numberOfAvailableSchedules) {
                $this->Flash->error(__('Tes que poñer os horarios antes de nada'));
                $this->redirect(['controller' => 'MyUsers', 'action' => 'edit']);
            }
        }
    }

    private function _setEditionInSession() {

        if(!empty(Configure::read('edition'))) {
            return true;
        }

        $this->loadModel('Editions');
        $edition = $this->Editions->find()
                        ->order([
                            'edition' => 'DESC'
                        ])
                        ->first();

        Configure::write('edition', [
            'edition_id' => $edition->id,
            'edition' => $edition->edition,
            'name' => $edition->name
        ]);

        $this->set([
            'editionData' => [
                'edition_id' => $edition->id,
                'edition' => $edition->edition,
                'name' => $edition->name
            ]
        ]);
    }
}
