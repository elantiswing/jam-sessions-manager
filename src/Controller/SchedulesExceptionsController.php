<?php

namespace App\Controller;

use Cake\Core\Configure;

class SchedulesExceptionsController extends AppController
{

    public function saveExceptions() {
        $this->request->allowMethod(['AJAX']);

        $editionId = Configure::read('edition')['edition_id'];

        $data = $this->request->getData();
        
        $this->SchedulesExceptions->deleteAll([
            'SchedulesExceptions.edition_id' => $editionId,
            'SchedulesExceptions.tune_id' => $data['tune_id']
        ]);

        if(empty($data['users'])) {
            $response = ['success' => true];
        }else {
            $entities = [];
            foreach($data['users'] as $user) {
                $entity = [
                    'tune_id' => $data['tune_id'],
                    'user_id' => $user,
                    'edition_id' => $editionId
                ];
                array_push($entities, $this->SchedulesExceptions->newEntity($entity));
            }

            $response = ['success' => false];
            if($this->SchedulesExceptions->saveMany($entities)) {
                $response = ['success' => true];
            }
        }

        $this->response->body(json_encode($response));
        return $this->response;
    }
}
