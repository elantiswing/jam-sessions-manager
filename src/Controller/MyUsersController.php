<?

namespace App\Controller;

use CakeDC\Users\Controller\Traits\LoginTrait;
use CakeDC\Users\Controller\Traits\RegisterTrait;
use CakeDC\Users\Controller\Traits\PasswordManagementTrait;
use CakeDC\Users\Controller\UsersController;
use Cake\Core\Configure;
use CakeDC\Users\Controller\Component\UsersAuthComponent;

class MyUsersController extends UsersController
{
  use LoginTrait;
  use RegisterTrait, PasswordManagementTrait {
    PasswordManagementTrait::resetPassword insteadof RegisterTrait;
    PasswordManagementTrait::requestResetPassword insteadof RegisterTrait;
    PasswordManagementTrait::resetGoogleAuthenticator insteadof RegisterTrait;
    PasswordManagementTrait::validate insteadof RegisterTrait;
    PasswordManagementTrait::resendTokenValidation insteadof RegisterTrait;
  }

  public function initialize()
  {
    parent::initialize();

    $this->Security->setConfig('unlockedActions', ['add', 'edit']);
  }

  public function add() {
    $user = $this->MyUsers->newEntity();

    if($this->request->is(['POST'])) {
        $user = $this->MyUsers->patchEntity($user, $this->request->getData());

        if($this->MyUsers->save($user)) {
            $this->Flash->success(__('A información gardouse correctamente'));
            $this->redirect(['action' => 'list']);
        }else {
            $this->Flash->error(__('Ups! Non se puido gardar a información, proba de novo ou contacta con Max'));
        }
    }

    $this->loadModel('AvailableSchedules');
    
    $availableSchedules = $this->AvailableSchedules->find()->toArray();

    $this->set(compact('user'));
  }

  public function edit($id = NULL) {
    if(!$id || !$this->Auth->user('is_superuser')) {
        $id = $this->Auth->user('id');
    }
    
    $user = $this->MyUsers->find()
                ->contain(['Schedules', 'Instruments'])
                ->where(['MyUsers.id' => $id])
                ->first();

    if($this->request->is(['PUT', 'PATCH'])) {
        $user = $this->MyUsers->patchEntity($user, $this->request->getData());

        if($this->MyUsers->save($user)) {
            $this->Flash->success(__('A información gardouse correctamente'));
            $this->redirect(['action' => 'list']);
        }else {
            $this->Flash->error(__('Ups! Non se puido gardar a información, proba de novo ou contacta con Max'));
        }
    }

    $this->loadModel('AvailableSchedules');
    
    $availableSchedules = $this->AvailableSchedules->find()->toArray();

    $this->set(compact('user'));
  }

  public function changePassword($id = null) {
    $this->eventManager()->on(UsersAuthComponent::EVENT_AFTER_CHANGE_PASSWORD, function ($user) {
      $this->redirect(['plugin' => false, 'controller' => 'MyUsers', 'action' => 'edit']);
    });

    parent::changePassword();
  }

  public function list() {
    $this->paginate = [
      'sortWhitelist' => ['username'],
      'limit' => 50,
      'order' => ['username' => 'ASC']
    ];

    $query = $this->MyUsers->find()
              ->contain(['Editions']);

    $users = $this->paginate($query);

    $this->set(compact('users'));
  }

  public function activateForEdition($userId) {
    $this->loadModel('EditionsUsers');

    $editionUser = $this->EditionsUsers->newEntity([
                      'user_id' => $userId,
                      'edition_id' => Configure::read('edition')['edition_id']
                    ]);
    
    if($this->EditionsUsers->save($editionUser)) {
      $this->Flash->success(__('O usuario participa nesta edición'));
      $this->redirect(['action' => 'list']);
    }else {
      $this->Flash->error(__('Ups! Non se puido gardar a información, proba de novo ou contacta con Max'));
    }
  }

  public function deactivateForEdition($userId) {
    $this->loadModel('EditionsUsers');

    $editionUser = $this->EditionsUsers->find()
                    ->where([
                      'EditionsUsers.user_id' => $userId,
                      'EditionsUsers.edition_id' => Configure::read('edition')['edition_id']
                    ])
                    ->first();
    
    if($this->EditionsUsers->delete($editionUser)) {
      $this->Flash->success(__('O usuario xa non participa nesta edición'));
      $this->redirect(['action' => 'list']);
    }else {
      $this->Flash->error(__('Ups! Non se puido gardar a información, proba de novo ou contacta con Max'));
    }
  }

  public function getUserSelections($userId = null) {
    $this->request->allowMethod('AJAX');

    if(!$this->Auth->user('is_superuser') || ($this->Auth->user('is_superuser') && empty($userId))) {
      $userId = $this->Auth->user('id');
    }

    $this->loadModel('Instruments');
    $instruments = $this->Instruments->find()->toArray();

    $userSelections = $this->MyUsers->find('userSelections', ['user_id' => $userId, 'instruments' => $instruments]);
    
    $this->set(compact('userSelections', 'userId'));

    $this->render('/Element/user_selections');
  }

  public function usersSelectedTunes() {
    $this->loadModel('Instruments');
    $instruments = $this->Instruments->find()->toArray();

    $userSelections = $this->MyUsers->find('allUsersSelections', ['instruments' => $instruments]);

    $this->set(compact('userSelections'));
  }
}