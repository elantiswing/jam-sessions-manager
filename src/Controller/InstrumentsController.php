<?

namespace App\Controller;

class InstrumentsController extends AppController
{

  public function usersByInstrument() {
    $usersByInstrument = $this->Instruments->find('usersByInstrument');

    $this->set(compact('usersByInstrument'));
  }
}