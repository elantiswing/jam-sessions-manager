<?

namespace App\Controller;

use App\Controller\AppController;
use Cake\Datasource\ConnectionManager;

class MigrationsController extends AppController
{
    public function initialize() {
        $this->loadModel('MyUsers');

        $this->connection = ConnectionManager::get('default');
        $this->oldConnection = ConnectionManager::get('old');
    }

    public function index() {
        // $this->_migrateUsers();
        // $this->_migrateTunes();
        // $this->_migrateInstrumentsUsers();
        // $this->_migrateEditionsTunes();
    }

    private function _migrateEditionsTunes() {
        $this->loadModel('OldTunes');
        $this->OldTunes
            ->setConnection($this->oldConnection)
            ->table('tunes');

        $this->loadModel('EditionsTunes');
        $this->loadModel('Editions');

        $tunes = $this->OldTunes->find()->toArray();
        $editions = $this->Editions->find()->toArray();

        $editionsTunes = [];
        foreach($tunes as $tune) {
            $editionId = 0;

            foreach($editions as $edition) {
                if($edition->edition == $tune->selected) {
                    $editionId = $edition->id;
                    break;
                }
            }

            if($editionId !== 0) {
                array_push($editionsTunes, ['tune_id' => $tune->id, 'edition_id' => $editionId]);
            }
        }

        $editionsTunes = $this->EditionsTunes->newEntities($editionsTunes);

        debug($this->EditionsTunes->saveMany($editionsTunes));
        exit();
    }

    private function _migrateInstrumentsUsers() {
        $this->loadModel('CustomInstrumentsUsers');
        $this->CustomInstrumentsUsers->table('instruments_users');
        $this->CustomInstrumentsUsers->belongsTo('Users')
            ->bindingKey('id_old');

        $instrumumentsUsers = $this->CustomInstrumentsUsers->find()
                                    ->contain(['Users'])
                                    ->toArray();

        foreach($instrumumentsUsers as $instrumentUser) {
            $instrumentUser->user_id = $instrumentUser->user->id;
        }

        debug($this->CustomInstrumentsUsers->saveMany($instrumumentsUsers));
        exit();
    }

    private function _migrateTunes() {
        $this->loadModel('Tunes');

        $this->loadModel('CustomTunes');
        $this->CustomTunes->table('tunes');
        $this->CustomTunes->belongsTo('Users')->bindingKey('id_old');
        
        $tunes = $this->CustomTunes->find()
                    ->contain(['Users'])
                    ->toArray();

        $toSave = [];
        foreach($tunes as $tune) {
            if(!empty($tune->user)) {
                $tune->user_id = $tune->user->id;
            }
        }

        debug($this->Tunes->saveMany($tunes));
        exit();
    }

    private function _migrateUsers()
    {   
        $oldUsers = $this->oldConnection
                    ->execute('
                        SELECT 
                            users.id as user_id, 
                            users.username,
                            users.password,
                            users.role,
                            users.created,
                            profiles.id as profile_id, 
                            profiles.name,
                            profiles.surname1,
                            profiles.surname2,
                            profiles.phone,
                            profiles.email,
                            profiles.updated
                        FROM users, profiles 
                        WHERE users.id = profiles.user_id
                    ')
                    ->fetchAll('assoc');

        $toSave = [];
        foreach($oldUsers as $user) {
            if($user['role'] == 'admin') {
                $user['is_superuser'] = 1;
            }

            $user['id_old'] = $user['user_id'];
            $user['modified'] = $user['updated'];

            if(empty($user['created'])) {
                $user['created'] = date('Y-m-d H:i:s');
                $user['modified'] = $user['created'];
            }
            
            array_push($toSave, $this->MyUsers->newEntity($user));
        }

        debug($this->MyUsers->saveMany($toSave));
        exit();
    }
}