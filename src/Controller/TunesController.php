<?php

namespace App\Controller;

use Cake\Core\Configure;
use Cake\Collection\Collection;

class TunesController extends AppController
{
    public function initialize() {
        parent::initialize();

        $this->loadComponent('Search.Prg', [
            // This is default config. You can modify "actions" as needed to make
            // the PRG component work only for specified methods.
            'actions' => ['allTunes']
        ]);
    }
    public function allTunes() {
        $this->paginate = [
            'sortWhitelist' => ['name', 'artist'],
            'limit' => 50
        ];
        $query = $this->Tunes->find('search', ['search' => $this->request->getQueryParams()])
                    ->contain(['Editions', 'MyUsers']);

        $tunes = $this->paginate($query);

        $this->set(compact('tunes'));
    }

    public function proposesList() {
        $this->paginate = [
            'finder' => 'allProposes',
            'sortWhitelist' => ['name', 'artist', 'MyUsers.username'],
            'limit' => 30
        ];

        $tunes = $this->paginate($this->Tunes);

        $this->set(compact('tunes'));
    }

    public function viewProposes() {
        throw new \Cake\Http\Exception\NotImplementedException('Not implemented');
        // TODO
    }

    public function add() {
        $tune = $this->Tunes->newEntity();

        if($this->request->is('POST')) {
            $data = $this->request->getData();
            
            $edition = $this->Tunes->Editions->find()
                        ->where(['Editions.edition' => Configure::read('config')['edition']])
                        ->first();

            if(!$edition) {
                $this->Flash->error(__('Hai un problema na configuración da edición. Contacta con Max'));
                $this->redirect(['action' => 'proposesList']);
            }

            $data['editions']['_ids'] = [$edition->id];

            $tune = $this->Tunes->patchEntity($tune, $data);

            $tune->set([
                'user_id' => $this->Auth->user('id')
            ]);

            if($this->Tunes->save($tune)) {
                $this->Flash->success(__('O tema gardouse correctamente'));
                $this->redirect(['action' => 'proposesList']);
            }else {
                $this->Flash->error(__('Ups! Houbo un erro o gardar o tema. Proba de novo'));
            }
        }

        $this->set(compact('tune'));
    }

    public function edit($id = null) {
        if(empty($id)) {
            $this->Flash->error(__('Non se atopouo tema'));
            $this->redirect(['action' => 'proposesList']);
        }

        $tune = $this->Tunes->find('byId', ['id' => $id]);

        if($this->request->is(['PUT', 'PATCH'])) {
            $tune = $this->Tunes->patchEntity($tune, $this->request->getData());

            if($this->Tunes->save($tune)) {
                $this->Flash->success(__('O tema gardouse correctamente'));
                $this->redirect(['action' => 'proposesList']);
            }else {
                $this->Flash->error(__('Ups! Houbo un erro o gardar o tema. Proba de novo'));
            }
        }

        $this->set(compact('tune'));
    }

    public function addToEdition($tuneId) {
        if($this->Tunes->addToEdition($tuneId)) {
            $this->Flash->success('A canción engadiuse á lista de proposta desta edición');
        }else {
            $this->Flash->error('A canción que intentas engadir xa está na lista de propostas');
        }
        $this->redirect($this->referer());
    }

    public function removeFromEdition($tuneId) {
        if($this->Tunes->removeFromEdition($tuneId)) {
            $this->Flash->success('A canción quitouse da lista de proposta desta edición');
        }else {
            $this->Flash->error('Ups! Oubo un erro o quitar a canción da lista');
        }
        $this->redirect($this->referer());
    }

    public function select($userId = NULL) {
        if(!$this->Auth->user('is_superuser') || empty($userId)) {
            $userId = $this->Auth->user('id');
        }

        $tunes = $this->Tunes->find('forSelect');

        $user = $this->Tunes->MyUsers->find()
                            ->contain(['Instruments'])
                            ->where([
                                'MyUsers.id' => $userId
                            ])
                            ->first();
        
        $userInstruments = [];
        if(!empty($user->instruments)) {
            $instruments = new Collection($user->instruments);
            $userInstruments = $instruments->map(function($instrument) {
                                return $instrument->id;                
                            })
                            ->toArray();
        }

        $this->set([
            'tunes' => $tunes,
            'userInstruments' => $userInstruments,
            'user' => $user
        ]);
    }

    public function saveSelection() {
        $this->request->allowMethod(['AJAX']);

        $this->loadModel('UsersTunes');

        $response = ['success' => false];

        if(json_decode($this->request->getData('state'))) {
            $coros_id = 2;
            if($this->request->getData('instrument_id') != $coros_id) {
                $userTune = $this->UsersTunes->find()
                                            ->contain(['MyUsers'])
                                            ->where([
                                                'UsersTunes.tune_id' => $this->request->getData('tune_id'),
                                                'UsersTunes.instrument_id' => $this->request->getData('instrument_id'),
                                                'UsersTunes.edition_id' => Configure::read('edition')['edition_id']
                                            ])
                                            ->first();

                if($userTune) {
                    $this->response->body(json_encode(['status' => 'exists', 'success' => true, 'username' => $userTune->user->username]));
                    return $this->response;
                }
            }
            
            $userTune = $this->UsersTunes->newEntity([
                'tune_id' => $this->request->getData('tune_id'),
                'instrument_id' => $this->request->getData('instrument_id'),
                'user_id' => $this->request->getData('user_id'),
                'edition_id' => Configure::read('edition')['edition_id']
            ]);
            
            if($this->UsersTunes->save($userTune)) {
                $response = ['status' => 'saved', 'success' => true];
            }
        }else {
            $userTune = $this->UsersTunes->find()
                        ->where([
                            'UsersTunes.tune_id' => $this->request->getData('tune_id'),
                            'UsersTunes.instrument_id' => $this->request->getData('instrument_id'),
                            'UsersTunes.user_id' => $this->request->getData('user_id')
                        ])
                        ->first();
            
            if($this->UsersTunes->delete($userTune)) {
                $response = ['status' => 'removed', 'success' => true];
            }
        }

        $this->response->body(json_encode($response));
        return $this->response;
    }
}
