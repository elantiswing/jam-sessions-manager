<?php

namespace App\Controller;

use Cake\Core\Configure;

class ForumAdminController extends AppController
{

    public function setUpForumForEdition() {
        $this->loadModel('Tunes');
        $this->loadModel('\CakeDC\Forum\Model\Table\CategoriesTable');

        $tunes = $this->Tunes->find()
            ->contain('Editions')
            ->matching('Editions', function($q) {
                return $q->where(['Editions.id' => Configure::read('edition')['edition_id']]);
            })
            ->toArray();

        $toSave = [];
        foreach($tunes as $tune) {
            $data = [
                'parent_id' => null,
                'title' => $tune->fullName,
                'is_visible' => true
            ];
            array_push($toSave, new \CakeDC\Forum\Model\Entity\Category($data));
        }

        // debug($this->Categories->saveMany($toSave));
        exit();
    }
}
