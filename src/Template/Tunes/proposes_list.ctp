<div class="tunes index">
    <h2>
        <? 
            $count = !empty($this->request->getParam('paging')['Tunes']['count']) ? $this->request->getParam('paging')['Tunes']['count'] : 0;
            echo __('Lista de propostas') . ' (' . $count . ')'; 
        ?>
    </h2>
    <table class="table table-striped" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><? echo $this->Paginator->sort('name', 'Nome da canción'); ?></th>
                <th><? echo $this->Paginator->sort('artist', 'Grupo'); ?></th>
                <th>Link de youtube</th>
                <? if($loginData['is_superuser']): ?>
                    <th><? echo $this->Paginator->sort('Users.username', 'Proposto por'); ?></th>
                    <th class="actions"><? echo __('Accións'); ?></th>
                <? endif; ?>
            </tr>
        </thead>
        <tbody>
            <? foreach( $tunes as $tune ): ?>
                <tr>
                    <td>
                        <? echo $tune->name; ?>
                    </td>
                    <td>
                        <? echo $tune->artist; ?>
                    </td>
                    <td>
                        <? echo $this->Html->link('Clica aquí', $tune->link, array('target' => '_blank'));  ?>
                    </td>
                    <? if($loginData['is_superuser']): ?>
                        <td><? echo !empty($tune->user->username) ? $tune->user->username : ''; ?></td>
                        <td class="actions">
                            <? 
                                echo $this->Html->link(__('Editar'), 
                                        [
                                            'action' => 'edit', 
                                            $tune->id
                                        ], 
                                        ['class' => 'btn btn-xs btn-success']
                                    ); 
                            ?>
                            <? 
                                echo $this->Html->link(__('Quitar'), 
                                        [
                                            'action' => 'removeFromEdition', 
                                            $tune->id
                                        ], 
                                        ['class' => 'btn btn-xs btn-warning']
                                    ); 
                            ?>
                            <? 
                                echo $this->Form->postLink(__('Eliminar'), 
                                        [
                                            'action' => 'delete', 
                                            $tune->id
                                        ], 
                                        [
                                            'class' => 'btn btn-xs btn-danger',
                                            'confirm' => __('Seguro que queres eliminar ' . $tune->name . '??')
                                        ]
                                    ); 
                            ?>
                        </td>
                    <? endif; ?>
                </tr>
        <? endforeach; ?>
        </tbody>
    </table>
    <nav aria-label="Page navigation">
        <ul class="pagination">
            <? echo $this->Paginator->prev() ?>
            <? echo $this->Paginator->numbers() ?>
            <? echo $this->Paginator->next() ?>
        </ul>
    </nav>
</div>
