<div class="tunes index">
  <h2><?php echo __('As miñas propostas'); ?></h2>
  <p>Podes propoñer 3 temas para tocar na jam. Pénsaos ben porque só son 3. Podes Modificalos ou borralos cando queiras.</p>
  <p>Acórdate de que isto é unha Jam, xa sei que a todos nos gusta tocar temas que musicalmente molen moito, pero necesitamos temas de cachondeo tamen, temas en español, míticos...</p>
  <p><strong>O prazo de propostas remata o Domingo 20</strong></p>
  <table class="table table-striped" cellpadding="0" cellspacing="0">
    <thead>
      <tr>
        <th><?php echo __('Nome da canción'); ?></th>
        <th><?php echo __('Grupo'); ?></th>
        <th><?php echo __('Link de youtube'); ?></th>
        <th class="actions"><?php echo __('Accións'); ?></th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($tunes as $tune) : ?>
        <tr>
          <td><?php echo h($tune['Tune']['name']); ?>&nbsp;</td>
          <td><?php echo h($tune['Tune']['artist']); ?>&nbsp;</td>
          <td><?php echo $this->Html->link('Clica aquí', $tune['Tune']['link'], array('target' => '_blank'));  ?>&nbsp;</td>
          <td class="actions">
            <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $tune['Tune']['id']), array('class' => 'btn btn-xs btn-success')); ?>
            <?php echo $this->Form->postLink(
                __('Delete'),
                array('action' => 'delete', $tune['Tune']['id']),
                array('confirm' => __('Are you sure you want to delete # %s?', $tune['Tune']['id']), 'class' => 'btn btn-xs btn-danger')
              );
              ?>
          </td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
  <?
  if (count($tunes) < 3) {
    echo $this->Html->link(__('Nova Proposta'), array('controller' => 'tunes', 'action' => 'add'), array('class' => 'btn btn-default'));
  }
  ?>
</div>