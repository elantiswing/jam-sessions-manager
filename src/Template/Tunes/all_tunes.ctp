<div class="tunes index">
  <h2><? echo __('Cancións'); ?></h2>


  <? echo $this->Form->create(null, ['valueSources' => 'query']); ?>
    <div class="row">
      <div class="col-md-4 offset-md-8 col-sm-6 offset-sm-6">
        <div class="row">
          <div class="col-8">
            <?
              echo $this->cell('Editions', [
                  ['default' => $this->request->getQuery('editions')]
                ]);
            ?>
          </div>
          <div class="col-4">
            <? 
              echo $this->Form->button('Filtrar', [
                  'type' => 'submit',
                  'class' => 'btn btn-default'
                ]); 
            ?>
          </div>
        </div>
      </div>
      <div class="col-md-4 offset-md-8 col-sm-6 offset-sm-6">
        <?
          echo $this->Form->control('q', [
            'label' => false,
            'placeholder' => 'Buscar...'
          ]);

          if ($this->Search->isSearch()) {
            echo $this->Search->resetLink(__('Reset'), [
              'class' => 'reset-btn'
            ]);
          }
        ?>
      </div>
    </div>
  <? echo $this->Form->end(); ?>

  <table class="table table-striped" cellpadding="0" cellspacing="0">
    <thead>
      <tr>
        <th><? echo $this->Paginator->sort('name', 'Nome da canción'); ?></th>
        <th><? echo $this->Paginator->sort('artist', 'Grupo'); ?></th>
        <th>Link de youtube</th>
        <th>Tocouse en</th>
        <? if ($loginData['is_superuser']) : ?>
          <th class="actions"><? echo __('Accións'); ?></th>
        <? endif; ?>
      </tr>
    </thead>
    <tbody>
      <? foreach ($tunes as $tune) : ?>
        <tr>
          <td><? echo $tune->name; ?></td>
          <td><? echo $tune->artist; ?></td>
          <td><? echo $this->Html->link('Clica aquí', $tune->link, array('target' => '_blank'));  ?></td>
          <td><? echo !empty($tune->editionsList) ? $tune->editionsList : 'Non se tocou'; ?></td>
          <? if ($loginData['is_superuser']) : ?>
            <td class="actions">
              <? echo $this->Html->link(__('Editar'), array('action' => 'edit', $tune->id), array('class' => 'btn btn-xs btn-success')); ?>
              <? 
                echo $this->Html->link(__('Engadir'),
                      [
                        'action' => 'addToEdition', 
                        $tune->id
                      ],
                      ['class' => 'btn btn-xs btn-warning']
                    );
              ?>
              <? 
                echo $this->Form->postLink(__('Eliminar'),
                      [
                        'action' => 'delete', 
                        $tune->id
                      ],
                      ['confirm' => __('Seguro que queres eliminar ' . $tune->id . '??'), 'class' => 'btn btn-xs btn-danger']
                    );
              ?>
            </td>
          <? endif; ?>
        </tr>
      <? endforeach; ?>
    </tbody>
  </table>
  <nav aria-label="Page navigation">
    <ul class="pagination">
      <? echo $this->Paginator->prev() ?>
      <? echo $this->Paginator->numbers() ?>
      <? echo $this->Paginator->next() ?>
    </ul>
  </nav>
</div>