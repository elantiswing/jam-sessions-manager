<div class="tunes form">
    <? echo $this->Form->create($tune); ?>
    <fieldset>
        <legend><? echo __('Engadir canción'); ?></legend>
        <p>Revisa a <? echo $this->Html->link('lista de propostas', ['controller' => 'tunes', 'action' => 'proposes_list']) ?> antes de engadir novas propostas para que non haxa repetidas.</p>
        <p>Por favor, escribe o mellor que poidas o nome da canción e o grupo. E engade un enlace de youtube coa versión que queiras tocar.</p>
        <p><strong>Isto aforranos moito traballo ós organizadores despois, gracias!!</strong></p>
        <br/>
        
        <?
            echo $this->Form->input('name', array(
                    'label' => array(
                        'text' => __('Nome da canción'), 
                        'class' => 'control-label col-sm-2'
                    )
                ));
            echo $this->Form->input('artist', array(
                    'label' => array(
                        'text' => __('Grupo'), 
                        'class' => 'control-label col-sm-2'
                    )
                ));
            echo $this->Form->input('link', array(
                    'label' => array(
                        'text' => __('Link de youtube'), 
                        'class' => 'control-label col-sm-2'
                    )
                ));
            
            //Add instument selector for admins
            if($loginData['is_superuser']){
                echo $this->cell('Instruments::selectMultiple');
            }
            
            echo $this->Form->submit('Gardar', ['class' => 'btn btn-success float-right']);
        ?>
    </fieldset>
    <? echo $this->Form->end(); ?>
</div>
