<div class="tunes select">
    <div>
        <h2><?php echo __('Escoller temas'); ?></h2>

        <? if ($loginData['is_superuser']) : ?>
            <div class="row" id="users-selector">
                <div class="offset-lg-9 col-lg-3 offset-md-8 col-md-4 offset-sm-6 col-sm-6">
                    <h5>Seleccionando como</h5>
                    <?
                        echo $this->cell('Users', [['default' => $user->id]]);
                    ?>
                </div>
            </div>
        <? endif; ?>
        
        <div class="row" data-user-id="<? echo $user->id; ?>" data-user-username="<? echo $user->username; ?>">
            <? foreach ($tunes as $tune) : ?>
                <div class="tune col-lg-3 col-md-4 col-sm-6" data-tune-id="<? echo $tune->id; ?>">
                    <div class="inner">
                        <h5><? echo $this->Html->link($tune->name, $tune->link, ['target' => '_blank']); ?></h5>
                        <p>(<? echo $tune->artist; ?>)</p>
                        <table class="table table-sm table-striped">
                            <? foreach ($tune->instruments as $instrument) : ?>
                                <?
                                    $musicians = implode(', ', $instrument->musicians);
                                    $isChecked = in_array($user->id, array_keys($instrument->musicians));
                                    $coros_id = 2;
                                    $available = $instrument->id == $coros_id || (in_array($instrument->id, $userInstruments) && ($isChecked || empty($instrument->musicians)));
                                ?>
                                <tr class="instrument" data-instrument-id="<? echo $instrument->id ?>">
                                    <td class="instrument-name"><? echo $instrument->instrument; ?></td>
                                    <td class="musicians"><? echo $musicians ?></td>
                                    <? if ($loginData['is_superuser'] || !empty($config['tune_selection'])) : ?>
                                        <td class="selector">
                                            <? if($available): ?>
                                                <label class="custom-checkbox">
                                                    <input type="checkbox" name="selector" <? echo $isChecked ? 'checked' : '' ?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            <? endif; ?>
                                        </td>
                                    <? endif; ?>
                                </tr>
                            <? endforeach; ?>
                        </table>
                    </div>
                </div>
            <? endforeach; ?>
        </div>
    </div>
</div>
<? 
    echo $this->Html->script([
        'select_tunes',
    ], ['block' => 'scriptBottom']);
?>