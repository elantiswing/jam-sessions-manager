<? if(!empty($userId)):  ?>
    <div id="user-selections">
        <header>
            <h1 class="user-selections-title">Temas Seleccionados <? echo count($userSelections->tunes) ?></h1>
        </header>
        <section>
            <ol class="user-selections-tunes">
                <? foreach($userSelections->tunes as $tune): ?>
                    <li>
                        <? 
                            $instruments = implode(', ', \Cake\Utility\Hash::extract($tune->selected_instruments, '{n}.instrument'));

                            echo $this->Html->link($tune->name . ' (' . $tune->artist . ')', 
                                    $tune->link,
                                    ['target' => '_blank']
                                );
                        ?>
                        <p><? echo $instruments; ?></p>
                    </li>
                <? endforeach; ?>
            </ol>
        </section>
    </div>
<? endif; ?>