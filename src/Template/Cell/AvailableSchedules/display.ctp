<div id="available-schedules" class="row">
    <div class="col-12">
        <div class="row">
            <div class="col-12 day">
                <h3 class="text-center"><? echo $availableSchedules[0]->day; ?></h3>
            </div>
            <div class="col-6 schedule">
                <h5>Mañan</h5>
                <?
                    if(!empty($selected[0]->id)) {
                        echo $this->Form->hidden('schedules.0.id', ['value' => $selected[0]->id]);
                    }
                    echo $this->Form->hidden('schedules.0.available_schedules_id', ['value' => $availableSchedules[0]->id]);
                    echo $this->Form->hidden('schedules.0.edition_id', ['value' => $editionData['edition_id']]);
                    echo $this->Form->control('schedules.0.start', [
                        'label' => __('Dende'),
                        'type' => 'select',
                        'options' => $availableSchedules[0]->steps,
                        'data-type' => 'start',
                        'default' => empty($selected[0]->start) ? '' : $selected[0]->start->format('Y-m-d H:i:s'),
                        'disabled' => !empty($selected[0]->unavailable)
                    ]);

                    echo $this->Form->control('schedules.0.end', [
                        'label' => __('Hasta'),
                        'type' => 'select',
                        'options' => $availableSchedules[0]->steps,
                        'data-type' => 'end',
                        'default' => empty($selected[0]->end) ? key(array_slice($availableSchedules[0]->steps, -1, 1, true)) : $selected[0]->end->format('Y-m-d H:i:s'),
                        'disabled' => !empty($selected[0]->unavailable)
                    ]);
                ?>

                <label class="schedule-unavailable custom-checkbox">
                    <? echo __('NON PODO') ?>
                    <input type="checkbox" name="schedules[0][unavailable]" <? echo !empty($selected[0]->unavailable) ? 'checked' : '' ?>>
                    <span class="checkmark"></span>
                </label>
            </div>
            <div class="col-6 schedule">
                <h5>Tarde</h5>
                <? 
                    if(!empty($selected[1]->id)) {
                        echo $this->Form->hidden('schedules.1.id', ['value' => $selected[1]->id]);
                    }
                    echo $this->Form->hidden('schedules.1.available_schedules_id', ['value' => $availableSchedules[1]->id]);
                    echo $this->Form->hidden('schedules.1.edition_id', ['value' => $editionData['edition_id']]);
                    echo $this->Form->control('schedules.1.start', [
                        'label' => __('Dende'),
                        'type' => 'select',
                        'options' => $availableSchedules[1]->steps,
                        'data-type' => 'start',
                        'default' => empty($selected[1]->start) ? '' : $selected[1]->start->format('Y-m-d H:i:s'),
                        'disabled' => !empty($selected[1]->unavailable)
                    ]);
                    echo $this->Form->control('schedules.1.end', [
                        'label' => __('Hasta'),
                        'type' => 'select',
                        'options' => $availableSchedules[1]->steps,
                        'data-type' => 'end',
                        'default' => empty($selected[1]->end) ? key(array_slice($availableSchedules[1]->steps, -1, 1, true)) : $selected[1]->end->format('Y-m-d H:i:s'),
                        'disabled' => !empty($selected[1]->unavailable)
                    ]);
                ?>
                
                <label class="schedule-unavailable custom-checkbox">
                    <? echo __('NON PODO') ?>
                    <input type="checkbox" name="schedules[1][unavailable]" <? echo !empty($selected[1]->unavailable) ? 'checked' : '' ?>>
                    <span class="checkmark"></span>
                </label>
            </div>
        </div>
    </div>

    <div class="col-12">
        <div class="row">
            <div class="col-12 day">
                <h3 class="text-center"><? echo $availableSchedules[2]->day; ?></h3>
            </div>
            <div class="col-6 schedule">
                <h5>Mañan</h5>
                <? 
                    if(!empty($selected[2]->id)) {
                        echo $this->Form->hidden('schedules.2.id', ['value' => $selected[2]->id]);
                    }
                    echo $this->Form->hidden('schedules.2.available_schedules_id', ['value' => $availableSchedules[2]->id]);
                    echo $this->Form->hidden('schedules.2.edition_id', ['value' => $editionData['edition_id']]);
                    echo $this->Form->control('schedules.2.start', [
                        'label' => __('Dende'),
                        'type' => 'select',
                        'options' => $availableSchedules[2]->steps,
                        'data-type' => 'start',
                        'default' => empty($selected[2]->start) ? '' : $selected[2]->start->format('Y-m-d H:i:s'),
                        'disabled' => !empty($selected[2]->unavailable)
                    ]);
                    echo $this->Form->control('schedules.2.end', [
                        'label' => __('Hasta'),
                        'type' => 'select',
                        'options' => $availableSchedules[2]->steps,
                        'data-type' => 'end',
                        'default' => empty($selected[2]->end) ? key(array_slice($availableSchedules[2]->steps, -1, 1, true)) : $selected[2]->end->format('Y-m-d H:i:s'),
                        'disabled' => !empty($selected[2]->unavailable)
                    ]);
                ?>
                
                <label class="schedule-unavailable custom-checkbox">
                    <? echo __('NON PODO') ?>
                    <input type="checkbox" name="schedules[2][unavailable]" <? echo !empty($selected[2]->unavailable) ? 'checked' : '' ?>>
                    <span class="checkmark"></span>
                </label>
            </div>
            <div class="col-6 schedule">
                <h5>Tarde</h5>
                <? 
                    if(!empty($selected[3]->id)) {
                        echo $this->Form->hidden('schedules.3.id', ['value' => $selected[3]->id]);
                    }
                    echo $this->Form->hidden('schedules.3.available_schedules_id', ['value' => $availableSchedules[3]->id]);
                    echo $this->Form->hidden('schedules.3.edition_id', ['value' => $editionData['edition_id']]);
                    echo $this->Form->control('schedules.3.start', [
                        'label' => __('Dende'),
                        'type' => 'select',
                        'options' => $availableSchedules[3]->steps,
                        'data-type' => 'start',
                        'default' => empty($selected[3]->start) ? '' : $selected[3]->start->format('Y-m-d H:i:s'),
                        'disabled' => !empty($selected[3]->unavailable)
                    ]);
                    echo $this->Form->control('schedules.3.end', [
                        'label' => __('Hasta'),
                        'type' => 'select',
                        'options' => $availableSchedules[3]->steps,
                        'data-type' => 'end',
                        'default' => empty($selected[3]->end) ? key(array_slice($availableSchedules[3]->steps, -1, 1, true)) : $selected[3]->end->format('Y-m-d H:i:s'),
                        'disabled' => !empty($selected[3]->unavailable)
                    ]);
                ?>
                
                <label class="schedule-unavailable custom-checkbox">
                    <? echo __('NON PODO') ?>
                    <input type="checkbox" name="schedules[3][unavailable]" <? echo !empty($selected[3]->unavailable) ? 'checked' : '' ?>>
                    <span class="checkmark"></span>
                </label>
            </div>
        </div>
    </div>
</div>