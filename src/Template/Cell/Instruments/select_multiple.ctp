<?
    $defaults = [
        'label' => false,
        'type' => 'select',
        'multiple' => true,
        'id' => false,
        'class' => 'selectpicker',
        'data-none-selected-text' => 'Instrumentos'
    ];

    if(!empty($options['default'])) {
        $options['default'] = \Cake\Utility\Hash::extract($options['default'], '{n}.id');
    }
    
    $options = array_replace_recursive($defaults, $options);
    
    $name = !empty($options['name']) ? $options['name'] : 'instruments._ids';
    unset($options['name']);

    $options['options'] = $instruments;
    
    echo $this->Form->control($name, $options);
?>