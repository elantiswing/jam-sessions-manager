<?
    $defaults = [
        'label' => false,
        'type' => 'select',
        'id' => false,
        'empty' => 'TODOS'
    ];
    
    $options = array_replace_recursive($defaults, $options);
    
    $options['options'] = $editions;
    
    $name = !empty($options['name']) ? $options['name'] : 'editions';
    unset($options['name']);
    
    echo $this->Form->control($name, $options);
?>