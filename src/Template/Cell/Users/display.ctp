<?
    $defaults = [
        'label' => false,
        'type' => 'select',
        'id' => false
    ];
    
    $options = array_replace_recursive($defaults, $options);
    
    $options['options'] = $users;
    
    $name = !empty($options['name']) ? $options['name'] : 'users';
    unset($options['name']);
    
    echo $this->Form->control($name, $options);
?>