<div class="users form">
    <?= $this->Form->create($user) ?>
    <fieldset>
        <legend><?= __d('CakeDC/Users', 'Escribe o teu contrasinal actual') ?></legend>
        <?php if ($validatePassword) : ?>
            <?= $this->Form->control('current_password', [
                'type' => 'password',
                'required' => true,
                'label' => __d('CakeDC/Users', 'Contrasinal actual')]);
            ?>
        <?php endif; ?>
        <?= $this->Form->control('password', [
            'type' => 'password',
            'required' => true,
            'label' => __d('CakeDC/Users', 'Novo contrasinal')]);
        ?>
        <?= $this->Form->control('password_confirm', [
            'type' => 'password',
            'required' => true,
            'label' => __d('CakeDC/Users', 'Repite o contrasinal')]);
        ?>

    </fieldset>
    <?= $this->Form->button(__d('CakeDC/Users', 'Gardar'), ['class' => 'btn btn-success']); ?>
    <?= $this->Form->end() ?>
</div>