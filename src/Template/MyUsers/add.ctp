<? echo $this->Form->create($user); ?>
<div class="form-group row">
    <div class="col-md-8 col-sm-7">
        <fieldset>
            <legend><? echo __('Engadir usuario'); ?></legend>
            <?
                echo $this->Form->input('username', array(
                    'label' => __('Nome de usuario'),
                    'placeholder' => __('Nome de usuario')
                ));
                echo $this->Form->input('name', array(
                    'label' => __('Nome'),
                    'placeholder' => __('Nome'),
                ));
                echo $this->Form->input('first_surname', array(
                    'label' => __('Primeiro apelido'),
                    'placeholder' => __('Primeiro apelido'),
                ));
                echo $this->Form->input('second_surname', array(
                    'label' => __('Segundo apelido'),
                    'placeholder' => __('Segundo apelido'),
                ));
                echo $this->Form->input('phone', array(
                    'label' => __('Teléfono'),
                    'placeholder' => __('Teléfono'),
                ));
                echo $this->Form->input('email', array(
                    'label' => __('Email'),
                    'placeholder' => __('Email'),
                ));
                echo $this->Form->input('password', array(
                    'label' => __('Contrasinal'),
                    'placeholder' => __('Contrasinal'),
                ));
            ?>
        </fieldset>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <h4>Instrumentos</h4>
        <? echo $this->cell('Instruments::selectMultiple') ?>
    </div>
    <div class="col-12">
        <? echo $this->Form->submit(__('Gardar cambios'), ['class' => 'btn btn-success']) ?>
    </div>
</div>


<? echo $this->Form->end(); ?>