<? echo $this->Form->create($user); ?>
<div id="users-edit">
    <div class="form-group row">
        <div class="col-md-8">
            <fieldset>
                <legend><? echo __('Editar perfil'); ?></legend>
                <?
                    echo $this->Form->input('id');
                    echo $this->Form->input('username', array(
                        'label' => __('Nome de usuario'),
                        'placeholder' => __('Nome de usuario'),
                        'disabled' => 'disabled'
                    ));
                    echo $this->Form->input('name', array(
                        'label' => __('Nome'),
                        'placeholder' => __('Nome'),
                    ));
                    echo $this->Form->input('first_surname', array(
                        'label' => __('Primeiro apelido'),
                        'placeholder' => __('Primeiro apelido'),
                    ));
                    echo $this->Form->input('second_surname', array(
                        'label' => __('Segundo apelido'),
                        'placeholder' => __('Segundo apelido'),
                    ));
                    echo $this->Form->input('phone', array(
                        'label' => __('Teléfono'),
                        'placeholder' => __('Teléfono'),
                    ));
                    echo $this->Form->input('email', array(
                        'label' => __('Email'),
                        'placeholder' => __('Email'),
                    ));
                ?>

                <? if($loginData['is_superuser']): ?>
                    <br><hr><br>
                    <div class="row">
                        <div class="col-6">
                            <label>Instrumentos</label>
                            <? echo $this->cell('Instruments::selectMultiple', [['default' => $user->instruments]]); ?>
                        </div>
                        <div class="col-6">
                            <? 
                                echo $this->Form->input('password_change', array(
                                    'label' => __('Modifical contrasinal'),
                                    'placeholder' => __('Modifical contrasinal'),
                                    'value' => ''
                                ));
                            ?>
                        </div>
                    </div>
                <? else: ?>
                    <? echo $this->Html->link(__('Modificar contrasinal'), ['plugin' => false, 'controller' => 'MyUsers', 'action' => 'changePassword', $user->id]) ?>
                <? endif; ?>
            </fieldset>
        </div>
        <div class="col-md-4">
            <? echo $this->cell('AvailableSchedules', ['selected' => $user->schedules]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <? echo $this->Form->submit(__('Gardar cambios'), ['class' => 'btn btn-success']) ?>
        </div>
    </div>
    <? echo $this->Form->end(); ?>
</div>

<?
    echo $this->Html->script([
        '/plugins/moment.min',
        'schedules'
    ], ['block' => 'scriptBottom']);
?>