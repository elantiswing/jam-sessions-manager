<div class="tunes index">
  <h2><? echo __('Usuarios'); ?></h2>

  <p><span class="text-warning">Activar</span> ou <span class="text-info">desactivar</span> usuarios para a edición actual da JamMartiño</p>

  <table class="table table-striped" cellpadding="0" cellspacing="0">
    <thead>
      <tr>
        <th><? echo $this->Paginator->sort('username', 'Usuario'); ?></th>
        <th>Nome completo</th>
        <th><? echo $this->Paginator->sort('email', 'Email'); ?></th>
        <th class="actions"><? echo __('Accións'); ?></th>
      </tr>
    </thead>
    <tbody>
      <? foreach ($users as $user) : ?>
        <tr>
          <td><? echo $user->username; ?></td>
          <td><? echo $user->fullName; ?></td>
          <td><? echo $user->email;  ?></td>
          <td class="actions">
            <? echo $this->Html->link(__('Editar'), array('action' => 'edit', $user->id), array('class' => 'btn btn-xs btn-success')); ?>
            <? 
              echo $this->Form->postLink(__('Eliminar'),
                    [
                      'action' => 'delete', 
                      $user->id
                    ],
                    ['confirm' => __('Seguro que queres eliminar ' . $user->username . '??'), 'class' => 'btn btn-xs btn-danger']
                  );
            ?>
            <? 
              if($user->activeInEdition($editionData['edition_id'])) {
                echo $this->Html->link(__('Desactivar'), array('action' => 'deactivateForEdition', $user->id), array('class' => 'btn btn-xs btn-info')); 
              }else {
                echo $this->Html->link(__('Activar'), array('action' => 'activateForEdition', $user->id), array('class' => 'btn btn-xs btn-warning')); 
              }
            ?>
          </td>
        </tr>
      <? endforeach; ?>
    </tbody>
  </table>
  <nav aria-label="Page navigation">
    <ul class="pagination">
      <? echo $this->Paginator->prev() ?>
      <? echo $this->Paginator->numbers() ?>
      <? echo $this->Paginator->next() ?>
    </ul>
  </nav>
</div>