<div class="table-schedule">
  <div class="legend">
    <p>Podedes arrastrar as filas e as columnas dende os encabezados.</p>
    <p>Co botón de gardar quedan rexistrados os cambios feitos. Hai un histórico de cambios por si precisades voltar atrás (avisar a Max)</p>
    <p><span class="legend-item legend-red"></span> Coincidencia</p>
    <p><span class="legend-item legend-white"></span> Non coincidencia</p>
    <p><span class="legend-item legend-grey"></span> Excluído</p>
  </div>

  <table class="table table-sm table-bordered">
    <thead>
      <tr class="dnd-moved">
        <th class="locked"></th>
        <th data-user-id="100">Columna 1</th>
        <th data-user-id="200">Columna 2</th>
        <th data-user-id="300">Columna 3</th>
      </tr>
    </thead>
    <tboby>
      <tr class="dnd-moved" data-tune-id="1">
        <td class="handle-sortable">Fila 1</td>
        <td>Celda 1 - 1</td>
        <td>Celda 1 - 2</td>
        <td>Celda 1 - 3</td>
      </tr>
      <tr class="dnd-moved" data-tune-id="2">
        <td class="handle-sortable">Fila 2</td>
        <td>Celda 2 - 1</td>
        <td>Celda 2 - 2</td>
        <td>Celda 2 - 3</td>
      </tr>
      <tr class="dnd-moved" data-tune-id="3">
        <td class="handle-sortable">Fila 3</td>
        <td>Celda 3 - 1</td>
        <td>Celda 3 - 2</td>
        <td>Celda 3 - 3</td>
      </tr>
      <tr class="dnd-moved" data-tune-id="4">
        <td class="handle-sortable">Fila 4</td>
        <td>Celda 4 - 1</td>
        <td>Celda 4 - 2</td>
        <td>Celda 4 - 3</td>
      </tr>
      <tr class="dnd-moved" data-tune-id="5">
        <td class="handle-sortable">Fila 5</td>
        <td>Celda 5 - 1</td>
        <td>Celda 5 - 2</td>
        <td>Celda 5 - 3</td>
      </tr>
      <tr class="dnd-moved" data-tune-id="6">
        <td class="handle-sortable">Fila 6</td>
        <td>Celda 6 - 1</td>
        <td>Celda 6 - 2</td>
        <td>Celda 6 - 3</td>
      </tr>
      <tr class="dnd-moved" data-tune-id="7">
        <td class="handle-sortable">Fila 7</td>
        <td>Celda 7 - 1</td>
        <td>Celda 7 - 2</td>
        <td>Celda 7 - 3</td>
      </tr>
      <tr class="dnd-moved" data-tune-id="8">
        <td class="handle-sortable">Fila 8</td>
        <td>Celda 8 - 1</td>
        <td>Celda 8 - 2</td>
        <td>Celda 8 - 3</td>
      </tr>
      <tr class="dnd-moved" data-tune-id="9">
        <td class="handle-sortable">Fila 9</td>
        <td>Celda 9 - 1</td>
        <td>Celda 9 - 2</td>
        <td>Celda 9 - 3</td>
      </tr>
      <tr class="dnd-moved" data-tune-id="10">
        <td class="handle-sortable">Fila 10</td>
        <td>Celda 10 - 1</td>
        <td>Celda 10 - 2</td>
        <td>Celda 10 - 3</td>
      </tr>
    </tboby>
  </table>

  <button class="btn btn-success" id="save-data">Gardar</button>
</div>
<script>
  let data = <? echo json_encode($data) ?>;
  let sorter = <? echo json_encode($sorter) ?>;
</script>

<?
  echo $this->Html->css(
    [
      '/plugins/drag-n-drop-table-columns/css/dragndrop.table.columns',
    ],
    ['block' => 'cssTop']
  );
  echo $this->Html->script(
    [
      'https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js',
      'https://code.jquery.com/ui/1.12.1/jquery-ui.js',
      '/plugins/drag-n-drop-table-columns/dist/for-jQuery3.x/dragndrop.table.columns.min',
      'make_schedules'
    ],
    ['block' => 'scriptBottom']
  );
?>