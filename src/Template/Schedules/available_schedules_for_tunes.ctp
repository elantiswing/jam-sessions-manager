<div class="tunes index">
    <h2> <? echo __('Horarios dispoñibles por tema'); ?></h2>
    <table class="table table-striped" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><? echo $this->Paginator->sort('name', 'Nome da canción'); ?></th>
                <th><? echo $this->Paginator->sort('artist', 'Grupo'); ?></th>
                <? foreach($availableSchedules as $schedule): ?>
                  <th><? echo $schedule->day . ' (' . $schedule->type . ')' ?></th>
                <? endforeach; ?>
                <th>Excepciones</th>
            </tr>
        </thead>
        <tbody>
            <? foreach( $tunes as $tune ): ?>
                <tr>
                    <td>
                        <? echo $tune->name; ?>
                    </td>
                    <td>
                        <? echo $tune->artist; ?>
                    </td>
                    <? foreach($tune->schedule_max_range as $schedule): ?>
                      <td>
                        <? 
                          if($schedule['start'] == NULL) {
                            echo '---';
                          }else {
                            echo date('H:i', strtotime($schedule['start'])) . ' - ' . date('H:i', strtotime($schedule['end']));
                          }
                        ?>
                      </td>
                    <? endforeach; ?>
                    <td>
                      <?
                        $options = [];
                        foreach($tune->musicians as $musician) {
                          $options[$musician->id] = $musician->username;
                        }

                        $default = [];
                        foreach($tune->schedules_exceptions as $exception) {
                          array_push($default, $exception->user_id);
                        }

                        echo $this->Form->control('exceptions', [
                          'label' => false,
                          'type' => 'select',
                          'multiple' => true,
                          'options' => $options,
                          'default' => $default,
                          'class' => 'selectpicker',
                          'data-none-selected-text' => 'Músicos',
                          'data-tune-id' => $tune->id
                        ]);
                      ?>
                    </td>
                </tr>
        <? endforeach; ?>
        </tbody>
    </table>
    <nav aria-label="Page navigation">
        <ul class="pagination">
            <? echo $this->Paginator->prev() ?>
            <? echo $this->Paginator->numbers() ?>
            <? echo $this->Paginator->next() ?>
        </ul>
    </nav>
</div>

<? echo $this->Html->script(['schedules_exceptions'], ['block' => 'scriptBottom']) ?>