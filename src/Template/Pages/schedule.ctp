<div class="users index">
    <h2><?php echo __('Horarios'); ?></h2>
    <table class="table table-striped" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?php echo 'name'; ?></th>
                <th><?php echo 'horaio día 5 am'; ?></th>
                <th><?php echo 'horaio día 5 pm'; ?></th>
                <th><?php echo 'horaio día 6 am'; ?></th>
                <th><?php echo 'horaio día 6 pm'; ?></th>
                <th><?php echo 'músicos'; ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach( $schedules as $schedule ): ?>
                <? 
                    $class = '';
                    if($schedule['bestSchedule'][1]['start'] == false &&
                        $schedule['bestSchedule'][2]['start'] == false &&
                        $schedule['bestSchedule'][3]['start'] == false && 
                        $schedule['bestSchedule'][4]['start'] == false){
                        $class = 'danger';
                    }
                ?>
                <tr class="<? echo $class; ?>">
                    <td><?php echo $schedule['name']; ?>&nbsp;</td>
                    <td><?php echo $schedule['bestSchedule'][1]['start'] == false ? 'NON SE PODE' : date('H:i', strtotime($schedule['bestSchedule'][1]['start'])) . ' - ' . date('H:i', strtotime($schedule['bestSchedule'][1]['end'])); ?>&nbsp;</td>
                    <td><?php echo $schedule['bestSchedule'][2]['start'] == false ? 'NON SE PODE' : date('H:i', strtotime($schedule['bestSchedule'][2]['start'])) . ' - ' . date('H:i', strtotime($schedule['bestSchedule'][2]['end'])); ?>&nbsp;</td>
                    <td><?php echo $schedule['bestSchedule'][3]['start'] == false ? 'NON SE PODE' : date('H:i', strtotime($schedule['bestSchedule'][3]['start'])) . ' - ' . date('H:i', strtotime($schedule['bestSchedule'][3]['end'])); ?>&nbsp;</td>
                    <td><?php echo $schedule['bestSchedule'][4]['start'] == false ? 'NON SE PODE' : date('H:i', strtotime($schedule['bestSchedule'][4]['start'])) . ' - ' . date('H:i', strtotime($schedule['bestSchedule'][4]['end'])); ?>&nbsp;</td>
                    <td style="font-size: 11px">
                        <?php 
                            $listMusicians = [];
                            foreach($schedule['musicians'] as $musician){
                                array_push($listMusicians, $musician[1]['username']);
                            }
                            
                            echo implode(', ', $listMusicians);
                        ?>&nbsp;
                    </td>
                </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>
