<!DOCTYPE html>
<html lang="es">

<head>
  <title>Jam Martiño <? echo $editionData['name'] ?></title>

  <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Expires" content="0" />

  <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <meta name="description" content="">
  <meta name="author" content="Max Gómez">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" integrity="sha256-ENFZrbVzylNbgnXx0n3I1g//2WeO47XxoPe0vkp3NC8=" crossorigin="anonymous" />
  <!-- <link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet"> -->
  <?

    echo $this->fetch('cssTop');

    echo $this->Html->css('styles');
  ?>
</head>

<body class="preload">
  <?
  $controller = $this->request->getParam('controller');
  $menu = 0;

  if ($controller == 'pages')
    $menu = 1;
  if ($controller == 'profiles')
    $menu = 2;
  if ($controller == 'tunes')
    $menu = 3;
  if ($controller == 'users')
    $menu = 4;
  if ($controller == 'schedules')
    $menu = 5;
  if ($controller == 'topics' || $controller == 'posts' || $controller == 'forums')
    $menu = 6;
  ?>
  <header>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <div class="container">
        <?
          echo $this->Html->link($editionData['name'], 
                [
                  'plugin' => false,
                  'controller' => 'Pages',
                  'action' => 'index',
                  'prefix' => false
                ],
                ['class' => 'navbar-brand']
              );
        ?>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
              <?
                echo $this->Html->link(
                  'Inicio',
                  [
                    'plugin' => false,
                    'controller' => 'Pages',
                    'action' => 'index',
                    'prefix' => false
                  ],
                  ['class' => 'nav-link']
                );
              ?>
            </li>
            <li class="nav-item">
              <?
                echo $this->Html->link(
                  'Perfil',
                  [
                    'plugin' => false,
                    'controller' => 'MyUsers',
                    'action' => 'edit',
                    'prefix' => false
                  ],
                  ['class' => 'nav-link']
                );
              ?>
            </li>
            <li class="nav-item dropdown">
              <?
                echo $this->Html->link(
                  'Temas <span class="caret"></span>',
                  '#',
                  [
                    'class' => 'nav-link dropdown-toggle',
                    'id' => 'navbarTunesMenu',
                    'data-toggle' => 'dropdown',
                    'aria-haspopup' => true,
                    'aria-expanded' => false,
                    'escape' => false
                  ]
                );
              ?>
              <ul class="dropdown-menu" aria-labelledby="navbarTunesMenu">
                <li>
                  <?
                    echo $this->AuthLink->link(
                      'Todos os temas',
                      [
                        'plugin' => false,
                        'controller' => 'Tunes',
                        'action' => 'allTunes',
                        'prefix' => false
                      ],
                      ['class' => 'dropdown-item']
                    );
                  ?>
                </li>
                <li>
                  <?
                    echo $this->AuthLink->link(
                      'Lista de propostas',
                      [
                        'plugin' => false,
                        'controller' => 'Tunes',
                        'action' => 'proposesList',
                        'prefix' => false
                      ],
                      ['class' => 'dropdown-item']
                    );
                  ?>
                </li>
                <li>
                  <?
                    echo $this->AuthLink->link(
                      'As miñas propostas',
                      [
                        'plugin' => false,
                        'controller' => 'Tunes',
                        'action' => 'view_proposes',
                        'prefix' => false
                      ],
                      ['class' => 'dropdown-item']
                    );
                  ?>
                </li>
                <li>
                  <?
                    echo $this->AuthLink->link(
                      'Escoller temas',
                      [
                        'plugin' => false,
                        'controller' => 'Tunes',
                        'action' => 'select',
                        'prefix' => false
                      ],
                      ['class' => 'dropdown-item']
                    );
                  ?>
                </li>
              </ul>
            </li>
            <? if($loginData['is_superuser']): ?>
              <li class="nav-item dropdown">
                <?
                  echo $this->Html->link(
                    'Usuarios <span class="caret"></span>',
                    '#',
                    [
                      'class' => 'nav-link dropdown-toggle',
                      'id' => 'navbarUsersMenu',
                      'data-toggle' => 'dropdown',
                      'aria-haspopup' => true,
                      'aria-expanded' => false,
                      'escape' => false
                    ]
                  );
                ?>
                <ul class="dropdown-menu" aria-labelledby="navbarUsersMenu">
                  <li>
                    <?
                      echo $this->Html->link(
                        'Engadir',
                        [
                          'plugin' => false,
                          'controller' => 'MyUsers',
                          'action' => 'add',
                          'prefix' => false
                        ],
                        ['class' => 'dropdown-item']
                      );
                    ?>
                  </li>
                  <li>
                    <?
                      echo $this->Html->link(
                        'Listado',
                        [
                          'plugin' => false,
                          'controller' => 'MyUsers',
                          'action' => 'list',
                          'prefix' => false
                        ],
                        ['class' => 'dropdown-item']
                      );
                    ?>
                  </li>
                  <li>
                    <?
                      echo $this->Html->link(
                        'Usuarios por instrumento',
                        [
                          'plugin' => false,
                          'controller' => 'Instruments',
                          'action' => 'usersByInstrument',
                          'prefix' => false
                        ],
                        ['class' => 'dropdown-item']
                      );
                    ?>
                  </li>
                </ul>
              </li>
            <? endif; ?>
            <? if($loginData['is_superuser'] || $config['view_rehearsals']): ?>
              <li class="nav-item dropdown">
                <?
                  echo $this->Html->link(
                    'Horarios <span class="caret"></span>',
                    '#',
                    [
                      'class' => 'nav-link dropdown-toggle',
                      'id' => 'navbarSchedulesMenu',
                      'data-toggle' => 'dropdown',
                      'aria-haspopup' => true,
                      'aria-expanded' => false,
                      'escape' => false
                    ]
                  );
                ?>
                <ul class="dropdown-menu" aria-labelledby="navbarSchedulesMenu">
                  <? if($loginData['is_superuser']): ?>
                  <li>
                    <?
                      echo $this->Html->link(
                        'Horarios por tema',
                        [
                          'plugin' => false,
                          'controller' => 'Schedules',
                          'action' => 'availableSchedulesForTunes',
                          'prefix' => false
                        ],
                        ['class' => 'dropdown-item']
                      );
                    ?>
                  </li>
                  <li>
                    <?
                      echo $this->Html->link(
                        'Organizador',
                        [
                          'plugin' => false,
                          'controller' => 'Schedules',
                          'action' => 'makeSchedules',
                          'prefix' => false
                        ],
                        ['class' => 'dropdown-item']
                      );
                    ?>
                  </li>
                  <? endif; ?>
                  <? if($config['view_rehearsals']): ?>
                  <li>
                  </li>
                  <? endif; ?>
                </ul>
              </li>
            <? endif; ?>
            <li class="nav-item">
              <?
                echo $this->Html->link(
                  'Foro',
                  [
                    'plugin' => 'CakeDC/Forum',
                    'controller' => 'Categories',
                    'action' => 'index',
                    'prefix' => false
                  ],
                  ['class' => 'nav-link']
                );
              ?>
            </li>
          </ul>
          <ul class="navbar-nav">
            <li class="nav-item">
              <?
                echo $this->User->logout(
                  'Saír',
                  ['class' => 'nav-link']
                );
              ?>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  </header>
  <div class="container container-page">
    <?php echo $this->Flash->render() ?>

    <? echo $this->fetch('content'); ?>

    <? 
      if($loginData['is_superuser'] && !empty($user->id)) {
        $selectedUser = $user->id;
      }else {
        $selectedUser = $loginData['id'];
      }

      echo $this->cell('Users::selections', [$selectedUser]);
    ?>
  </div>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js" integrity="sha256-3blsJd4Hli/7wCQ+bmgXfOdK7p/ZUMtPXY08jmxSSgk=" crossorigin="anonymous"></script>
  <!-- <script src="//cdn.quilljs.com/1.3.6/quill.min.js"></script> -->
  <script src="//cdn.ckeditor.com/4.13.0/basic/ckeditor.js"></script>
  <?

    echo $this->Html->script(['user_selections', 'text-editor']); 

    echo $this->fetch('scriptBottom');
  // echo $this->Html->script('/plugins/bootstrap/js/bootstrap.min');
  // echo $this->Html->script('/plugins/jQuery-Multiple-Select/dist/js/bootstrap-multiselect');

  // echo $this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js');
  ?>


<script>
    $(function() {
      $("body").removeClass("preload");

      CSRF_TOKEN = <? echo json_encode($this->request->getParam('_csrfToken')); ?>;
    });
  </script>
</body>

</html>