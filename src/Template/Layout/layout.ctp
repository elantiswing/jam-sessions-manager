<!DOCTYPE html>
<html lang="es">

<head>
  <title>Jam Martiño Delas Jam</title>

  <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Expires" content="0" />

  <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <meta name="description" content="">
  <meta name="author" content="">

  <?
  echo $this->Html->css('/plugins/bootstrap/css/bootstrap.min');
  echo $this->Html->css('/plugins/bootstrap/css/bootstrap-theme.min');
  echo $this->Html->css('/plugins/jQuery-Multiple-Select/dist/css/bootstrap-multiselect');

  echo $this->Html->css(['https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css']);

  echo $this->Html->css('style');

  echo $this->Html->script('jquery-3.1.1.min');
  ?>
  <script>
    $(function() {
      toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": true,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "600",
        "hideDuration": "1000",
        "timeOut": "15000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
      }

      $('.instrumentSelect').multiselect({
        numberDisplayed: 5
      });
    })
  </script>
</head>

<body>
  <?
  $controller = $this->params['controller'];
  $menu = 0;

  if ($controller == 'pages')
    $menu = 1;
  if ($controller == 'profiles')
    $menu = 2;
  if ($controller == 'tunes')
    $menu = 3;
  if ($controller == 'users')
    $menu = 4;
  if ($controller == 'schedules')
    $menu = 5;
  if ($controller == 'topics' || $controller == 'posts' || $controller == 'forums')
    $menu = 6;
  ?>
  <nav id="myNavbar" class="navbar navbar-default navbar-inverse navbar-fixed-top" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbarCollapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">Jam Martiño Delas Jam</a>
      </div>
      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="navbarCollapse">
        <ul class="nav navbar-nav">
          <li class="<? echo $menu == 1 ? 'active' : '' ?>">
            <? echo $this->Html->link('Inicio', array('plugin' => false, 'controller' => 'pages', 'action' => 'index')) ?>
          </li>
          <li class="<? echo $menu == 2 ? 'active' : '' ?>">
            <? echo $this->Html->link('Perfil', array('plugin' => false, 'controller' => 'profiles', 'action' => 'edit', $authUser['id'])) ?>
          </li>
          <li class="<? echo $menu == 3 ? 'active' : '' ?>">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Temas <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <? if (!empty($config['users_view_select_tunes_list'])) : ?>
                <li><? echo $this->Html->link('Escoller temas', array('plugin' => false, 'controller' => 'tunes', 'action' => 'select')) ?></li>
              <? endif; ?>
              <? if (!empty($config['users_view_tunes_list'])) : ?>
                <li><? echo $this->Html->link('Lista', array('plugin' => false, 'controller' => 'tunes', 'action' => 'index')) ?></li>
              <? endif; ?>
              <li><? echo $this->Html->link('Lista de propostas', array('plugin' => false, 'controller' => 'tunes', 'action' => 'index')) ?></li>
              <li><? echo $this->Html->link('As miñas propostas', array('plugin' => false, 'controller' => 'tunes', 'action' => 'view_proposes')) ?></li>
              <? if ($authUser['role'] == 'admin') : ?>
                <li><? echo $this->Html->link('Engadir', array('plugin' => false, 'controller' => 'tunes', 'action' => 'add')) ?></li>
                <li><? echo $this->Html->link('Lista de temas', array('plugin' => false, 'controller' => 'tunes', 'action' => 'tunesList')) ?></li>
                <li><? echo $this->Html->link('Todos os temas', array('plugin' => false, 'controller' => 'tunes', 'action' => 'allTunes')) ?></li>
              <? endif; ?>
            </ul>
          </li>
          <? if ($authUser['role'] == 'admin') : ?>
            <li class="<? echo $menu == 4 ? 'active' : '' ?>">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Usuarios <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><? echo $this->Html->link('Lista', array('plugin' => false, 'controller' => 'users', 'action' => 'index')) ?></li>
                <li><? echo $this->Html->link('Engadir', array('plugin' => false, 'controller' => 'users', 'action' => 'add')) ?></li>
                <li><? echo $this->Html->link('Músicos por Instrumento', array('plugin' => false, 'controller' => 'users', 'action' => 'byInstrument')) ?></li>
              </ul>
            </li>
          <? endif; ?>
          <li class="<? echo $menu == 4 ? 'active' : '' ?>">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Horarios <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <? if ($authUser['role'] == 'admin') : ?>
                <li><? echo $this->Html->link('Posibilidades por tema', array('plugin' => false, 'controller' => 'pages', 'action' => 'schedule')) ?></li>
              <? endif; ?>
              <? if (!empty($config['view_rehearsals'])) : ?>
                <li><? echo $this->Html->link('Ensaios', array('plugin' => false, 'controller' => 'schedules', 'action' => 'viewPdf', 'ensaios2017')) ?></li>
              <? endif ?>
            </ul>
          </li>
          <li class="<? echo $menu == 6 ? 'active' : '' ?>"><? echo $this->Html->link('Foro', array('plugin' => 'forum', 'controller' => 'topics', 'action' => 'index', date('Y'))) ?></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
          <li><? echo $this->Html->link('Saír <span class="glyphicon glyphicon-log-out" aria-hidden="true"></span>', array('plugin' => false, 'controller' => 'users', 'action' => 'logout'), array('escape' => false)) ?></li>
        </ul>
      </div>
    </div>
  </nav>

  <div class="container container-page">
    <?php echo $this->Flash->render() ?>

    <? echo $this->fetch('content'); ?>
  </div>

  <?
  echo $this->Html->script('/plugins/bootstrap/js/bootstrap.min');
  echo $this->Html->script('/plugins/jQuery-Multiple-Select/dist/js/bootstrap-multiselect');

  echo $this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js');
  ?>
</body>

</html>