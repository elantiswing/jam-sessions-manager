<div class="tunes select">
    <div>
        <h2><?php echo __('Usuarios por Instrumento'); ?></h2>
        
        <div class="row">
            <? foreach ($usersByInstrument as $instrument) : ?>
                <div class="tune col-lg-3 col-md-4 col-sm-6">
                    <div class="inner">
                        <h5><? echo $instrument->instrument . '(' . count($instrument->my_users) . ')'; ?></h5>

                        <table class="table table-sm table-striped">
                          <? foreach ($instrument->my_users as $user) : ?>
                            <tr class="instrument">
                                <td><? echo $user->username; ?></td>
                            </tr>
                          <? endforeach; ?>
                        </table>
                    </div>
                </div>
            <? endforeach; ?>
        </div>
    </div>
</div>