<?

namespace App\View\Cell;

use Cake\View\Cell;
use Cake\Core\Configure;

class AvailableSchedulesCell extends Cell {

    public function display($selected = []) {
        $this->loadModel('AvailableSchedules');

        $editionData = Configure::read('edition');
        
        $availableSchedules = $this->AvailableSchedules->find()->toArray();
        
        $this->set(compact(['availableSchedules', 'selected', 'editionData']));
    }

}
