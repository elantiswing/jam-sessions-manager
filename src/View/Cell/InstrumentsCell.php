<?

namespace App\View\Cell;

use Cake\View\Cell;

class InstrumentsCell extends Cell {

    public function selectMultiple($options = []) {
        $this->loadModel('Instruments');
        
        $instruments = $this->Instruments->find('list')->toArray();
        
        $this->set(compact(['instruments', 'options']));
    }

}
