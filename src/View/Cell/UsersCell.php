<?

namespace App\View\Cell;

use Cake\View\Cell;
use Cake\Core\Configure;

class UsersCell extends Cell {

    public function initialize() {
        $this->loadModel('MyUsers');
    }

    public function display($options = []) {
        $users = $this->MyUsers->find('list')->toArray();
        
        $this->set(compact(['users', 'options']));
    }

    public function selections($userId) {
        $this->loadModel('Instruments');
        $instruments = $this->Instruments->find()->toArray();

        $userSelections = $this->MyUsers->find('userSelections', ['user_id' => $userId, 'instruments' => $instruments]);

        $this->set(compact('userSelections', 'userId'));
    }

}
