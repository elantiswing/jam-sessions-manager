<?

namespace App\View\Cell;

use Cake\View\Cell;

class EditionsCell extends Cell {

    public function display($options = []) {
        $this->loadModel('Editions');
        
        $editions = $this->Editions->find('list');
        
        $this->set(compact(['editions', 'options']));
    }

}
