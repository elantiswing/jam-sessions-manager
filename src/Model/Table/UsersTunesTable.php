<?

namespace App\Model\Table;

use Cake\ORM\Table;

class UsersTunesTable extends Table
{
  public function initialize(array $config) {
    parent::initialize($config);

    $this->setTable('users_selected_tunes');

    $this->belongsTo('Instrument');
    $this->belongsTo('MyUsers')
      ->setForeignKey('user_id')
      ->setProperty('user');
  }
}