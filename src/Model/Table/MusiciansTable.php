<?

namespace App\Model\Table;

use Cake\ORM\Table;

class MusiciansTable extends Table
{
  public function initialize(array $config) {
    parent::initialize($config);

    $this->setTable('users');

    $this->setEntityClass('MyUser');

    $this->hasMany('Instrument');
  }
}