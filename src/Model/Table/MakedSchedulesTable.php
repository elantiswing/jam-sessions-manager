<?

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\Query;
use Cake\Validation\Validator;
use Cake\Event\Event;
use ArrayObject;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;

class MakedSchedulesTable extends Table {
    
    public function initialize(array $config) {
        parent::initialize($config);
        
        $this->addBehavior('Timestamp');
    }
}
