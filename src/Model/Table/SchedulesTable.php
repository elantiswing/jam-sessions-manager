<?

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\Query;
use Cake\Validation\Validator;
use Cake\Event\Event;
use ArrayObject;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;

class SchedulesTable extends Table {
    
    public function initialize(array $config) {
        parent::initialize($config);
        
        $this->addBehavior('Timestamp');

        $this->hasMany('MyUsers');
        $this->belongsTo('AvailableSchedules');
    }

    public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options) {
        if (isset($data['unavailable']) && $data['unavailable'] == 'on') {
            $data['unavailable'] = true;
            $data['start'] = null;
            $data['end'] = null;
        }else {
            $data['unavailable'] = false;
        }
    }

    public function findAvailablesTunes(Query $query, array $options = []) {
        $availableSchedules = $this->AvailableSchedules->find()->toArray();

        $this->SchedulesExceptions = TableRegistry::get('SchedulesExceptions');
        $exceptions = $this->SchedulesExceptions->find()
                        ->where(['SchedulesExceptions.edition_id' => Configure::read('edition')['edition_id']])
                        ->all();

        $maxRange = [];
        foreach($availableSchedules as $availableSchedule) {
            $maxRange[$availableSchedule->id]['start'] = $availableSchedule->start->toUnixString();
            $maxRange[$availableSchedule->id]['end'] = $availableSchedule->end->toUnixString();
        }

        $this->Tunes = TableRegistry::get('Tunes');

        return $this->Tunes->find()
                ->contain(['SchedulesExceptions', 'Musicians' => ['Schedules']])
                ->matching('Editions', function($q) {
                    return $q->where(['Editions.edition' => Configure::read('edition')['edition']]);
                })
                ->formatResults(function($tunes) use ($maxRange, $exceptions) {
                    return $tunes->map(function($tune) use ($maxRange, $exceptions){
                        $maxRangeTune = $maxRange;
                        foreach($tune->musicians as $musician) {
                            $findInExceptions = $exceptions->firstMatch([
                                'tune_id' => $tune->id,
                                'user_id' => $musician->id
                            ]);

                            if($findInExceptions) {
                                continue;
                            }

                            foreach($musician->schedules as $schedule) {
                                if($schedule->unavailable == true || $maxRangeTune[$schedule->available_schedules_id] == NULL) {
                                    $maxRangeTune[$schedule->available_schedules_id] = NULL;
                                    continue;
                                }

                                $start = $schedule->start->toUnixString();
                                if($start > $maxRangeTune[$schedule->available_schedules_id]['start']) {
                                    $maxRangeTune[$schedule->available_schedules_id]['start'] = $start;
                                }

                                $end = $schedule->end->toUnixString();
                                if($end < $maxRangeTune[$schedule->available_schedules_id]['end']) {
                                    $maxRangeTune[$schedule->available_schedules_id]['end'] = $end;
                                }
                                
                                if($maxRangeTune[$schedule->available_schedules_id]['start'] >= $maxRangeTune[$schedule->available_schedules_id]['end']) {
                                    $maxRangeTune[$schedule->available_schedules_id] = NULL;
                                }
                            }
                        }

                        $tune['schedule_max_range'] = $this->_formatMaxRangeTune($maxRangeTune);
                        // unset($tune->musicians);
                        unset($tune->_matchingData);

                        return $tune;
                    });
                });
    }

    private function _formatMaxRangeTune($maxRangeTune) {
        $formated = [];
        foreach($maxRangeTune as $key => $date) {
            if($date == NULL) {
                $formated[$key]['start'] = NULL;
                $formated[$key]['end'] = NULL;
            }else {
                $formated[$key]['start'] = date('Y-m-d H:i', $date['start']);
                $formated[$key]['end'] = date('Y-m-d H:i', $date['end']);
            }
        }

        return $formated;
    }

    public function findDataForTableMaker (Query $query, array $options = []) {
        $this->MyUsers = TableRegistry::get('MyUsers');
        $usersForEdition = $this->MyUsers->find('usersForEdition');
        $users = [];
        foreach($usersForEdition as $musician) {
            array_push($users, ['userId' => $musician->id, 'name' => $musician->username]);
        }

        $data = [];

        $data[0] = $users;

        $tunes = $this->findAvailablesTunes($query, $options)->toArray();

        $count = 1;
        foreach ($tunes as $tune) {
            $data[$count][0] = [
                'tuneId' => $tune->id, 
                'name' => $tune->name,
                'artist' => $tune->artist,
                'schedules' => $tune->schedule_max_range
            ];

            foreach($users as $user) {
                $match = false;
                if(array_search($user['userId'], array_column($tune->schedules_exceptions, 'user_id')) === false) {
                    if(array_search($user['userId'], array_column($tune->musicians, 'id')) !== false) {
                        $match = true;
                    }
                }else {
                    $match = -1; // SI está excluído devolve -1
                }

                array_push($data[$count], $match);
            }

            $count++;
        }
        
        return $data;
    }
}
