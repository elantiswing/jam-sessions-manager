<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\Query;
use Cake\Collection\Collection;
use Cake\Core\Configure;

class InstrumentsTable extends Table {
    
    public function initialize(array $config) {
        parent::initialize($config);
        
        $this->setDisplayField('instrument');
        
        $this->addBehavior('Timestamp');

        $this->belongsToMany('Tunes');
        $this->belongsToMany('MyUsers')
            ->setTargetForeignKey('user_id');

        $this->edition = Configure::read('edition');
    }
    
    public function findUsersByInstrument(Query $query, array $options = []) {
        $editionInfo = $this->edition;

        return $query
            ->contain(['MyUsers' => [
                'sort' => [
                    'username' => 'ASC'
                ],
                'Editions'
            ]])
            ->formatResults(function($instruments) use ($editionInfo) {
                return $instruments->map(function($instrument) use ($editionInfo) {
                    $users = new Collection($instrument->my_users);

                    $users = $users->filter(function($user) use ($editionInfo) {
                        foreach($user->editions as $edition) {
                            if($edition->edition == $editionInfo['edition']) {
                                return true;
                            }
                        }

                        return false;
                    })->toArray();

                    $instrument->my_users = $users;
                    $instrument->clean();

                    return $instrument;
                });
            });
    }
}
