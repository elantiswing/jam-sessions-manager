<?php

namespace App\Model\Table;

use Cake\ORM\Table;

/**
 * CakePHP ConfigTable
 * @author elant
 */
class ConfigTable extends Table {
    
    public function initialize(array $config) {
        parent::initialize($config);
        
        $this->addBehavior('Timestamp');
    }

    public function getConfig() {
        return $this->find()->formatResults(function (\Cake\Collection\CollectionInterface $results) {
            $newArray = [];
            foreach($results as $result) {
                $newArray[$result['config']] = $result['value'];
            }

            return $newArray;
        });
    }
}
