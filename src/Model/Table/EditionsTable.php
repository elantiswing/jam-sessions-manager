<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Table;

use Cake\ORM\Table;

/**
 * CakePHP EditionsTable
 * @author elant
 */
class EditionsTable extends Table {
    
    public function initialize(array $config) {
        parent::initialize($config);
        
        $this->setDisplayField('edition');
        
        $this->addBehavior('Timestamp');

        $this->belongsToMany('MyUsers');
        $this->belongsToMany('Tunes');
    }
}
