<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model\Table;

use App\Model\Entity\EditionTune;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\Query;
use Cake\Core\Configure;
use Cake\Utility\Hash;

/**
 * CakePHP TunesTable
 * @author elant
 */
class TunesTable extends Table {
    
    public function initialize(array $config) {
        parent::initialize($config);
        
        $this->addBehavior('Timestamp');
        $this->addBehavior('Search.Search');

        // Setup search filter using search manager
        $this->searchManager()
            ->add('q', 'Search.Like', [
                'before' => true,
                'after' => true,
                'fieldMode' => 'OR',
                'comparison' => 'LIKE',
                'wildcardAny' => '*',
                'wildcardOne' => '?',
                'field' => ['name', 'artist']
            ])
            ->add('editions', 'Search.Callback', [
                'callback' => function ($query, $args, $filter) {
                    return $query->matching('Editions', function($q) use($args) {
                        return $q->where(['Editions.id' => $args['editions']]);
                    });
                }
            ]);

        $this->belongsToMany('Editions');
        $this->belongsToMany('Instruments');
        $this->hasMany('EditionsTunes');
        $this->belongsTo('MyUsers')
            ->setForeignKey('user_id');

        $this->belongsToMany('Musicians')
                ->setClassName('MyUsers')
                ->setThrough('users_selected_tunes')
                ->setTargetForeignKey('user_id')
                ->setConditions(['users_selected_tunes.edition_id' => Configure::read('edition')['edition_id']]);

        $this->hasMany('SchedulesExceptions');

        $this->edition = Configure::read('edition');
    }

    public function findAllTunes(Query $query, array $options = []) {
        return $query
            ->contain(['Editions', 'MyUsers']);
    }

    public function findAllProposes(Query $query, array $options = []) {
        return $query
            ->contain(['Editions', 'MyUsers', 'Instruments'])
            ->matching('Editions', function($q) {
                return $q->where(['Editions.edition' => $this->edition['edition']]);
            });
    }

    public function findForSelect(Query $query, array $options = []) {
        return $query
            ->contain(['Instruments', 'Musicians' => 'Instruments'])
            ->matching('Editions', function($q) {
                return $q->where(['Editions.edition' => $this->edition['edition']]);
            })
            ->formatResults(function($result) {
                return $result->map(function($row) {
                    foreach($row->instruments as $key => $instrument) {
                        $row->instruments[$key]['musicians'] = [];
                        foreach($row->musicians as $musician) {
                            if($musician->_joinData->instrument_id == $instrument->id) {
                                $row->instruments[$key]['musicians'][$musician->id] = $musician->username;
                            }
                        }
                    }

                    return $row;
                });
            });
    }

    public function findById(Query $query, array $options = []) {
        return $query
            ->contain(['Editions', 'MyUsers', 'Instruments'])
            ->where(['Tunes.id' => $options['id']])
            ->first();
    }

    public function addToEdition($tuneId) {
        $edition = $this->edition['edition'];

        $tune = $this->find()
                ->contain(['Editions'])
                ->notMatching('Editions', function($q) use($edition) {
                    return $q->where(['Editions.edition' => $edition]);
                })
                ->where(['Tunes.id' => $tuneId])
                ->first();

        if(!$tune) {
            return false;
        }

        $ids = Hash::extract($tune->editions, '{n}.id');
        $edition = $this->Editions->find()
                        ->where(['Editions.edition' => $edition])
                        ->first();

        if(!$edition) {
            return false;
        }

        array_push($ids, $edition->id);

        $entity = $this->patchEntity($tune, ['editions' => ['_ids' => $ids]]);

        return $this->save($entity);
    }

    public function removeFromEdition($tuneId) {
        $tune = $this->find()
                ->contain([
                    'Editions' => [
                        'conditions' => [
                            'Editions.edition' => $this->edition['edition']
                        ]
                    ]
                ])
                ->where(['Tunes.id' => $tuneId])
                ->first();
        
        if(empty($tune->editions[0]->_joinData)) {
            return false;
        }

        return $this->EditionsTunes->delete($tune->editions[0]->_joinData);
    }
}
