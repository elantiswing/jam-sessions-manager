<?

namespace App\Model\Table;

use CakeDC\Users\Model\Table\UsersTable;
use Cake\Core\Configure;
use Cake\ORM\Query;
use Cake\Event\Event;
use ArrayObject;
use Cake\Collection\Collection;

/**
 * Application specific Users Table with non plugin conform field(s)
 */
class MyUsersTable extends UsersTable
{
  public function initialize(array $config) {
    parent::initialize($config);

    // $this->setTable('users');
    $this->setEntityClass('MyUser');

    $this->belongsToMany('Editions');

    $this->belongsToMany('Instruments');

    $this->belongsToMany('Tunes')
        ->setThrough('UsersTunes')
        ->setConditions(['UsersTunes.edition_id' => Configure::read('edition')['edition_id']]);
        
    $this->hasMany('Proposals')
        ->setClassName('Tunes');

    $this->hasMany('Schedules')
        ->setConditions(['edition_id' => Configure::read('edition')['edition_id']]);
  }

  public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options) {
      if (!empty($data['password_change'])) {
          $data['password'] = $data['password_change'];
      }
  }

  public function findUsersForEdition(Query $query, array $options = []) {
    $editionId = Configure::read('edition')['edition_id'];

    return $query
            ->matching('Editions', function($q) use ($editionId) {
                return $q->where([ 
                    'Editions.id' => $editionId
                ]);
            });
  }

  public function findUserSelections(Query $query, array $options = []) {
    $userId = $options['user_id'];
    $instruments = $options['instruments'];
    $editionId = Configure::read('edition')['edition_id'];

    return $query->find('allUsersSelections')
            ->where([
                'MyUsers.id' => $userId
            ])
            ->first();
  }

  public function findAllUsersSelections(Query $query, array $options = []) {
    $instruments = $options['instruments'];
    $editionId = Configure::read('edition')['edition_id'];

    return $query
            ->contain(['Tunes.Editions'])
            ->matching('Tunes.Editions', function($q) use ($editionId) {
                return $q->where([ 
                    'Editions.id' => $editionId
                ]);
            })
            ->formatResults(function($results) {
                return $results->map(function($users) {
                    $tunes = new Collection($users->tunes);

                    $users->tunes = $tunes->filter(function($tune) {
                        return !empty($tune->editions);
                    })->toArray();

                    $users->clean();
                    return $users;
                });
            })
            ->formatResults(function($results) use ($instruments) {
                return $results->map(function($result) use($instruments) {
                    $groupedTunes = [];
                    foreach($result->tunes as $tune) {
                        if(empty($groupedTunes[$tune->id])) {
                            $groupedTunes[$tune->id] = $tune;
                        }

                        $findedInstrumentKey = array_search($tune->_joinData->instrument_id, array_column($instruments, 'id'));

                        if(empty($groupedTunes[$tune->id]['selected_instruments'])) {
                            $groupedTunes[$tune->id]['selected_instruments'] = [];
                        }
                        
                        array_push($groupedTunes[$tune->id]['selected_instruments'], $instruments[$findedInstrumentKey]);
                    }

                    $result->tunes = $groupedTunes;
                    $result->clean();
                    
                    return $result;
                });
            });
  }
}