<?

namespace App\Model\Entity;

use \CakeDC\Users\Model\Entity\User;

class MyUser extends User {
    
  public $_accesible = [
    '*' => true
  ];

  public $_virtual = ['fullName'];

  protected function _getFullName() {
    if(empty($this->_properties['name'])) {
      return '';
    }

    $fullName = $this->_properties['name'];
    if(!empty($this->_properties['first_surname'])) {
      $fullName .= ' ' . $this->_properties['first_surname'];
    }

    if(!empty($this->_properties['second_surname'])) {
      $fullName .= ' ' . $this->_properties['second_surname'];
    }

    return $fullName;
  }

  public function activeInEdition($editionId) {
    $editions = $this->_properties['editions'];

    foreach($editions as $edition) {
      if($edition->id == $editionId){
        return true;
      }
    }
    
    return false;
  }
}
