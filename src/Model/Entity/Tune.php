<?

namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\Utility\Hash;

class Tune extends Entity {
    
  public $_accesible = [
    '*' => true
  ];

  public $_virtual = ['editionsList', 'fullName'];

  protected function _getEditionsList() {
    if(empty($this->_properties['editions'])) {
      return '';
    }
    
    $editionsArray = Hash::extract($this->_properties['editions'], '{n}.edition');
    rsort($editionsArray, SORT_NUMERIC);

    return implode(', ', $editionsArray);
  }

  protected function _getFullName() {
    return $this->_properties['name'] . ' (' . $this->_properties['artist'] . ')';
  }
}
