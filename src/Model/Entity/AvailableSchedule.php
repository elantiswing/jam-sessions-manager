<?

namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\I18n\Time;

class AvailableSchedule extends Entity {
    
  public $_accesible = [
    '*' => true
  ];

  public $_virtual = ['steps', 'day'];

  protected function _getSteps() {
    return $this->_createSteps();
  }

  protected function _getDay() {
    $days = ['Domingo', 'Luns', 'Martes', 'Mércores', 'Xoves', 'Venres', 'Sábado'];
    return $days[$this->_properties['start']->format('w')] . ' ' . $this->_properties['start']->format('j');
  }

  private function _createSteps() {
    $secondsBetweenSteps = 15 * 60;
    $start = $this->_properties['start']->toUnixString();
    $end = $this->_properties['end']->toUnixString();

    $steps[$this->_properties['start']->format('Y-m-d H:i:s')] = $this->_properties['start']->format('H:i');
    
    while (strtotime(key(array_slice($steps, -1, 1, true))) < $end) {
      $lastStep = key(array_slice($steps, -1, 1, true));
      $nextStep = strtotime($lastStep) + $secondsBetweenSteps;

      $steps[date('Y-m-d H:i:s', $nextStep)] = date('H:i', $nextStep);
    }

    return $steps;
  }
}
